<?php

namespace App\Http\Controllers\Customer\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Category;
use Illuminate\Http\Request;
use App\Profile;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        $categories = Category::all();
        $areas      = DB::table('areas')->get();
        $pincodes   = DB::table('pincodes')->get();

        return view('customer.auth.register',compact('categories',$categories,'areas',$areas,'pincodes',$pincodes));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            "first_name"         => 'required|max:255',
            "last_name"          => 'required|max:255',
            "email"             => 'required|email|max:255|unique:users',
            "password"          => 'required|min:5|confirmed',
            "telephone"         => 'required|numeric|min:10|max:10|unique:users',
            "phone_alternate"   => 'required|numeric',
            "address"           => 'required|max:255|',
            "landmark"          => 'required|max:255|',
            "direction"         => 'required|max:255|',
            "area_id"           => 'required',
            "pincode_id"        => 'required|numeric|min:6|max:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(Request $request)
    {        
        $this->validate($request,[
            "first_name"         => 'required|max:255',
            "last_name"          => 'required|max:255',
            "email"             => 'required|email|max:255|unique:users',
            "password"          => 'required|min:6|confirmed',
            "telephone"         => 'required|numeric|unique:customer_profiles,phone_number',
            "phone_alternate"   => 'required|numeric|unique:customer_profiles,alt_phone_number',
            "address"           => 'required|max:255|',
            "landmark"          => 'required|max:255|',
            "direction"         => 'required|max:255|',
            "area_id"           => 'required',
            "pincode_id"        => 'required',
        ]);
        
        $user = User::create([
            'role'      => 'Customer',
            'first_name'=> $request->first_name,
            'last_name' => $request->last_name,
            'email'     => $request->email,
            'password'  => bcrypt($request->password),
        ]);
           
        $customerProfile = Profile::create([
            'user_id'           => $user->id,
            'phone_number'      => $request->telephone,
            'alt_phone_number'  => $request->phone_alternate,
            'address'           => $request->address,
            'landmark'          => $request->landmark,
            'direction'         => $request->direction,
            'area_id'           => $request->area_id,
            'pincode_id'        => $request->pincode_id,
        ]);
        if($user && $customerProfile){
            $request->session()->flash('success', 'User Added Successfully !!');
        }else{
            $request->session()->flash('warning', 'Something went wrong. User is not saved!!');
        }
        return redirect(url('/'));
    }
}
