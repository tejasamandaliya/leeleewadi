<?php

namespace App\Http\Controllers\Customer;

use Support;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the customer home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function categoriesList(Category $category = null)
    {
        $productLists   = Product::where('category_id',$category->id)->get();
        $categories     = Category::all();
        return view('customer.list',compact('productLists',$productLists,'categories',$categories));
    }
    /**
     * Show the customer home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function productView(Product $product = null)
    {

        $product    = Product::find($product->id);
        $categories = Category::all();
        $randomProducts = Product::inRandomOrder()->take(4)->where('id','<>',$product->id)->get();
        return view('customer.product',compact('product',$product,'categories',$categories,'randomProducts',$randomProducts));
    }

}
