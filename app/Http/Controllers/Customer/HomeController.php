<?php

namespace App\Http\Controllers\Customer;

use Support;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\User;
use App\Profile;
use Illuminate\Support\Facades\Auth;
use App\Order;
use App\Products;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the customer home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('customer.home',compact('categories',$categories));
    }
    /**
     * Show the customer home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function userProfile()
    {
        $categories = Category::all();
        $areas      = DB::table('areas')->get();
        $pincodes   = DB::table('pincodes')->get();
        return view('customer.profile',compact('categories',$categories,'areas',$areas,'pincodes',$pincodes));
    }
    /**
     * Show the customer home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function userProfileSave(Request $request){
        $this->validate($request,[
            "first_name"         => 'required|max:255',
            "last_name"          => 'required|max:255',
            "email"             => 'required|email|max:255|unique:users,id,'.$request->user_id,
            "telephone"         => 'required|numeric|unique:customer_profiles,user_id,'.$request->user_id,
            "phone_alternate"   => 'required|numeric|unique:customer_profiles,user_id,'.$request->user_id,
            "address"           => 'required|max:255|',
            "landmark"          => 'required|max:255|',
            "area_id"           => 'required',
            "pincode_id"        => 'required',
        ]);
        $user = User::find($request->user_id);
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->email = $request->input('email');
        $successUser = $user->save();
        
        $customerProfile = Profile::find(Auth::user()->customerProfile->id);
        
        $customerProfile->phone_number = $request->input('telephone');
        $customerProfile->alt_phone_number = $request->input('phone_alternate');
        $customerProfile->address =$request->input('address') ;
        $customerProfile->landmark = $request->input('landmark');
        $customerProfile->area_id = $request->input('area_id');
        $customerProfile->pincode_id = $request->input('pincode_id');
        $successCustUser = $customerProfile->save();

        if($successUser && $successCustUser){
            $request->session()->flash('success', 'User Update Successfully !!');
        }else{
            $request->session()->flash('warning', 'Something went wrong. User is not saved!!');
        }
        return redirect(url('/profile'));

            
    }

     /**
     * Show the customer home page.
     * 
     * @return \Illuminate\Http\Response
     */
    public function productList()
    {
        $categories = Category::all();
        return view('customer.list',compact('categories',$categories));
    }

    /**
     * Show the product product.
     *
     * @return \Illuminate\Http\Response
     */
    public function product($id)
    {
        return view('customer.product');
    }

    /**
     * Show the product checkout.
     *
     * @return \Illuminate\Http\Response
    */
    public function checkout()
    {
        $categories = Category::all();        
        return view('customer.checkout',compact('categories',$categories));

    }
    
    /**
        * Show the product place order.
        *
        * @return \Illuminate\Http\Response
    */
    public function place_order(Request $request) 
    {
        if($request->isMethod('post')) {
            
            if(Auth::check()) {
                if(Auth::user()->toArray()['role'] == Auth::user()->role) {
                    $productsData = $request->input('products');
                    $shippingCharge = $request->input('shippingCharge');

                    $order                  = new Order();
                    $order->user_id         = Auth::user()->id;
                    $order->list_price      = $request->input('total_price');
                    $order->shipping_charge = $shippingCharge;
                    $order->sale_price      = $shippingCharge + $request->input('total_price') ;
                    $order->status          = 'new';
                    $order->save();

                    foreach($productsData as $productData){
                        
                        $orderProduct = new Products;
                        $orderProduct->order_id         = $order->id;
                        $orderProduct->product_id       = $productData['id'];
                        $orderProduct->list_price       = $productData['price'];
                        $orderProduct->quantity         = $productData['quantity'];
                        $orderProduct->shipping_charges = $shippingCharge;
                        $orderProduct->sale_price       = $productData['price'] + $shippingCharge;
                        $orderProduct->per_quantity     = $productData['per_quantity'];
                        $orderProduct->save();
                    }

                    return response()->json(array('success'=> true), 200);
                }
            }
        }
        return redirect(url('/thanks'));
    }

     /**
     * Show the customer thanks page.
     *
     * @return \Illuminate\Http\Response
     */
    public function thanks()
    {
        $categories = Category::all();
        return view('customer.thanks',compact('categories',$categories));
    }
}
