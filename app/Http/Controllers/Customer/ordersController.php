<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\User;
use App\Profile;
use Illuminate\Support\Facades\Auth;
use App\Order;
use App\Products;
use DB;

class ordersController extends Controller
{
    /**
     * Show the Order History.
     *
     * @return \Illuminate\Http\Response
    */
    public function orderHistory()
    {
        $categories = Category::all();

        $orderList = Order::where('user_id', Auth::id())->orderBy('id', 'desc')->get();
        
        return view('customer.orders',compact('categories',$categories,'orderList',$orderList));
    }
    /**
     * Show the View Order History.
     *
     * @return \Illuminate\Http\Response
    */
    public function orderView($order_id)
    {
        $order = Order::where('id', $order_id)->where('user_id', Auth::id())->first();

        $categories = Category::all();
        
        if($order) {

            $success = true;
            
            $finalOrder = $order->toArray();

            $orderProducts = $order->orderProducts()->get();

            $finalOrder['products'] = [];

            foreach ($orderProducts as $p_index => $product) {

                $finalOrder['products'][$p_index] = $product->toArray();
                $finalOrder['products'][$p_index]['order_product_id'] = $finalOrder['products'][$p_index]['id'];
                unset($finalOrder['products'][$p_index]['id']);

                $mainProduct = $product->product()->get()->toArray()[0];
                unset($mainProduct['id']);

                $finalOrder['products'][$p_index] = array_merge($finalOrder['products'][$p_index], $mainProduct);
            }
        } else {
            $finalOrder = [];
            $success = false;
        }
        return view('customer.view_order',compact('categories',$categories,'finalOrder',$finalOrder, 'order_id', $order_id));
    }

    public function showAddresses(){
        $profileList = Profile::where('user_id', Auth::id())->orderBy('id', 'desc')->get();
        $categories = Category::all();
        
        return view('customer.profile_addresses',compact('categories',$categories,'profile_list',$profileList));
    }
}
