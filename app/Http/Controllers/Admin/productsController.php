<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\Http\Controllers\Session;
use App\Image;
use App\Product;

class productsController extends Controller
{

    /*Product CategoryList*/
    public function productCategoryList(Request $request){

    	if($request->input('category_name') !== null) {
            $category_name = $request->input('category_name');
            $categories = Category::where('name', $category_name)->count();
            if(!$categories)
            {
                $category = new Category;
                $category->name = $category_name;
                $category->url = $request->input('url');
                $succes = $category->save();

                if($succes)
                $request->session()->flash('success', 'Product Category Added Successfully !!');

            }
            else{
                $request->session()->flash('warning', 'Product Category Already Exists !!');
            }
        }

        $parent_categories = Category::all();

        if($request->expectsJson() || $request->segment(1) == 'api') {

            return response()->json(['success' => true, 'data' => compact('parent_categories', $parent_categories)]);
        }
        return view('admin.product_category', compact('parent_categories', $parent_categories));
    }

    /*Product Category update*/
	public function productCategoryUpdate(Request $request){
        $cat_id = $request->input('cat_id');
    	$new_cat_name = $request->input('new_cat_name');

    	$category = Category::find($cat_id);

        $category->name = $new_cat_name;
    	$category->url  = $request->input('new_cat_url');
    	$category->updated_at = time();

    	echo $category->save();
    }

    /*Product Category update*/
	public function productCategoryDelete(Request $request){

    	$cat_id = $request->input('cat_id');

    	$category = Category::find($cat_id);
    	echo $category->delete();
    }

    /*Product Listing*/
    public function productList(Request $request) {

        $productList = Product::all();
        if($request->has('category_id') && $request->get('category_id', 'all') != 'all') {
            $productList->where('category_id', $request->get('category_id'));
        }
        return view('admin.products', ['products' => $productList]);
    }

    /*Product View*/
    public function viewProduct(Request $request, $productId) {

        $product = Product::find($productId);

        $category = Category::find($product->category_id);
        
        $product = $product->toArray();
        $product['category'] = $category ? $category->name : '';

        // if($request->expectsJson() || $request->segment(1) == 'api') {

        //     return response()->json(['success' => true, 'data' => $product]);
        // }
        return view('admin.view_product', ['product' => $product]);
    }

    /*Product Create*/
    public function createProduct(Request $request, $productId = null) {


        if($request->isMethod('post')) {
            
            $validationRules = [
                'name'          =>  'required:min:3',
                'description'   =>  'required:min:5',
                'categories'    =>  'required',
                'price'         =>  'required|numeric',
                'url' =>  'required',
                'units'         =>  'required',
                'quantity'      =>  'required',
            ];
            
            $this->validate($request, $validationRules);


            if( $productId && $productId != 0 ) {

                $product = Product::findOrFail($productId);

                $product->name          = $request->input('name');
                $product->description   = $request->input('description');
                $product->category_id   = $request->input('categories');
                $product->price         = $request->input('price');
                $product->url         = $request->input('url');
                $product->nutrient              = $request->input('nutrient');
                $product->shelf_life            = $request->input('shelf_life');
                $product->storage_tips          = $request->input('storage_tips');
                $product->storage_temperature   = $request->input('storage_temperature');
                $product->disclaimer            = $request->input('disclaimer');
                $product->quantity            = $request->input('quantity');
                $product->units            = $request->input('units');
                $success                        = $product->save();

                if($success && (!$request->expectsJson() || $request->segment(1) != 'api')) {
                    $request->session()->flash('success', 'Product Changes successfully submitted.');
                }
                
            } else {

                $product = new Product();
                $product->name          = $request->input('name');
                $product->description   = $request->input('description');
                $product->category_id   = $request->input('categories');
                $product->price         = $request->input('price');
                $product->url         = $request->input('url');
                $product->nutrient              = $request->input('nutrient');
                $product->shelf_life            = $request->input('shelf_life');
                $product->storage_tips          = $request->input('storage_tips');
                $product->storage_temperature   = $request->input('storage_temperature');
                $product->disclaimer         = $request->input('disclaimer');
                $product->quantity            = $request->input('quantity');
                $product->units            = $request->input('units');
                $success = $product->save();
            }

            // if($request->file('primary_image') && $request->file('primary_image')->isValid()) {

            //     $primaryImagePath = $request->file('primary_image')->store('product_images');

            //     $mainImage = Image::where('file',$primaryImagePath)->where('product_id',$productId)->first();
            //     if($mainImage){
            //         $mainImage->product_id  = $product->id;
            //         $mainImage->caption     = $request->file('primary_image')->getClientOriginalName();
            //         $mainImage->file        = $primaryImagePath;

            //         $mainImage->save();
            //     } else {
            //         $productImage = new Image();

            //         $productImage->product_id = $product->id;
            //         $productImage->caption = $request->file('primary_image')->getClientOriginalName();
            //         $productImage->file = $primaryImagePath;

            //         $productImage->save();

            //         $product->primary_image_id = $productImage->id;
            //         $success = $product->save();
            //     }


            // }

            // Saving sub images if there
            // if(count($request->file('sub_images')) > 0) {

            //     foreach ($request->file('sub_images') as $index => $subImage) {

            //         if($subImage->isValid()) {

            //             $secondaryImagePath = $subImage->store('product_images');

            //             $productImage = new Image();

            //             $productImage->product_id = $product->id;
            //             $productImage->caption = $subImage->getClientOriginalName();
            //             $productImage->file = $secondaryImagePath;

            //             $productImage->save();

            //             $request->session()->flash('success', 'Product sub images are saved successfully !!');
            //         }
            //     }
            // }

            if($request->expectsJson() || $request->segment(1) == 'api') {

                return response()->json([
                    'success' => true,
                    'msg' => (($success || count($updates) > 0) ? 'Product Changes successfully submitted for admin approval !!' : 'Something went wrong. Product is not saved!!'),
                    'data' => $product->id
                ]);
            }
            return redirect('admin/products');

        } else {

            $productDetail = new Product();
            $productImages = [];

            if($productId) {
                $productDetail = Product::find($productId);
                // $productImages = $productDetail->productImages;
            }

            $productCategories = Category::all();

            return view('admin.addproduct', [
                'categories'    =>  $productCategories,
                'product'       =>  $productDetail,
                // 'images'        =>  $productImages
            ]);
        }
    }
    public function removeProductImage($imageId) {

        $image = Image::find($imageId);

        if($image) {

            $image->delete();

            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false, 'errorCode' => config('constants.ERROR_NO_DATA_FOUND')]);
    }

    public function productDelete(Request $request) {

        $product = Product::find($request->input('product_id'));
        if($product) {

            $product->delete();

            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false, 'errorCode' => config('constants.ERROR_NO_DATA_FOUND')]);
    }

    public function exportData(Request $request){

        $retailer = Auth::user()->retailerStaff->retailer;

        $productList = $retailer->products;

        $retailerName = $retailer->name;
        
        if($request->segment(3) === 'excel'){

            Excel::create($retailerName.' Product List', function($excel) use ($productList) {
                $excel->sheet('Retailer', function($sheet) use ($productList)
                {
                    $sheet->fromArray($productList);
                });
            })->download("xls");

        }else if($request->segment(3) === 'csv'){

            Excel::create($retailerName.' Product List', function($excel) use ($productList) {
                $excel->sheet('Retailer', function($sheet) use ($productList)
                {
                    $sheet->fromArray($productList);
                });
            })->download("csv");    

        }else if($request->segment(3) === 'xml'){

            return view('retailer.export_view', ['products' => $productList,'retailerName' => $retailerName]);

        }else if($request->segment(3) === 'print'){

            return view('retailer.export_view', ['products' => $productList,'retailerName' => $retailerName]);

        }else{

            return response()->json(['success' => false, 'errorCode' => config('constants.ERROR_NO_DATA_FOUND')]);
        }
    }
}
