<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
// use App\Models\Coin\Coin;
// use App\Models\Coin\Redemption;
use App\Order;
use App\Products;
// use App\Models\Retailer\Retailer;
// use App\Models\Customer\Feedback;
use App\Profile;
use App\Product as MainProduct;
// use App\Models\Retailer\Store;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
Use App\Country;
use PDF;

class ordersController extends Controller
{
    public function orderList(Request $request) {

        // $orderList = Auth::user()->retailerStaff->retailer->orders()->where('status', '!=', 'new')->orderBy('id', 'desc')->get();
       $orderList = Order::where('status','completed')->orWhere('status','cancelled')->get(); // To fetch deleted orders too

        if(count($orderList) > 0) {
            foreach ($orderList as $index => $order) {
                $order->user;

                $order->customerFeedbacks;

                /*$order->orderProducts;

                foreach ($order->orderProducts as $p_index => $product) {

                    $product->product;
                }*/
            }
        }
        if($request->expectsJson() || $request->segment(1) == 'api') {

            return response()->json(['success' => true, 'data' => $orderList]);
        }
        return view('admin.orders', ['orderList' => $orderList]);
    }

    public function pendingOrder(Request $request){
       $orderList = Order::where('status','!=','completed')->where('status','!=','cancelled')->get(); // To fetch deleted orders too

        if(count($orderList) > 0) {
            foreach ($orderList as $index => $order) {
                $order->user;

                $order->customerFeedbacks;

                /*$order->orderProducts;

                foreach ($order->orderProducts as $p_index => $product) {

                    $product->product;
                }*/
            }
        }
        if($request->expectsJson() || $request->segment(1) == 'api') {

            return response()->json(['success' => true, 'data' => $orderList]);
        }
        return view('admin.pending_orders', ['orderList' => $orderList]);
    }
    public function getorderstatistics(Request $request) {

        $noofdays=false;
        // Orders
        $orders = Order::where('deleted_at',NULL);
        if($noofdays) {
           $orders = $orders->where('created_at', '>', Carbon::now()->subDays(30));
        }

        $orderList = $orders->get();


        $results = array();
        $results['sale'] = number_format($orders->sum('list_price'),2);
        $results['trxCount'] = $orders->count();
        $results['AvgpriceTrx'] = number_format($orders->avg('list_price'), 2);

       if($request->expectsJson() || $request->segment(1) == 'api') {
            return response()->json(['success' => true, 'data' => $results]);
        }

    }

    public function getchart(Request $request){
		$m = date("m");
		$de= date("d");
		$y= date("Y");
		$format = 'd/m/Y';
		$dateArray = array();
		for ($i=0; $i<=6; $i++){
			$dateArray[] = date($format, mktime(0,0,0,$m,($de-$i),$y));
		}
		$last7days=array_reverse($dateArray);
		foreach($last7days as $lastday) {
			$actualdate=date('Y-d-m',strtotime($lastday));
			$orders = Auth::user()->retailerStaff->retailer->orders()->whereDate('created_at', '=',$actualdate);
			$sumprice[$actualdate]=$orders->sum('list_price');
			$totalorder[$actualdate]=$orders->count();
		}
		if($request->expectsJson() || $request->segment(1) == 'api') {
			return response()->json(['success' => true, 'data' => $sumprice,'totalorderdata' => $totalorder]);
		}
	}

    public function getCustomers() {

        $response = [
            'success'   =>  false,
            'data'      =>  []
        ];

        $response['data'] = User::where('role', 'Customer')->get();

        if($response['data'] && count($response['data']) > 0) {
            $response['success'] = true;
        }
        return response()->json($response);
    }

    public function getCustomerProfile($customerId) {

        
        $user = User::where('id', $customerId)->where('role', 'Customer')->first();

        if($user) {

            $profile = $user->customerProfile()->get();

            $user = $user->toArray();

            if(count($profile) > 0) {

                $country = $profile[0]->country()->get()->toArray();
                $user['profile'] = $profile->toArray()[0];

                if(count($country) > 0) {

                    $user['profile']['country'] = $country[0];
                }
            }
            return response()->json([ 'success' => true, 'data' => $user ]);
        }
        return response()->json([ 'success' => false, 'errorCode' => config('constants.ERROR_NO_DATA_FOUND') ]);
    }

    public function viewOrder(Order $order = null) {

        if($order->id) {

            return view('admin.view_order', [
                'order' => $order,
                'order_id' => $order->id
            ]);
        }
        return redirect('/admin/orders');
    }

    public function acceptStatusOrder(Request $request, $order_id)  {

        if($order_id) {
            $order = Order::find($order_id);
            $order->status = 'process';
            $order->save();
        }
        return redirect('/admin/pending/orders');
    }

    public function getMenifest()  {
        $orders = Order::where('status','process')->get();

        if($orders) {
            $finalOrder = [];
            foreach ($orders as $p_index => $order) {
                $finalOrder['orders'][$p_index] = $order->toArray();
                $finalOrder['orders'][$p_index]['customer'] = Profile::where('user_id',$order->user_id)->first();
            }
            return view('/admin/manifest', [
                'data' => $finalOrder
            ]);
            
        } else {

            return redirect('/admin/orders');
        }
    }

    public function cancelStatusOrder(Request $request, $order_id) {

        if($order_id) {
            $order = Order::find($order_id);
            $order->status = 'cancelled';
            $order->save();
        }
        return redirect('/admin/pending/orders');
    }

    public function printOrderInvoice($order_id) {

        $order = Order::find($order_id);

        if($order) {

            $finalOrder = $order->toArray();

            $orderProducts = $order->orderProducts()->get();

            $finalOrder['products'] = [];

            foreach ($orderProducts as $p_index => $product) {

                $finalOrder['products'][$p_index] = $product->toArray();
                $finalOrder['products'][$p_index]['order_product_id'] = $finalOrder['products'][$p_index]['id'];
                unset($finalOrder['products'][$p_index]['id']);

                $mainProduct = $product->product()->get()->toArray()[0];
                unset($mainProduct['id']);

                $finalOrder['products'][$p_index] = array_merge($mainProduct, $finalOrder['products'][$p_index]);
            }
            return view('/admin/invoice', [
                'orders' => $finalOrder,
                'order_id' => $order_id,
                'customer' => Profile::where('user_id',$order->user_id)->first()
            ]);
        }
        return redirect('/admin/orders');
    }

    public function getOrderDetails(Request $request, Order $order = null) {
        
        if($order->id) {

            $success = true;

            $finalOrder = $order->toArray();

            $orderProducts = $order->orderProducts()->get();

            $finalOrder['products'] = [];

            foreach ($orderProducts as $p_index => $product) {

                $finalOrder['products'][$p_index] = $product->toArray();

                $finalOrder['products'][$p_index]['order_product_id'] = $finalOrder['products'][$p_index]['id'];

                unset($finalOrder['products'][$p_index]['id']);

                $mainProduct = $product->product()->get()->toArray()[0];
                unset($mainProduct['id']);

                $finalOrder['products'][$p_index] = array_merge($mainProduct, $finalOrder['products'][$p_index]);
                // $finalOrder['products'][$p_index] = array_merge($finalOrder['products'][$p_index], $mainProduct);
            }
        } else {
            $finalOrder = [];
            $success = false;
        }

        return response()->json(['success' => $success, 'data' => $finalOrder]);
    }

    public function deleteOrder(Request $request, Order $order = null) {

        if(!$order->id) {

            if ($request->expectsJson() || $request->segment(1) == 'api') {

                return response()->json(['success' => false, 'errorCode' => config('constants.ERROR_NO_DATA_FOUND'), 'error' => 'No such order found.']);
            }
            $request->session()->flash('error', 'No such order found.');
            return redirect('/retailer/orders');
        }

        if($order->status == 'completed') {

            if ($request->expectsJson() || $request->segment(1) == 'api') {

                return response()->json(['success' => false, 'error' => 'You don\'t have permission to delete Order Detail !!']);
            }
            $request->session()->flash('warning', 'You don\'t have permission to delete Order Detail !!');
            return redirect('/retailer/orders');

        } else {

            if ($order) {
                $products = $order->orderProducts();

                if ($products) {
                    $products->delete();
                }

                $order->delete();
            }


            if ($request->expectsJson() || $request->segment(1) == 'api') {

                return response()->json(['success' => true]);
            }
            return redirect('/retailer/orders');
        }
    }

    public function startOrder(Request $request) {

        $this->validate($request, [
            'customer_id'   => 'required|exists:customer_profiles,user_id',
            'store_id'      => 'required|exists:retailer_stores,id'
        ]);

        $orderData = Order::create([
            'user_id'   => $request->input('customer_id'),
            'store_id'  => $request->input('store_id'),
            'status'    =>  'new',
            'retailer_id' => Store::find($request->input('store_id'))->retailer_id
        ]);

        return response()->json(['success' => true, 'order_id' => $orderData->id]);
    }


    public function addProductsToOrder(Request $request, Order $order = null) {

        if($order && $order->id) {

            $this->validate($request, [
                'products'  => 'required|array|min:'
            ]);

            $orderProducts = [];
            $orderListPriceTotal = 0;
            $orderDiscountTotal = 0;
            $orderSalePriceTotal = 0;

            $orderProductIds = array_map(function($item) {

                return $item['order_product_id'];

            }, $request->input('products'));

            Products::whereNotIn('id', $orderProductIds)->where('order_id', $order->id)->delete();

            foreach ($request->input('products') as $index => $product) {

                $product = array_filter($product);

                if(!isset($product['id']) || $product['id'] == 0 || !($productInfo = MainProduct::find($product['id']))) {

                    return response()->json(['success' => false, 'error' => 'Invalid product id provided for some product in cart.']);
                }
                if(!isset($product['quantity']) || $product['quantity'] == 0 || $product['quantity'] < 0) {

                    return response()->json(['success' => false, 'error' => 'Quantity is required for all ordered products.']);
                }

                $bestOffer = null;
                foreach ($productInfo->offerProducts as $offerProduct) {

                    if(strtotime($offerProduct->offer->start_at) < time() && strtotime($offerProduct->offer->end_at) > time()
                        && ($bestOffer == null || floatval($offerProduct->offer->discount_percentage) > floatval($bestOffer->discount_percentage))) {

                        $bestOffer = $offerProduct->offer;
                    }
                }

                if(isset($product['order_product_id']) && $product['order_product_id'] != 0) {

                    $orderProduct = Products::find($product['order_product_id']);

                    if(!$orderProduct) {

                        $orderProduct = new Products();
                    }
                } else {

                    $orderProduct = new Products();
                }

                if($orderProduct->id == null) {

                    $orderProduct->product_id = $product['id'];
                    $orderProduct->quantity = $product['quantity'];
                    $orderProduct->list_price = floatval($productInfo->price);
                    if($bestOffer != null) {

                        $orderProduct->discount = floatval($productInfo->price)*floatval($bestOffer->discount_percentage)/100;
                        $orderProduct->sale_price = floatval($productInfo->price) - (floatval($productInfo->price) * floatval($bestOffer->discount_percentage)/100);
                        $orderProduct->discount_percent = $bestOffer->discount_percentage;
                    } else {

                        $orderProduct->discount = 0;
                        $orderProduct->sale_price = floatval($productInfo->price);
                        $orderProduct->discount_percent = 0;
                    }
                } else {

                    $orderProduct->quantity = $product['quantity'];
                    $orderProduct->discount = floatval($orderProduct->list_price)*floatval($orderProduct->discount_percent)/100;
                    $orderProduct->sale_price = floatval($orderProduct->list_price) - (floatval($orderProduct->list_price) * floatval($orderProduct->discount_percent)/100);
                }

                $orderListPriceTotal += $orderProduct->list_price * $orderProduct->quantity;
                $orderDiscountTotal += $orderProduct->discount * $orderProduct->quantity;
                $orderSalePriceTotal += $orderProduct->sale_price * $orderProduct->quantity;

                $orderProduct->order_id = $order->id;
                $orderProduct->save();

                $orderProduct->order_product_id = $orderProduct->id;
                $orderProducts[] = $orderProduct;
            }

            $order->list_price = $orderListPriceTotal;
            $order->discount = $orderDiscountTotal;
            $order->sale_price = $orderSalePriceTotal;
            $order->coin_amount = 0;

            $order->save();

            return response()->json([
                'success'       => true,
                'order_id'      => $order->id,
                'order'         => $order,
                'cart_products' => $orderProducts
            ]);
        }
        return response()->json(['success' => false, 'errorCode' => config('constants.ERROR_NO_DATA_FOUND'), 'error' => 'No Order found']);
    }

    public function finalizeOrder(Request $request, Order $order = null) {

        if($order && $order->id) {

            if($order->products->count() > 0) {

                $coinsRedeemed = intval($request->input('coins_redeemed', 0));
                $customer = $order->user()->first();

                if($order->sale_price > 0 && $coinsRedeemed > 0) {

                    $request->request->add([
                        'coins' => $coinsRedeemed,
                        'payableAmount' => $order->sale_price
                    ]);

                    $coinsCheckerResponse = $this->checkCoinsForRedemption($request, $customer);
                    if(!$coinsCheckerResponse->getData()->success) {

                        return $coinsCheckerResponse;
                    }
                    $order->coin_amount = $coinsCheckerResponse->getData()->deductibleAmount;
                }

                $order->payment_time = date('Y-m-d H:i:s');

                if($order->status == 'new') {

                    $order->status = 'completed'; // TODO : Ask Hadi about order status on placing order
                }
                $order->sale_price = $order->sale_price - $order->coin_amount;

                $order->save();

                if($order->coin_amount > 0 && isset($coinsCheckerResponse) && $coinsCheckerResponse->getData()->success) {

                    // Creating transaction for coin redemption for order
                    Redemption::create([
                        'user_id' => $customer->id,
                        'order_id'=> $order->id,
                        'coins'   => $coinsRedeemed,
                        'amount'  => $coinsCheckerResponse->getData()->deductibleAmount
                    ]);

                    // Updating coins for user after redemption
                    $customer->coins = floatval($customer->coins) - $coinsRedeemed;

                    $customer->save();
                }

                Coin::addCoinForOrder($order->id, $order->user_id);

                return response()->json(['success' => true]);

            } else {

                return response()->json(['success' => false, 'error' => 'Can\'t place an order. No products found in cart.']);
            }
        }
        return response()->json(['success' => false, 'errorCode' => config('constants.ERROR_NO_DATA_FOUND'), 'error' => 'No Order found']);
    }

    public function cancelOrder(Order $order = null) {

        if($order && $order->id) {

            if($order->status != 'cancelled') {

                $order->status = 'cancelled';
                $order->save();
            }
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false, 'errorCode' => config('constants.ERROR_NO_DATA_FOUND'), 'error' => 'No Order found']);
    }

    /**
     * Show the application Pick Up List.
     *
     * @return \Illuminate\Http\Response
     */
    public function pickUpList()
    {
        // $orders = Order::where('status','process')->get();
        // if($orders) {
        //     $finalOrder = [];
        //     foreach ($orders as $p_index => $order) {
        //         $finalOrder['products'][$p_index] = Products::where('order_id',$order->id)->get()->toArray();
        //         // $finalOrder['orders'][$p_index]['customer'] = Profile::where('user_id',$order->user_id)->first();
        //     }
        //     dd($finalOrder);
        //     return view('/admin/manifest', [ 'data' => $finalOrder ]);
            
        // } else {

        //     return redirect('/admin/orders');
        // }
        $orderQuantity = DB::table('order_products')
                        ->join('orders', 'order_products.order_id', '=', 'orders.id')
                        ->where('orders.status','=','process')
                        ->select('order_products.product_id',DB::raw('SUM(order_products.quantity * order_products.per_quantity ) as Total_Buy'),DB::raw('SUM(order_products.quantity * order_products.list_price ) as total_price'),DB::raw('SUM(order_products.list_price) as total_list_price'))
                        ->groupBy('product_id')->get();
        return view('admin.pickup',compact('orderQuantity',$orderQuantity));
    }
}
