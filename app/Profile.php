<?php

namespace App;

use App\User;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Profile
 * @package App\Models\Customer
 * @version January 26, 2017, 10:53 am UTC
 */
class Profile extends Model
{
    use SoftDeletes;

    public $table = 'customer_profiles';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'address',
        'landmark',
        'pincode_id',
        'area_id',
        'phone_number',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'address' => 'string',
        'landmark' => 'string',
        'pincode_id' => 'integer',
        'area_id' => 'integer',
        'phone_number' => 'string',
        'alt_phone_number' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function country()
    {
        return $this->belongsTo(\App\Country::class);
    }

    public function referee() {

        return $this->belongsTo(User::class);
    }

}
