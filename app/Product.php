<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    public $table = 'products';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'description',
        'category_id',
        'primary_image_id',
        'price',
        'quantity',
        'url',
        'units',
        'nutrient',
        'shelf_life',
        'storage_tips',
        'storage_temperature',
        'disclaimer',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                    => 'integer',
        'name'                  => 'string',
        'description'           => 'string',
        'category_id'           => 'integer',
        'primary_image_id'      => 'integer',
        'nutrient'              => 'string',         
        'shelf_life'            => 'string',
        'url'            => 'string',
        'storage_tips'          => 'string',
        'storage_temperature'   => 'string',
        'disclaimer'            => 'string',
        'quantity'              => 'integer',
        'units'                 => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function productCategory()
    {
        return $this->belongsTo(\App\Category::class, 'category_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function productPrimaryImage()
    {
        return $this->belongsTo(\App\Image::class, 'primary_image_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function orderProducts()
    {
        return $this->hasMany(\App\Products::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function productImages()
    {
        return $this->hasMany(\App\Image::class);
    }
}
