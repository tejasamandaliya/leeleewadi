@extends('customer.layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row df-form-header" style="background: url(&quot;{{url('public/frontend-assets/images/data/background/register_banner1.jpg')}}&quot;) center center / cover no-repeat;">
        <div class="container df-form-title">
            <h2 class="pull-left">
                <i class="fa fa-chain-broken"></i>Log In
            </h2>
        </div>
    </div>
    <div class="container">
        <div class="card df-form-box  title-colorful  col-sm-10">
            <div class="">
                <form class="signup-form df-form form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="df-form-step">
                        <span class="df-form-step-title">Please Feel Details For Login</span>
                        <span class="pull-right">Don't Have Account?                                                  
                            <a class="btn btn-primary m-l-10" href="{{ url('/register') }}">Sign Up</a>
                        </span>
                    </div>
                    <div class="card-block">
                        <fieldset class="form-group">
                            <div class="col-sm-8">
                                <div>
                                    <input type="text" name="email" value="{{ old('email') }}" placeholder="*Email" class="form-control" required=""/>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="col-sm-8">
                                <div>
                                    <input type="password" name="password" value="" placeholder="*password" class="form-control" required=""/>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-large btn-success df-form-btn">
                                Login
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
