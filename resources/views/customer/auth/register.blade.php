@extends('customer.layouts.app')

@section('content')
<style type="text/css">
    @media only screen and (max-width: 600px) {
        .df-form-step {
            font-size: 16px;
        }
        .already-register{display: none;}
    }
</style>
<div class="container-fluid">
    <div class="row df-form-header" style="background: url(&quot;{{url('public/frontend-assets/images/data/background/register_banner1.jpg')}}&quot;) center center / cover no-repeat;">
        <div class="container df-form-title">
            <h2 class="pull-left">
                <i class="fa fa-chain-broken"></i>Sign Up
            </h2>
        </div>
    </div>
    <div class="container">
        
        <div class="card df-form-box title-colorful row" style="padding:5px;">
            <div class="col-sm-12">
                <form role="form" method="POST" action="{{ url('/register') }}">
                    {{ csrf_field() }}
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="df-form-step">
                        <span class="label label-success">1</span>
                        <span class="df-form-step-title">Customer Basic Information</span>
                        <span class="already-register pull-right">
                            <span>Already Registered?</span>                                         
                            <a href="{{url('/login')}}" class="btn btn-primary m-l-10">Log In</a>
                        </span>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label>First Name</label>
                                <input type="text" name="first_name" value="{{ old('first_name') }}" placeholder="*First Name" class="form-control" required="" />
                            </div>
                            <div class="col-sm-6">
                                <label>Last Name</label>
                                <input type="text" name="last_name" value="{{ old('last_name') }}" placeholder="*Last Name" class="form-control" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Email</label>
                                <input type="text" name="email" value="{{ old('email') }}" placeholder="*Email" class="form-control" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label>Mobile or Phone</label>
                                <input type="text" name="telephone" value="{{ old('telephone') }}" placeholder="*Mobile or Phone" class="form-control" required=""/>
                            </div>
                            <div class="col-sm-6">
                                <label>Alternative Contact Number</label>
                                <input type="text" name="phone_alternate" value="{{ old('phone_alternate') }}" placeholder="Alternative Contact Number" class="form-control" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label>Password</label>
                                <input type="password" name="password" value="" placeholder="*password" class="form-control" required=""/>
                            </div>
                            <div class="col-sm-6">
                                <label>Confirm Password</label>
                                <input type="password" name="password_confirmation" value="" placeholder="*Confirm Password" class="form-control" required=""/>
                            </div>
                        </div>
                    </div>
                    
                    <div class="df-form-step">
                        <span class="label label-success">2</span>
                        <span class="df-form-step-title">Customer Address Information </span>
                    </div>
                    
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Address</label>
                                <input type="text" name="address" value="{{ old('address') }}" placeholder="*Address" class="form-control" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label>Landmark</label>
                                <input type="text" name="landmark" value="{{ old('landmark') }}" placeholder="*Landmark" class="form-control" required=""/>
                            </div>
                            <div class="col-sm-6">
                                <label>Direction</label>
                                <input type="text" name="direction" value="{{ old('direction') }}" placeholder="Direction" class="form-control" required="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label>City</label>
                                <div class="form-control">Surat</div>
                            </div>
                            <div class="col-sm-6">
                                <label>State, Country</label>
                                <div class="form-control">Gujarat, INDIA</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label>Area</label>
                                <select name="area_id" class="form-control">
                                    <option value="">Select Area</option>
                                    @foreach($areas as $area)
                                        <option value="{{$area->id}}">{{$area->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label>Pincode</label>
                                <select name="pincode_id" class="form-control">
                                    <option value="">Select Pincode</option>
                                    @foreach($pincodes as $pincode)
                                        <option value="{{$pincode->id}}">{{$pincode->code}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-large btn-success df-form-btn"> Sign Up </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
