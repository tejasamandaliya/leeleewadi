@extends('customer.layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row df-form-header" style="background-image: url({{asset('public/images/checkout_background.jpg') }});background-size: cover;background-position: 50%;background-repeat: no-repeat;font-weight: 400;font-size: 13px;line-height: 21px;color: #000;">

        <div class="heading" style="margin-bottom: 10px;">
            <div class="container">
              <div class="row">
                <ul class="breadcrumb">
                  <li>
                    <a href="{{ url('/') }}">Home</a>
                  </li>
                  <li>
                    <a href="{{ url('/checkout') }}">Checkout</a>
                  </li>
                </ul>
              </div>
            </div>
        </div>

        <div class="container" style="padding: 15px 0px;margin-bottom: 0px;">
            <h2 class="pull-left">
                <!-- <i class="fa fa-user"> </i>  -->
                @if(Auth::check())
                    @if(Auth::user()->toArray()['role'] == 'Customer') 
                        {{ Auth::user()->first_name .' '. Auth::user()->last_name }} Profile
                    @endif
                @endif
            </h2>
        </div>
    </div>
</div>
<div class="checkout" style="min-height: 454px;">
  <!-- react-empty: 2684 -->
    <!-- <div class="full-row checkout-info" style="background: linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.8)) center center / cover no-repeat, url(&quot;{{url('frontend-assets/images/data/background/cuisine_background_primary.png')}}&quot;);">
        <div class="container">
            <div class="title text-center df-design-header">
                <div class="title-name"><h2><i class="fa fa-shopping-cart fa-white"></i>Checkout</h2></div>
            </div>
        </div>
    </div> -->
    <div class="checkout" >
        <div class="row m-0">
            <div class="col-lg-1 hidden-md-down"></div>
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                @if(!Auth::check())
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="checkout-account">
                            <div class="card">
                                <div class="card-header">
                                    <i class="fa fa-lock"></i>Login
                                </div>
                                <form method="POST" action="{!! url('/login') !!}" novalidate="novalidate">
                                {!! csrf_field() !!}
                                    <div class="card-block">
                                        <fieldset class="form-group">
                                            <div>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-envelope-o"></i>
                                                    </span>
                                                    <input type="text" name="email" value="" placeholder="Your Email" class="form-control"/>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <div>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-key"></i>
                                                    </span>
                                                    <input type="password" name="password" value="password" placeholder="Your Password" class="form-control"/>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary btn btn-primary">Sign In</button>
                                        <a href="{{ url('/register')}}" class="btn btn-default pull-right">Register</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $classBlockSize = 8 ?>
                @else
                <?php $classBlockSize = 12 ?>
                @endif
            
                <div class="col-lg-{{$classBlockSize}} col-md-{{$classBlockSize}} col-sm-12 col-xs-12">
              <div id="checkout-cart">
                <!-- react-text: 2725 -->
                <!-- /react-text -->
                <div class="card card-cart">
                  <div class="card-header">
                    <i class="fa fa-shopping-basket fa-white"></i>
                    Order Summary
                  </div>
                  <div class="card-body my-checkout-page-cart-table"></div>
                  <div class="card-block card-padding my-empty-checkout-page-cart-table">
                    <div class="well text-muted text-center m-t-10">
                      <i class="fa fa-shopping-basket fa-5x m-b-1"></i>
                      <h3>
                        <strong>Your cart is empty</strong>
                      </h3>You haven't added any items to your cart!
                      <br>
                      <a class="btn btn-primary hidden-xs m-t-1" href="{{url('/')}}">Continue Shopping</a>
                    </div>
                    </div>
                  <div class="card-footer my-cart-footer" style="display:none;">
                    <div class="row">
                      <div class="col-sm-12 text-right" aria-describedby="cart.popover" style="cursor: pointer; opacity: 0.65;">
                        <a class="btn btn-primary hidden-xs pull-left" href="{{url('/')}}">Continue Shopping</a>
                        
                        @if(Auth::check())
                            <a class="btn btn-success m-r-5 my-place-order">
                            <i class="fa fa-check-circle"></i>Place Order (COD)
                            </a>
                        @else
                            <a class="btn btn-success m-r-5 my-place-order" href="{{ url('/register') }}">
                                <i class="fa fa-check-circle fa-user">  </i> Please Login to Place Order (COD)
                            </a>
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="checkout-page" data-page="checkout"></div>
@endsection
@section('js-content')
<script type="text/javascript">
  $(function () {
    $('.my-cart-icon').click();
  });
</script>
@endsection
