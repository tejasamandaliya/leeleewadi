@extends('customer.layouts.app')

@section('content')
<div class="product-detail" >
    <div class="heading">
        <div class="container">
            <div class="row">
                <ul class="breadcrumb">
                    <li>
                        <a href="{{ url('/') }}">Home</a>
                    </li>
                    @foreach($categories as $category)
                        @if($category->id === $product->category_id)
                            <li>
                                <a href="{{ url('/categoris/'. $category->id) }}">{{ $category->name }}</a>
                            </li>
                        @endif
                    @endforeach
                    <li>
                        <a href="#">{{ $product->name }}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container" style="min-height: 670px;">
        <div class="row">
            <div class="card">
                <div class="col-sm-12">
                    <div class="col-sm-7">
                        <ul class="thumbnails">
                            <li>
                                <a class="" href="#">
                                    <img class="img-fluid" src="{{ $product->url }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-5">
                        <div class="product-name m-t-20">
                            <div class="pull-left">
                                <h2><b>{{ $product->name }}</b></h2>
                            </div>
                        </div>
                        <div class="product-price">
                            <h3>
                                <b>Rs. {{ $product->price }} / {{ $product->quantity }} {{$product->units}} </b>
                            </h3>
                        </div>
                        <div class="message">
                            "Delivering 
                            {{$product->name}}
                            Happiness @ Your Doorstep"
                        </div>
                        
                        <div class="info">
                            <span class="label label-success">Best Quality @ Best Price</span>
                            <span class="label label-success">FREE Home Delivery</span>
                        </div>
                        <div id="product">
                            <form name="product-option" class="form-horizontal">
                                <div class="row">
                                    <label class="col-xs-4 col-sm-4 control-label">Quantity:</label>
                                    <div class="col-xs-8 col-sm-8">
                                        <div>
                                            <input type="number" name="quantity" value="1" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row m-t-10">
                                    <div class="col-sm-12">
                                    </div>
                                </div>
                                <div class="row m-t-10">
                                    <div class="col-sm-12">
                                  <button type="button" class="btn btn-default btn-cart my-cart-btn" data-id="{{$product->id}}" data-name="{{$product->name}}" data-summary="{{$product->name}}" data-price="{{$product->price}}" data-quantity="1" data-per-quantity="{{ $product->quantity }}" data-units="{{ $product->units }}" data-image="{{ $product->url }}">
                                    <i class="fa fa-shopping-basket"></i>
                                    <span class="hidden-xs hidden-sm hidden-md">Add To Basket</span>
                                  </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="description"><div><p><br></p></div></div>            
            </div>
            <div> <h4>  <b> Features : </b> </h4> </div>
            <div class="card">
                <div class="discription">
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <tbody>
                                <tr>
                                    <td width="300px">
                                        <h5>Description</h5>
                                    </td>
                                    <td class="hidden-xs">{{ $product->description }} </td>
                                </tr>
                                <tr>
                                    <td width="300px">
                                        <h5>Nutrient Value & Benefits</h5>
                                    </td>
                                    <td class="hidden-xs">{{ $product->nutrient }} </td>
                                </tr>
                                <tr>
                                    <td width="300px">
                                        <h5>Shelf Life </h5>
                                    </td>
                                    <td class="hidden-xs">{{ $product->shelf_life }} </td>
                                </tr>
                                <tr>
                                    <td width="300px">
                                        <h5>Storage Tips </h5>
                                    </td>
                                    <td class="hidden-xs">{{ $product->storage_tips }} </td>
                                </tr>
                                <tr>
                                    <td width="300px">
                                        <h5>Storage Temperature (degC)</h5>
                                    </td>
                                    <td class="hidden-xs">{{ $product->storage_temperature }} </td>
                                </tr>
                                <tr>
                                    <td width="300px">
                                        <h5>Disclaimer</h5>
                                    </td>
                                    <td class="hidden-xs">{{ $product->disclaimer }} </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                </div> 
            </div> 
        </div>
    </div>

    <div class="container">
      <div class="product-related">
        <div class="card-title">
          <h2>
            <b> Related Product </b>
          </h2>
        </div>
        <div class="row">
            @foreach($randomProducts as $randomProduct)
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
                    <div class="product-layout">
                        <div class="product-thumb transition">
                            <div class="image">
                                <a href="{{ url('/products/'. $randomProduct->id) }}">

                                    <img class="img-fluid" src="{{ $randomProduct->url }}" alt="{{ $randomProduct->name }}" title="{{ $randomProduct->name }}">
                                </a>
                            </div>
                            <div class="caption">
                                <div class="title">
                                    <h4>
                                        <a href="{{ url('/products/'. $randomProduct->id) }}">{{ $randomProduct->name }}</a>
                                    </h4>
                                </div>
                                <b class="text-center"></b>
                                <div class="product-info">
                                    <p class="price">
                                        <b> Rs. {{ $randomProduct->price }} / 500 Grams </b>
                                    </p>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-add-cart my-cart-btn" data-id="{{ $randomProduct->price }}" data-name="{{ $randomProduct->name }}" data-summary="{{ $randomProduct->name }}" data-price="{{ $randomProduct->price }}" data-quantity="1" data-image="{{ $randomProduct->url }}">
                                <i class="fa fa-shopping-basket"></i>
                                <span class="hidden-xs hidden-sm hidden-md">Add to Basket</span>
                            </button>
                        </div>
                    </div>
                </div>                       
            @endforeach
        </div>
      </div>
    </div>
</div>    
@endsection