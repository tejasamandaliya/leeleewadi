<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>{{ config('app.name') }}</title>

    <link  rel="icon" href="asset('public/images/favicon.png')}}"/>
    <link href="{{asset('public/images/favicon.png')}}" rel="icon" ></link>
    <script src="{{asset('public/frontend-assets/js/jquery-2.2.3.min.js')}}"></script>
    <script src="{{asset('public//frontend-assets/js/tether-1.2.2.min.js')}}"></script>
    <script src="{{asset('public//frontend-assets/js/bootstrap-4.0.0-alpha.2.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('public//frontend-assets/fonts/font-awesome/4.5.0/css/font-awesome.min.css')}}">

    <link href='https://fonts.googleapis.com/css?family=Fredoka+One' rel='stylesheet' type='text/css'>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />    
    <link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('public/frontend-assets/css/styles.css')}}">
    
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <style type="text/css">
        @media only screen and (max-width: 200px) {
            .product-thumb{
                display: block;
                width: 100%;
            }
        }
        @media (max-width: 201px) and (max-width: 300px) {
            .product-thumb{
                display: block;
                height: 125px;
            }
        }

        @media (max-width: 500px){
            .product-layout{
                min-height: 250px
            }

            .product-layout .my-cart-btn{
              position: absolute;
              bottom: 0px;
            }
        }

        @media (max-width: 1024px) and (min-width: 768px) {
            .product-layout{
                min-height: 250px
            }

            .product-layout .my-cart-btn{
              position: absolute;
              bottom: 0px;
            }
        }



       
    </style>
    
  </head>
  <body>
    <div>
      <div>
        <header class="header" >
          <span ></span>
          <!-- react-text: 4 -->
          <!-- /react-text -->
          <div class="hidden-md-down" >
            <div class="headerTop" >
              <div class="container-fluid" >
                <div class="col-sm-4 col-md-6" >
                  <div class="pull-left" >Delivering Fruits &amp; Vegetables @ Doorstep</div>
                </div>
                <div class="col-sm-6" >
                  <ul class="nav navbar-nav pull-right" >
                    <li class="nav-item hidden-md-down" >
                      <a class="nav-link" href="#" >
                        Need Help ?  <i class="fa fa-phone fa-lg" style="margin-left: 5px "></i>709-66666-47 <i style="margin-left: 5px " class="fa fa-whatsapp fa-lg"></i>  709-66666-48
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="headerMain" >
              <div class="container-fluid" >
                <div class="col-sm-2 col-md-2" >
                  <div id="logo" >
                    <a href="{{ url('/') }}" >
                      <img title="Leeleewadi" alt="Leeleewadi" class="img-fluid" src="{{asset('public/images/logo.png')}}" />
                    </a>
                  </div>
                </div>
                <div class="col-sm-6 col-md-7 col-lg-7" >
                  <ul class="nav navbar-nav category-nav" >
                    @foreach($categories as $category)
                    <li class="nav-item">

                      <a class="nav-link" style="color:#f17525" href="{{ url('/categoris/'. $category->id) }}" >{{ $category->name }}</a>
                    </li>
                    @endforeach
                  </ul>
                </div>
                <div class="col-sm-4 col-md-3 col-lg-3 header-main-right" >
                  <ul class="nav navbar-nav pull-right" >
                    @if (Auth::guest())
                        <li class="nav-item dropdown header-customer" >
                            <a class="nav-link" href="#" data-toggle="modal" data-target="#loginModal">
                                <span class="hidden-md-down" style="color: black" >Login </span>
                                <i class="fa fa-user fa-lg fa-white" ></i>
                            </a>
                        </li>
                    @else
                        <li class="nav-item header-customer dropdown dropdown-user">
                            <a class="nav-link" href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                       data-close-others="true">
                                <span class="hidden-md-down" style="color: black;">{{ Auth::user()->first_name .' '.Auth::user()->last_name }}</span>
                                <i class="fa fa-angle-down" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                            </a>
                            <div class="dropdown-menu" style="width: 100%;padding-top: 1px;border-radius: 0px;">
                              <a class="dropdown-item" href="{!! url('/orders') !!}">My Orders</a>
                              <a class="dropdown-item" href="{!! url('/profile') !!}">My Profile</a>
                              <a class="dropdown-item" href="{!! url('/logout') !!}"> Log Out</a>
                            </div>
                        </li>
                        <!-- <ul class="dropdown-menu dropdown-menu-default">
                            <li class="divider"></li>
                            <li>    
                                <a href="{!! url('/logout') !!}">
                                    <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul> -->
                    @endif
                    <li class="nav-item header-cart" >
                      <div class="header-cart" >
                        <button class="btn nav-link my-cart-icon" >
                          <i class="fa fa-shopping-basket cart-icon" ></i>
                        </button>
                        <div class="dropdown-menu1" >
                          <div class="card-header cart-close-header" >
                            <span style="margin-top:5px;" class="pull-left" >Your Cart</span>
                            <button class="btn btn-cart-close pull-right" >
                              <span class="fa fa-times" ></span>
                              <!-- react-text: 83 --> Close
                              <!-- /react-text -->
                            </button>
                          </div>
                          <div class="cart" >
                            <div class="card-product scrollbar" >
                              <div class="well text-muted text-center cart-empty m-t-10" >
                                <i class="fa fa-shopping-basket fa-5x m-b-1" ></i>
                                <h3 >
                                  <strong >Your cart is empty</strong>
                                </h3>
                                  You                                 
                                  haven&#x27;t
                                  added any items to your cart!
                                <br />
                                <button class="btn btn-primary hidden-xs m-t-1" >Continue Shopping</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="cart_overlay" ></div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="row" id="search-bar" >
            <div class="col-xs-10 col-lg-8 col-lg-offset-2" >
              <div class="search-block" >
                <div class="row" >
                  <div class="input-group" >
                    <input type="text" name="search" placeholder="Search by Vegetable Or Fruit .." class="form-control" />
                    <span class="input-group-btn" >
                      <button type="button" class="btn btn-danger" >Search</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-2 text-right" >
              <div class="btn-close" >
                <a class="collapsed" >
                  <i class="fa fa-times" ></i>
                </a>
              </div>
            </div>
          </div>
          <nav id="menu" class="navbar hidden-lg-up" >
            <div class="navbar-header" >
              <div class="row m-0" >
                <div class="col-xs-10 text-center" >
                  
                  <span>
                    <button type="button" class="btn btn-navbar navbar-toggle pull-left" data-toggle="collapse" data-target=".navbar-ex1-collapse" >
                      <i class="fa fa-bars" ></i>
                    </button>
                  </span>
                  <span>
                    <a href="#" >
                      <img title="Leeleewadi" alt="Leeleewadi" class="img-fluid" src="{{asset('public/images/mobilelogo.png')}}" />
                    </a>
                  </span>
                
                </div>
                <div class="col-xs-2" >
                  <ul class="nav navbar-nav pull-right" >
                    @if (Auth::guest())
                    <li class="nav-item dropdown header-customer" >
                      <a class="nav-link" style="color: #f17525" href="#" data-toggle="modal" data-target="#loginModal">
                        <span class="hidden-md-down" >Login </span>
                        <i class="fa fa-user fa-lg fa-white" ></i>
                      </a>
                    </li>
                    @else
                      <li class="nav-item dropdown header-customer dropdown dropdown-user">
                            <a class="nav-link" href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                       data-close-others="true">
                                <span class="hidden-md-down" style="color: black;">{{ Auth::user()->first_name .' '.Auth::user()->last_name }}</span>
                                <i class="fa fa-angle-down" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                            </a>
                            <div class="dropdown-menu" style="width: 100%;padding-top: 1px;border-radius: 0px;">
                              <a class="dropdown-item" href="{!! url('/profile') !!}">My Profile</a>
                              <a class="dropdown-item" href="{!! url('/logout') !!}"> Log Out</a>
                            </div>
                        </li>
                    @endif
                    <li class="nav-item header-cart" >
                      <div class="header-cart"  >
                        <button class="btn nav-link my-cart-icon"  >
                          <i class="fa fa fa-shopping-cart cart-icon" ></i>
                        </button>
                        <div class="dropdown-menu1" >
                          <div class="card-header cart-close-header" >
                            <span style="margin-top:5px;" class="pull-left" >Your Cart</span>
                            <button class="btn btn-cart-close pull-right"  >
                              <span class="fa fa-times" ></span>
                               Close
                            </button>
                          </div>
                          <div class="cart" >
                            <div class="card-product scrollbar" >
                              <div class="well text-muted text-center cart-empty m-t-10" >
                                <i class="fa fa-shopping-basket fa-5x m-b-1" ></i>
                                <h3 >
                                  <strong >Your cart is empty</strong>
                                </h3>
                                You haven&#x27;t added any items to your cart!
                                <br />
                                <button class="btn btn-primary hidden-xs m-t-1" >Continue Shopping</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="cart_overlay" ></div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
              <ul class="nav navbar-nav">
                @foreach($categories as $category)
                <li >
                  <a class="nav-link" href="{{ url('/categoris/'. $category->id) }}">{{ $category->name }}</a>
                </li>
                @endforeach
              </ul>
            </div>
          </nav>
        </header>
        
        @yield('content')

        <div class="footer">
          <div class="hidden-md-down" >
            <div class="container" >
              <div class="purchase" >
                
              </div>
              <div class="row m-t-20" >
                <div class="col-sm-4 col-lg-4" >
                  <h4 >Leeleewadi</h4>
                  <!-- <div >
                    <div class="load_small load_id" >
                      <div class="load_wht" >
                        <ul class="bokeh" >
                          <li ></li>
                          <li ></li>
                          <li ></li>
                          <li ></li>
                        </ul>
                      </div>
                    </div>
                  </div> -->
                </div>
                <div class="col-sm-4 col-lg-4" >
                  <h4 >Category</h4>
                  <ul class="list-unstyled" >
                    @foreach($categories as $category)
                    <li >
                      <a href="{{ url('/categoris/'. $category->id) }}" >{{ $category->name }}</a>
                    </li>
                    @endforeach
                  </ul>
                </div>
                <!-- <div class="col-sm-3 col-lg-3" >
                  <h4 >Other @ Leeleewadi</h4>
                  <ul class="list-unstyled" >
                    <li >
                      <a target="_blank" href="https://leeleewadi.com/" >Leeleewadi</a>
                    </li>
                    <li >
                      <a target="_blank" href="https://leeleewadi.com" >Leeleewadi Cakes</a>
                    </li>
                    <li >
                      <a target="_blank" href="https://leeleewadi.com" >Food Collections</a>
                    </li>
                    <li >
                      <a target="_blank" href="https://leeleewadi.com" >Food Deals/Offers</a>
                    </li>
                  </ul>
                </div> -->
                <div class="col-sm-4 col-lg-4" >
                  <h4 >Contact &amp; Help</h4>
                  <ul class="list-unstyled" >
                    <li >
                      <a href="help.html" >Help / Contact Us</a>
                    </li>
                    <!-- <li >
                      <a href="contact.html" >Contact Us</a>
                    </li>
                    <li >
                      <span >Any Suggestions</span>
                    </li> -->
                  </ul>
                </div>
              </div>
            </div>
            <div class="footer-feature" >
              <div class="container" >
                <div class="row" >
                  <!--<div class="col-sm-3 col-md-4 col-lg-4" >
                     <a href="app.html" >
                      <label class="app-label" >Mobile Apps</label>
                      <span class="app-icon" to="/app" >
                        <i class="fa fa-apple m-l-10 m-r-10" ></i>
                        <i class="fa fa-android" ></i>
                      </span>
                    </a> 
                  </div>-->
                  <div class="col-sm-4 col-md-4 col-lg-4" >
                    <label class="follow-us-label" >Follow Us</label>
                    <a class="follow-us" target="_blank" href="#" >
                      <i class="fa fa-facebook-square" ></i>
                    </a>
                    <a class="follow-us" target="_blank" href="#" >
                      <i class="fa fa-google-plus-square" ></i>
                    </a>
                    <a class="follow-us" target="_blank" href="#" >
                      <i class="fa fa-twitter-square" ></i>
                    </a>
                    <a class="follow-us" target="_blank" href="#" >
                      <i class="fa fa-linkedin-square" ></i>
                    </a>
                    <a class="follow-us" target="_blank" href="#" >
                      <i class="fa fa-youtube-square" ></i>
                    </a>
                    <a class="follow-us" target="_blank" href="#" >
                      <i class="fa fa-instagram" ></i>
                    </a>
                  </div>
                  <div class="col-sm-8 col-md-8 col-lg-8 text-right" >
                    <a class="btn btn-default-special btn-sm m-r-5" href="#" >
                      <i class="fa fa-phone fa-lg" style="margin-left: 5px "></i>709-66666-47 
                    </a>
                    <a class="btn btn-default-special btn-sm m-r-5" href="#" >
                      <i style="margin-left: 5px " class="fa fa-whatsapp fa-lg"></i>  709-66666-48
                    </a>
                    <a target="_blank" class="btn btn-default-special btn-sm" href="mailto:order@leeleewadi.com?subject=Info Home leeleewadi?" >
                      <i class="fa fa-envelope fa-lg fa-white" ></i>
                      order@leeleewadi.com
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="copy-right text-center" >
              <div class="container" >
                <span class="pull-left" >Leeleewadi © 2017, All Rights Reserved.</span>
                <span class="pull-right" >
                  Made with Love @ SURAT, INDIA by 
                  <a target="_blank" href="#" >Prizma Infotech</a>. Make In India.
                </span>
              </div>
            </div>
          </div>
          <div class="mobile-footer hidden-lg-up">
            <div class="container-fluid">
              <div class="row">
                <div class="col-xs-12">
                  <div role="tablist" class="panel-group">
                    <div icon="fa fa-chevron-down" class="panel panel-default">
                      <div class="panel-heading">
                        <h4 role="presentation" class="panel-title">
                          <a role="tab" aria-expanded="false" aria-selected="false" class="collapsed" >Leeleewadi</a>
                        </h4>
                      </div>
                    </div>
                  </div>
                  <div role="tablist" class="panel-group" >
                    <div icon="fa fa-chevron-down" class="panel panel-default" >
                      <div class="panel-heading">
                        <h4 role="presentation" class="panel-title" >
                          <a role="tab" aria-expanded="false" aria-selected="false" class="collapsed" >Category</a>
                        </h4>
                      </div>
                    </div>
                  </div>
                  <div role="tablist" class="panel-group" >
                    <div icon="fa fa-chevron-down" class="panel panel-default" >
                      <div class="panel-heading" >
                        <h4 role="presentation" class="panel-title" >
                          <a role="tab" aria-expanded="false" aria-selected="false" class="collapsed">Contact &amp; Help</a>
                        </h4>
                      </div>
                      <div role="tabpanel" class="panel-collapse collapse" aria-hidden="true" >
                        <div class="panel-body" >
                          <ul class="list-unstyled" >
                            <li >
                              <a href="help.html" >Help / FAQs</a>
                            </li>
                            <li >
                              <a href="contact.html">Contact Us</a>
                            </li>
                            <li >
                              <span >Any Suggestions</span>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="react-redux-modal" >
          <div class="rr-modals" ></div>
        </div>
      </div>
    </div>
    
    <!-- Modal -->
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div id="login-overlay" class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                  <h4 class="modal-title" id="myModalLabel">Login to leeleewadi.com</h4>
              </div>
              <div class="modal-body">
                  <div class="row">
                      <div class="col-xs-6">
                          <div class="well">
                              <form id="loginForm" method="POST" action="{!! url('/login') !!}" novalidate="novalidate">
                              {!! csrf_field() !!}
                                  <div class="form-group">
                                      <label for="username" class="control-label">Email</label>
                                      <input type="text" class="form-control" id="username" name="email" value="" required="" title="Please enter you username" placeholder="example@gmail.com">
                                      <span class="help-block"></span>
                                  </div>
                                  <div class="form-group">
                                      <label for="password" class="control-label">Password</label>
                                      <input type="password" class="form-control" id="password" name="password" value="" required="" title="Please enter your password">
                                      <span class="help-block"></span>
                                  </div>
                                  <div class="checkbox">
                                      <label>
                                          <input type="checkbox" name="remember" id="remember"> Remember login
                                      </label>
                                      <p class="help-block">(if this is a private computer)</p>
                                  </div>
                                  <button type="submit" class="btn btn-success btn-block">Login</button>
                                  <!-- <a href="/forgot/" class="btn btn-default btn-block">Help to login</a> -->
                              </form>
                          </div>
                      </div>
                      <div class="col-xs-6">
                          <p class="lead">Register now for <span class="text-success">FREE</span></p>
                          <ul class="list-unstyled" style="line-height: 2">
                              <li><span class="fa fa-check text-success"></span> See all your orders</li>
                              <li><span class="fa fa-check text-success"></span> Fast re-order</li>
                              <li><span class="fa fa-check text-success"></span> Fast checkout</li>
                              <!-- <li><a href="/read-more/"><u>Read more</u></a></li> -->
                          </ul>
                          <p><a href="/register/" class="btn btn-info btn-block">Yes please, register now!</a></p>
                      </div>
                  </div>
              </div>
          </div>
        </div>
    </div>

    <script src="{{asset('public/frontend-assets/js/jquery.mycart.js')}}"></script>
    <script src="{{asset('public/frontend-assets/js/jquery.mycart.custom.js')}}"></script>
    @yield('js-content')
  </body>
</html>
  