@extends('customer.layouts.app')

@section('content')

<div class="container-fluid " style="min-height: 594px;">
    <div class="row df-form-header" style="background-image: url({{asset('public/frontend-assets/images/image/account_background.jpg') }});background-size: cover;background-position: 50%;background-repeat: no-repeat;font-weight: 400;font-size: 13px;line-height: 21px;color: #000;">
        
        <div class="heading" style="margin-bottom: 10px;">
            <div class="container">
              <div class="row">
                <ul class="breadcrumb">
                  <li>
                    <a href="{{ url('/') }}">Home</a>
                  </li>
                  <li>
                    <a href="{{ url('/profile') }}">My Account</a>
                  </li>
                  <li>
                    <a href="#">Order History</a>
                  </li>
                </ul>
              </div>
            </div>
        </div>
        <div class="container" style="padding: 15px 0px;margin-bottom: 0px;">
            <h3 class="bigger">
               {{ Auth::user()->first_name .' '. Auth::user()->last_name }} Profile 
                <a href="{{ url('/profile') }}" class="btn btn-white btn-sm b-radius-50 m-r-10"><i class="fa fa-pencil"></i></a>
            </h3>
            <div class="btn-group navigation" style="margin-bottom: 5px;">
                <a class="btn btn-default" href="{{url('/home')}}">Home</a>
                <a class="btn btn-default" href="{{url('/orders')}}">Orders</a>
                <a class="btn btn-default" href="{{url('/addresses')}}">Addresses</a>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="">
            <div class="row">
                @if(Session::has('success'))
                    <div class="col-md-11 alert alert-info">
                        {{ Session::get('success') }}
                    </div>
                @elseif(Session::has('warning'))
                    <div class="col-md-11 alert alert-warning">
                        {{ Session::get('warning') }}
                    </div>
                @endif
                @if (count($errors) > 0)
                    @foreach ($errors->all() as $error)
                        <div class="col-md-11 alert alert-warning">{{ $error }}</div>
                    @endforeach
                @endif
                <p id="products" class="page_type"></p>
                <p class="update_status"></p>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 df-content-box">
                <div class="card">
                    <div class="card-header"><i class="fa fa-building-o"></i>
                        Order List
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table">
                            <thead class="thead-default">
                                <tr>
                                    <th>Order Id</th>
                                    <th>Date</th>
                                    <th>Order Status</th>
                                    <th>Total</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($orderList as $order)
                                <tr>
                                    <th scope="row">{{$order->id}}</th>
                                    <td>{{ $order->updated_at }}</td>
                                    <td>{{ $order->status }}</td>                  
                                    <td>Rs.{{ $order->sale_price }}
                                    </td>
                                    <td>
                                        <a class="btn btn-primary" href="{{ url('/order/view/'.$order->id) }}">View</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
