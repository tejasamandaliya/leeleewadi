@extends('customer.layouts.app')

@section('content')
 <div class="home-container" >
  <!-- react-empty: 170 -->
  <div class="home-heading" style="background:linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url({{asset('public/images/main.jpg') }}) " >
  <!-- no-repeat scroll center center / cover rgba(0, 0, 0, 0); -->
    <div class="container-fluid" >
      <div class="row" >
        <div class="col-xs-12 col-sm-12" >
          <div class="intro-message" >
            <div class="title" >
              Delivering the Freshness to Your Home
              <span style="color:inherit;" >
                <span class="react-rotating-text-cursor" >|</span>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container" >
      <div class="row" >
        <div class="col-sm-12" >
          <div class="delivery-city" >
            <span >Fresh Delivery @ Surat</span>
          </div>
            <div class="row" >
                <div class="col-sm-1 col-md-1 col-lg-2 hidden-xs-down" ></div>
                @foreach($categories as $category)
                <div class="col-xs-6 col-sm-5 col-md-5 col-lg-4" >
                    <a href="{{ url('/categoris/'. $category->id) }}" >
                        <img alt="" src="{{ $category->url }}" class="img-fluid portfolio-item" style="height: 207px;width: 100%;" />
                    <div class="img-title" >{{ $category->name }}</div>
                    </a>
                </div>
                @endforeach
                <div class="col-sm-1 col-md-1 col-lg-2 hidden-xs-down" ></div>
            </div>
            <div class="delivery-content" >
                <div class="row" >
                    <div class="title col-xs-3 col-sm-3 col-xs-6" >FREE
                        <br />Delivery
                    </div>
                    <div class="title col-xs-3 col-sm-3 col-xs-6" >Best
                        
                        <br />Quality
                    </div>
                    <div class="title col-xs-3 col-sm-3 col-xs-6" >Best
                        <br />Price
                    </div>
                    <div class="title col-xs-3 col-sm-3 col-xs-6" >Easy
                        <br />Ordering
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <div class="home-app" >
    <div class="container" >
      
      <div class="row home-app-row" >
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" >
          <img class="img-fluid" src="{{asset('public/images/f3-sider.jpg')}}" />
        </div>
        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12" >
          <h4 class="df-link" >FRESH Food Market <small>on your finger tip</small></h4>
          <p class="margin-top-20" >
            <i class="fa fa-arrow-right" ></i>
            Order Your All Vegetables & Fruits 
          </p>
          <p >
            <i class="fa fa-arrow-right" ></i>
            Personalize Your Food Ordering Experience
            
          </p>
          <p >
            <i class="fa fa-arrow-right" ></i>
            Get Real-Time Order Updates
            
          </p>
          <p >
            <i class="fa fa-arrow-right" ></i>
            Fresh super market at your finger tip
            
          </p>
          <div class="row m-t-2" >
            <!-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-3 col-xs-6 col-app-link p-0" >
              <a target="_blank" href="https://itunes.apple.com/us/app/delfoofresh/id1198206258?mt=8" >
                <img src="frontend-assets/images/data/apps/app-store.png" class="img-fluid" />
              </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 p-0" >
              <a target="_blank" href="https://play.google.com/store/apps/details?id=com.delfoofresh&amp;hl=en" >
                <img src="frontend-assets/images/data/apps/google-play.png" class="img-fluid" /> 
              </a>
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection