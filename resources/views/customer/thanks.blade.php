@extends('customer.layouts.app')

@section('content')

<div class="home-container" style="min-height: 530px;">
 
  <div class="home-app" >
    <div class="container" >
      
      <div class="row home-app-row" >
       <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12" >
          <h4 class="df-link" >Your order is placed sucessfully. <small><a href="{{ url('/orders')}}">Your Order</a></small></h4>
          <p class="margin-top-20" >
            <i class="fa fa-arrow-right" ></i>
            You can check your order status By clicking
          </p>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
