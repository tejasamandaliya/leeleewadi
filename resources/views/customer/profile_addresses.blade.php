@extends('customer.layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row df-form-header" style="background-image: url({{asset('public/frontend-assets/images/image/account_background.jpg') }});background-size: cover;background-position: 50%;background-repeat: no-repeat;font-weight: 400;font-size: 13px;line-height: 21px;color: #000;">

        <div class="heading" style="margin-bottom: 10px;">
            <div class="container">
              <div class="row">
                <ul class="breadcrumb">
                  <li>
                    <a href="{{ url('/') }}">Home</a>
                  </li>
                  <li>
                    <a href="{{ url('/addresses') }}">My Addresses</a>
                  </li>
                </ul>
              </div>
            </div>
        </div>

        <div class="container" style="padding: 15px 0px;margin-bottom: 0px;">
            <h3 class="bigger">
               {{ Auth::user()->first_name .' '. Auth::user()->last_name }} Profile 
                <a href="{{ url('/profile') }}" class="btn btn-white btn-sm b-radius-50 m-r-10"><i class="fa fa-pencil"></i></a>
            </h3>
            <div class="btn-group navigation" style="margin-bottom: 5px;">
                <a class="btn btn-default" href="{{url('/home')}}">Home</a>
                <a class="btn btn-default" href="{{url('/orders')}}">Orders</a>
                <a class="btn btn-default" href="{{url('/addresses')}}">Addresses</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="card df-form-boxtitle-colorful">
            <div class="row">
                @if(Session::has('success'))
                    <div class="col-md-11 alert alert-info">
                        {{ Session::get('success') }}
                    </div>
                @elseif(Session::has('warning'))
                    <div class="col-md-11 alert alert-warning">
                        {{ Session::get('warning') }}
                    </div>
                @endif
                @if (count($errors) > 0)
                    @foreach ($errors->all() as $error)
                        <div class="col-md-11 alert alert-warning">{{ $error }}</div>
                    @endforeach
                @endif
                <p id="products" class="page_type"></p>
                <p class="update_status"></p>
            </div>
          
            <div class="row">
                <div class="col-lg-2 "></div>
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 df-content-box">
                    <div class="card">
                        <div class="card-header"><i class="fa fa-home"></i>
                            Addresses
                            <div class="card-heading-link pull-right">
                                <span><i class="fa fa-plus-circle"></i>New Address</span>
                            </div>
                        </div>
                        <div class="card-body" id="address-blog">
                            <div class="row m-0">
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                    <div class="card-block bg-white">
                                        <div class="top-title m-b-10">
                                            <b>Shivang Koshia</b>
                                            <label class="label label-warning pull-right" style="padding-top: 4px;">
                                                <i class="fa fa-briefcase"></i>
                                                 Office
                                            </label>
                                        </div>
                                        <div class="info">
                                            <div>
                                                03-Sanskrut Complex,, Causeway Road,, Near haridarshan khado,, manibaa school pase,, Surat-395004<br>
                                                <div>
                                                Gujarat
                                                <br>
                                                </div>
                                                Ved Road (395004)<br>Surat, Gujarat, India <br>
                                            </div>
                                            <div class="buttons">
                                                <button class="btn btn-default btn-xs"><i class="fa fa-edit"></i>
                                                 Edit</button>
                                                 <button class="btn btn-default btn-xs pull-right"><i class="fa fa-trash-o"></i> Delete</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                    <div class="card-block bg-white">
                                        <div class="top-title m-b-10">
                                            <b>Shivang   Koshia</b>
                                            <label class="label label-default pull-right" style="padding-top: 4px; margin-left: 5px;">Default</label>
                                            <label class="label label-warning pull-right" style="padding-top: 4px;"><i class="fa fa-home"></i> Home </label>
                                        </div>
                                        <div class="info">
                                            <div>
                                                Blok No -10 Tapidarshan Soc 
                                                <br>
                                                <div>
                                                    Singanpur Road<br>
                                                </div>
                                                <div>
                                                    Near. Bharat Gas<br>
                                                </div>
                                                Ved Road (395004)<br>
                                                Surat, Gujarat, India <br>
                                            </div>
                                            <div class="buttons">
                                                <button class="btn btn-default btn-xs">
                                                    <i class="fa fa-edit"></i> Edit
                                                </button>
                                                <button class="btn btn-default btn-xs pull-right">
                                                    <i class="fa fa-trash-o"></i> Delete
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div></div></div></div>
        </div>
    </div>
</div>

@endsection
