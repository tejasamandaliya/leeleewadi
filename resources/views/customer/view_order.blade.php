@extends('customer.layouts.app')

@section('content')

<div class="container-fluid" style="min-height: 594px;">
    <div class="row df-form-header" style="background-image: url({{asset('public/frontend-assets/images/image/account_background.jpg') }});background-size: cover;background-position: 50%;background-repeat: no-repeat;font-weight: 400;font-size: 13px;line-height: 21px;color: #000;">

        <div class="heading" style="margin-bottom: 10px;">
            <div class="container">
              <div class="row">
                <ul class="breadcrumb">
                  <li>
                    <a href="{{ url('/') }}">Home</a>
                  </li>
                  <li>
                    <a href="{{ url('/orders') }}">My Orders</a>
                  </li>
                  <li>
                    <a href="#">Order Information</a>
                  </li>
                </ul>
              </div>
            </div>
        </div>
        <div class="container" style="padding: 15px 0px;margin-bottom: 0px;">
            <h3 class="bigger">
                {{ Auth::user()->first_name .' '. Auth::user()->last_name }} Profile 
                <a href="{{ url('/profile') }}" class="btn btn-white btn-sm b-radius-50 m-r-10"><i class="fa fa-pencil"></i></a>
            </h3>
            <div class="btn-group navigation" style="margin-bottom: 5px;">
                <a class="btn btn-default" href="{{url('/home')}}">Home</a>
                <a class="btn btn-default" href="{{url('/orders')}}">Orders</a>
                <a class="btn btn-default" href="{{url('/addresses')}}">Addresses</a>
            </div>
        </div>
    </div>

    <div class="container" data-ng-app="CustomerOrder">
        <div class="card df-form-boxtitle-colorful" ng-controller="ViewOrderController">
            <div class="row">
                @if(Session::has('success'))
                    <div class="col-md-11 alert alert-info">
                        {{ Session::get('success') }}
                    </div>
                @elseif(Session::has('warning'))
                    <div class="col-md-11 alert alert-warning">
                        {{ Session::get('warning') }}
                    </div>
                @endif
                @if (count($errors) > 0)
                    @foreach ($errors->all() as $error)
                        <div class="col-md-11 alert alert-warning">{{ $error }}</div>
                    @endforeach
                @endif
                <p id="products" class="page_type"></p>
                <p class="update_status"></p>
            </div>
            <div class="container-fluid order-box">
            <div class="row">
                <div class="heading">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="title pull-left">
                            <h3 style="color: #FFF"><i style="color: #FFF" class="fa fa-truck"> </i> Order Information</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 df-content-box">
                    <div class="card order-info">
                        <div class="card-header"> Order Information </div>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="row field-content">
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">Order ID:</div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">{{ $finalOrder['id']}}</div>
                                    </div>
                                    <div class="row field-content">
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">Date Added:</div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">{{ $finalOrder['updated_at']}}</div>
                                    </div>
                                    <div class="row field-content">
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">Payment Method:</div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">Cash on delivery</div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="row field-content">
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-4">Email :</div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">{{ Auth::user()->email }}</div>
                                    </div>
                                    <div class="row field-content">
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">Telephone :</div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">{{ Auth::user()->customerProfile->phone_number }}</div>
                                    </div>
                                    {{--<div class="row field-content">
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">Delivery Date :</div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">- 27-May-2017</div>
                                    </div>
                                    <div class="row field-content">
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">Delivery Time :</div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">- 15:00:00</div>
                                    </div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row m-0">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0">
                            <div class="card">
                                <div class="card-header"><i class="fa fa-map-marker"></i>
                                    <!-- react-text: 794 -->Delivery Address
                                    <!-- /react-text -->
                                </div>
                                <div class="card-block">
                                    <div>
                                        {{ Auth::user()->first_name . ' ' . Auth::user()->last_name }}
                                        <br>
                                        {{ Auth::user()->customerProfile->address }}
                                        <br>
                                        Surat, Gujarat
                                        <br>
                                        India
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-r-0">
                            <div class="card">
                                <div class="card-header"><i class="fa fa-credit-card"></i>
                                    <!-- react-text: 814 -->Payment Address
                                    <!-- /react-text -->
                                </div>
                                <div class="card-block">
                                    <div>
                                        {{ Auth::user()->first_name . ' ' . Auth::user()->last_name }}
                                        <br>
                                        {{ Auth::user()->customerProfile->address }}
                                        <br>
                                        Surat, Gujarat
                                        <br>
                                        India
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            Product Detail
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-left">Product Name</th>
                                        <th class="text-right">Unit</th>
                                        <th class="text-right">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($finalOrder['products'] as $order)
                                        <tr>
                                            <td class="text-left">
                                                {{$order['name']}}
                                            </td>
                                            <td class="text-right">{{$order['quantity']}} {{$order['units']}} 
                                            </td>
                                            <td class="text-right"><i class="fa fa-inr"></i>{{$order['list_price']}} 
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="1"></td>
                                        <td class="text-right">Total</td>
                                        <td class="text-right"><i class="fa fa-inr"></i>{{$finalOrder['list_price']}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
        </div>

    </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript" src="/mt/assets/global/plugins/angularjs/angular.min.js"></script>
<script type="text/javascript" src="/mt/assets/global/plugins/angularjs/angular-sanitize.min.js"></script>
<script type="text/javascript">
    (function() {

        var CustomerOrder = angular.module('CustomerOrder', ['ngSanitize'], function ($interpolateProvider) {

            $interpolateProvider.startSymbol('<%');
            $interpolateProvider.endSymbol('%>');

        });

        CustomerOrder.controller('ViewOrderController', function($scope, $http) {

            $http.get("{{ url('/customer/orders/detail/' . $order_id) }}").then(function(success) {

                success = success.data;

                if(success.success) {

                    $scope.Cart = success.data.products;
                    $scope.storeInfo = success.data.store;
                    $scope.subTotal = success.data.list_price;
                    $scope.totalDiscount = success.data.discount;
                    $scope.coinDiscount = success.data.coin_amount;

                } else {

                    alert('Order not found in system. Now you will be redirected to order listing');
                    window.location = '{{ url('/customer/orders') }}';
                }

            }, function(error) {

                console.log(error);
                alert('Something went wrong. Can\'t load order details');
                window.location = '{{ url('/customer/orders') }}';
            });
        });
    })(angular);
</script>
@endsection
