@extends('customer.layouts.app')
@section('content')
<div class="category-detail" style="background: url({{asset('public/images/main.jpg') }});padding-top: 0px;">

  <div class="heading">
    <div class="container">
      <div class="row">
        <ul class="breadcrumb">
          <li>
            <a href="{{ url('/') }}">Home</a>
          </li>
          <li>
            <a href="#">List</a>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <div class="container" style="min-height: 670px;">
    <div class="card-category-product">
      <!-- <div class="row search-vendor-app-banner hidden-sm-down">
        <div class="col-sm-12">
          <a href="/app">
            <img class="img-responsive" src="https://image.delfoo.com/data/banner/delfoo_fresh_mobile_app.jpg">
          </a>
        </div>
      </div> -->
      <div class="row">
        @if(isset($productLists) && count($productLists) !=0)
            @foreach($productLists as $product)
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
                    <div class="product-layout">
                    <div class="product-thumb transition">
                      <div class="image">
                        <a href="{{ url('/products/'. $product->id) }}">
                          <img class="img-fluid" src="{{ $product->url }}" alt="{{ $product->name }}" title="{{ $product->name }}">
                        </a>
                        <div class="cart-orders cart-orders-{{$product->id}}"></div>
                      </div>
                      <div class="caption">
                        <div class="title">
                          <h4>
                            <a href="{{ url('/products/'. $product->id) }}">{{ $product->name }}</a>
                          </h4>
                        </div>
                        <b class="text-center"></b>
                        <div class="product-info">
                          <p class="price"><b>Rs. {{ $product->price }} / {{ $product->quantity }} {{$product->units}}</b></p>
                        </div>
                      </div>
                      
                      <button type="button" class="btn btn-add-cart my-cart-btn" data-id="{{ $product->id }}" data-name="{{ $product->name }}" data-summary="{{ $product->name }}" data-price="{{ $product->price }}" data-quantity="1" data-per-quantity="{{ $product->quantity }}" data-units="{{ $product->units }}" data-image="{{ $product->url }}">
                        <i class="fa fa-shopping-basket"></i>
                        <span class="hidden-xs hidden-sm hidden-md">Add to Basket</span>
                      </button>
                    </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
             
                <div class="card card-cart">
                  
                  <div class="card-body my-checkout-page-cart-table"></div>
                  <div class="card-block card-padding my-empty-checkout-page-cart-table">
                    <div class="well text-muted text-center m-t-10">
                      <i class="fa fa-shopping-basket fa-5x m-b-1"></i>
                      <h3>
                        <strong>Coming Soon. . . . </strong>
                      </h3>
                      <br>
                    </div>
                    </div>

                </div>

            </div>
        @endif
          
        <!-- <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
          <div class="product-layout">
            <div class="product-thumb transition">
              <div class="image">
                <a href="{{url('product/1002')}}">
                  <img class="img-fluid" src="https://image.delfoo.com/fresh/catalog/Vegetable/turmeric white.png" alt="White Turmeric Root (આંબા હળદર)" title="White Turmeric Root (આંબા હળદર)">
                </a>
              </div>
              <div class="caption">
                <div class="title">
                  <h4>
                    <a href="{{url('product/1002')}}">White Turmeric Root (આંબા હળદર)</a>
                  </h4>
                </div>
                <b class="text-center"></b>
                <div class="product-info">
                  <p class="price"><b>Rs. 20 / 250 Grams</b></p>
                </div>
              </div>
              <button type="button" class="btn alert alert-danger">Seasonal Not Available</button>
            </div>
          </div>
        </div> -->
      </div>

    </div>
  </div>
</div>
@endsection