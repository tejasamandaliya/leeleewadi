@extends('customer.layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row df-form-header" style="background-image: url({{asset('public/frontend-assets/images/image/account_background.jpg') }});background-size: cover;background-position: 50%;background-repeat: no-repeat;font-weight: 400;font-size: 13px;line-height: 21px;color: #000;">

        <div class="heading" style="margin-bottom: 10px;">
            <div class="container">
              <div class="row">
                <ul class="breadcrumb">
                  <li>
                    <a href="{{ url('/') }}">Home</a>
                  </li>
                  <li>
                    <a href="{{ url('/profile') }}">My Account</a>
                  </li>
                </ul>
              </div>
            </div>
        </div>

        <div class="container" style="padding: 15px 0px;margin-bottom: 0px;">
            <h3 class="bigger">
               {{ Auth::user()->first_name .' '. Auth::user()->last_name }} Profile 
                <a href="{{ url('/profile') }}" class="btn btn-white btn-sm b-radius-50 m-r-10"><i class="fa fa-pencil"></i></a>
            </h3>
            <div class="btn-group navigation" style="margin-bottom: 5px;">
                <a class="btn btn-default" href="{{url('/home')}}">Home</a>
                <a class="btn btn-default" href="{{url('/orders')}}">Orders</a>
                <a class="btn btn-default" href="{{url('/addresses')}}">Addresses</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="card df-form-boxtitle-colorful">
            <div class="row">
                @if(Session::has('success'))
                    <div class="col-md-11 alert alert-info">
                        {{ Session::get('success') }}
                    </div>
                @elseif(Session::has('warning'))
                    <div class="col-md-11 alert alert-warning">
                        {{ Session::get('warning') }}
                    </div>
                @endif
                @if (count($errors) > 0)
                    @foreach ($errors->all() as $error)
                        <div class="col-md-11 alert alert-warning">{{ $error }}</div>
                    @endforeach
                @endif
                <p id="products" class="page_type"></p>
                <p class="update_status"></p>
            </div>
          
            <div class="">
                <form class="signup-form df-form form-horizontal" role="form" method="POST" action="{{ url('/profile') }}">
                    {{ csrf_field() }}
                    <div class="df-form-step">
                        <span class="label label-success">1</span>
                        <span class="df-form-step-title">Customer Basic Information</span>
                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                    </div>
                    <div class="card-block">
                        <fieldset class="form-group">
                            <div class="col-sm-6">
                                <div>
                                    <input type="text" name="first_name" value="{{ Auth::user()->first_name }}" placeholder="*First Name" class="form-control" required="" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div>
                                    <input type="text" name="last_name" value="{{ Auth::user()->last_name }}" placeholder="*Last Name" class="form-control" required=""/>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="col-sm-12">
                                <div>
                                    <input type="text" name="email" value="{{ Auth::user()->email }}" placeholder="*Email" class="form-control" required=""/>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="col-sm-6">
                                <div>
                                    <input type="text" name="telephone" value="{{ Auth::user()->customerProfile->phone_number }}" placeholder="*Mobile or Phone" class="form-control" required=""/>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div>
                                    <input type="text" name="phone_alternate" value="{{ Auth::user()->customerProfile->alt_phone_number }}" placeholder="Alternative Contact Number" class="form-control" required=""/>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="df-form-step">
                        <span class="label label-success">2</span>
                        <span class="df-form-step-title">Customer Address Information </span>
                    </div>
                    <div class="card-block">
                        <fieldset class="form-group">
                            <div class="col-sm-12">
                                <div>
                                    <input type="text" name="address" value="{{ Auth::user()->customerProfile->address }}" placeholder="*Address" class="form-control" required=""/>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="col-sm-6">
                                <div>
                                    <input type="text" name="landmark" value="{{ Auth::user()->customerProfile->landmark }}" placeholder="*Landmark" class="form-control" required=""/>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="col-sm-6">
                                <div class="form-control"> Surat </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-control">Gujarat, INDIA</div>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <div class="col-sm-6">
                                <div>
                                    <select name="area_id" class="form-control">
                                        <option value="">Select Area</option>
                                        @foreach($areas as $area)
                                            <option value="{{$area->id}}" {{ ($area->id == Auth::user()->customerProfile->area_id)?'selected=""':'' }} >{{$area->name}}</option>
                                            
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div>
                                    <select name="pincode_id" class="form-control">
                                        <option value="">Select Pincode</option>
                                        @foreach($pincodes as $pincode)
                                            <option value="{{$pincode->id}}" {{ ($pincode->id == Auth::user()->customerProfile->pincode_id)?'selected=""':'' }}>{{$pincode->code}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                        <!-- <fieldset class="form-group">
                            <label class="col-sm-2 control-label">Address type: </label>
                            <div class="col-sm-10">
                                <div>
                                    <input type="radio" value="0"/>Home
                                                                                
                                    <input type="radio" value="1"/>Office
                                                                                    
                                    <input type="radio" value="2"/>Others
                                                                                    
                                </div>
                            </div>
                        </fieldset> -->
                        <!-- <fieldset class="form-group privacy">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-10">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="privacy" value="true"/>I have read and agree to the Privacy Policy                                         
                                    </label>
                                </div>
                            </div>
                        </fieldset> -->
                    </div>
                    <div class="card-footer">
                        <button action="signup" type="submit" class="btn btn-large btn-success df-form-btn">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
