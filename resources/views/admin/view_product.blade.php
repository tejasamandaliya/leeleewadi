@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ url('/admin/home') }}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{ url('/admin/products') }}">Products</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>View Product</span>
                    </li>
                </ul>
            </div>
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dropbox"></i>View Product </div>
                    <div class="actions btn-set">
                        <a class="btn btn-secondary-outline pull-left" href="{{ url('/admin/products') }}">
                            <i class="fa fa-angle-left"></i> Back</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="col-xs-12">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Name:</label>
                                <div class="col-md-9">
                                    <label class="form-control">{{$product['name']}}</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Description:</label>
                                <div class="col-md-9" >
                                    <div class="form-control bg-white" readonly style="margin-bottom: 5px;height: auto;">{!! nl2br($product['description']) !!}</div>
                                </div>
                            </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Nutrient:</label>
                                    <div class="col-md-9">
                                        <div class="form-control bg-white" readonly style="margin-bottom: 5px;height: auto;">{!!    nl2br($product['nutrient']) !!}</div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Shelf Life:</label>
                                    <div class="col-md-9">
                                        <div class="form-control bg-white" readonly style="margin-bottom: 5px;height: auto;">{!!    nl2br($product['shelf_life']) !!}</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Storage Tips:</label>
                                    <div class="col-md-9">
                                        <div class="form-control bg-white" readonly style="margin-bottom: 5px;height: auto;">{!!    nl2br($product['storage_tips']) !!}</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Storage Temperature (degC):
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="form-control bg-white" readonly style="margin-bottom: 5px;height: auto;">{!!    nl2br($product['storage_temperature']) !!}</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Disclaimer:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">

                                        <div class="form-control bg-white" readonly style="margin-bottom: 5px;height: auto;">{!!    nl2br($product['disclaimer']) !!}</div>
                                    </div>
                                </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Category:</label>
                                <div class="col-md-9">
                                    <label class="form-control">{{$product['category']}}</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Price:</label>
                                <div class="col-md-9">
                                    <label class="form-control">{{$product['price']}}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Primary Product Image:</label>
                                <div class="col-md-9">
                                    <div class="thumbnail-preview">
                                        @if(count($product['images']) > 0)
                                            @foreach($product['images'] as $image)
                                                @if(isset($product['primary_image_id']) && $image->id == $product['primary_image_id'])
                                                    <img src="{{ asset('/public/uploads/'.$image->file) }}" alt="Primary Image" />
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Sub Product Images:</label>
                                <div class="col-md-9">
                                    <div class="thumbnail-preview">
                                        @if(count($product['images']) > 0)
                                            @foreach($product['images'] as $index => $image)
                                                @if(!isset($product['primary_image_id']) || $image->id != $product['primary_image_id'])
                                                    <img src="{{ asset('/public/uploads/'.$image->file) }}" alt="Secondary Image {{ $index+1 }}" />
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection