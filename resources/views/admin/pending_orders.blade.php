@extends('admin.layouts.app')
@section('content')
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/admin/home') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Orders</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <h1 class="page-title"> Orders
        <small>Manage orders</small>
    </h1>
   
   <!--order blocks end -->
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
            @if(Session::has('success'))
                <div class="col-md-11 alert alert-info">
                    {{ Session::get('success') }}
                </div>
            @elseif(Session::has('warning'))
                <div class="col-md-11 alert alert-warning">
                    {{ Session::get('warning') }}
                </div>
            @endif
            </div>
            <typer id="orders" class="page_type"></typer>
            <div class="update_status"></div>
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

            <div class="portlet ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dropbox"></i>Pending Order Listing
                        
                    </div>
                    <div class="actions">
                        <a target="_blank" href="{{ url('/admin/pdfview/') }}" class="btn btn-circle btn-default" style="width: 40%;">
                            <i class="fa fa-list" aria-hidden="true"></i>
                            Get Menifest
                        </a>
                        <a target="_blank" href="{{ url('admin/pickUpList') }}" class="btn btn-circle btn-info">
                            <i class="fa fa-file-excel-o"></i>
                            <span class="hidden-xs"> Generate Pickup List </span>
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <div class="table-actions-wrapper">
                            <span> </span>
                            <select class="table-group-action-input form-control input-inline input-small input-sm">
                                <option value="">Select...</option>
                                <option value="publish">Publish</option>
                                <option value="unpublished">Un-publish</option>
                                <option value="delete">Delete</option>
                            </select>
                            <button class="btn btn-sm btn-success table-group-action-submit">
                                <i class="fa fa-check"></i> Submit
                            </button>
                        </div>
                        <table class="table table-striped table-bordered table-hover datatable">
                            <thead>
                                <tr role="row" class="heading">
                                    <th width="5%"> ID </th>
                                    <th width="10%"> Purchase Date </th>
                                    <th width="10%"> Customer Name</th>
                                    <th width="10%"> Total Amount </th>
                                    <th width="10%"> Status </th>
                                    <th width="20%"> Actions </th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($orderList) > 0)
                                    @foreach($orderList as $order)
                                        <tr class="{{ $order->deleted_at != null ? 'row-deleted' : ''}}">
                                            <td>{{ $order->id }}</td>
                                            <td>{{ $order->created_at }}</td>
                                            <td>{{ $order->user->first_name . ' ' . $order->user->last_name}}</td>
                                            <td><i class="fa fa-inr" style="font-size: 20px;"></i> {{ $order->list_price }}</td>
                                            <td>{{ $order->status }}</td>                                            
                                            <td>
                                                {{--<a href="{{ url('/admin/orders/addOrder/' . $order->id) }}" class="btn btn-circle btn-default" style="width: 30%;">
                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                    Edit
                                                </a>--}}
                                                @if($order->status == 'new')
                                                <a href="{{ url('/admin/orders/accept/' . $order->id) }}" class="btn btn-circle btn-default" style="width: 40%;">
                                                    <i class="fa fa-check" aria-hidden="true"></i>
                                                    Accept
                                                </a>
                                                @elseif($order->status == 'process')
                                                    <a target="_blank" class="btn btn-circle btn-primary" style="margin-top:5px"
                                                        href="{{ url('/admin/orders/print/' . $order->id) }}">
                                                        <i class="fa fa-print"></i> 
                                                        Print Invoice 
                                                    </a>
                                                @endif
                                                <a href="{{ url('/admin/orders/view/' . $order->id) }}" class="btn btn-circle btn-default" style="width: 25%;">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                    View
                                                </a>
                                                <a href="{{ url('/admin/orders/cancel/' . $order->id) }}" class="btn btn-circle btn-danger" style="width: 25%;">
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                    Cancel
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="orderFeedbackModal" tabindex="-1" role="basic" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Order Feedback</h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(function() {
            $('.datatable').dataTable();
            $('#orderFeedbackModal').on('show.bs.modal', function(e) {
              $(this).find('.modal-body').text($(e.relatedTarget).data('message'));
            });
        });
    </script>
@endsection
