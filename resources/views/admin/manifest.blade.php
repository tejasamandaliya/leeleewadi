<!DOCTYPE html>
<html lang="en">
    <head>
        <link href="/mt/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        
        <style type="text/css">
        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #5D6975;
            text-decoration: underline;
        }

        body {
            position: relative;
            width: 21cm;  
            /*height: 29.7cm; */
            margin: 0 auto; 
            color: #001028;
            background: #FFFFFF; 
            font-family: Arial, sans-serif; 
            font-size: 14px; 
            font-family: Arial;
        }

        #logo {
            text-align: center;
            margin-bottom: 10px;
        }

        #logo img {
            width: 90px;
        }

        h1 {
            border-top: 1px solid  #5D6975;
            border-bottom: 1px solid  #5D6975;
            color: #5D6975;
            font-size: 2.4em;
            line-height: 1.4em;
            font-weight: normal;
            text-align: center;
            margin: 0 0 20px 0;
            /*background: url(dimension.png);*/
            width: 93%;
        }

        h3 {
            color: #5D6975;
            font-size: 1.4em;
            line-height: 1.4em;
            font-weight: normal;
            text-align: center;
            margin: 0 0 20px 0;
            background: url(dimension.png);
        }

        .invoice-table {
            width: 93%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 20px;
            padding-top: 10px;
        }

        .invoice-table tr:nth-child(2n-1) td {
            background: #F5F5F5;
        }

        .invoice-table th,
        .invoice-table td {
            text-align: center;
        }

        .invoice-table th {
            padding: 5px 20px;
            color: #5D6975;
            border-bottom: 1px solid #C1CED9;
            white-space: nowrap;        
            font-weight: normal;
        }

        .invoice-table .service,
        .invoice-table .desc {
            text-align: left;
        }

        .invoice-table td {
            padding: 20px;
            text-align: right;
        }

        .invoice-table td.service,
        .invoice-table td.desc {
            vertical-align: top;
        }

        .invoice-table td.unit,
        .invoice-table td.qty,
        .invoice-table td.total {
            font-size: 1.2em;
        }

        .invoice-table td.grand {
            border-top: 1px solid #5D6975;;
        }

        .common-float-width {
            float: left;
            
        }
          
        .info-table tr td.value {
            padding-left: 10px;
        }
        .info-table tr td.name {
            width: 4cm;
        }

        </style>
        <meta charset="utf-8">
        <title>Order Summary</title>
    </head>
    <body>
        <header class="clearfix">
            <div id="logo" style="width: 93%">
                <img src="{{ url('/public/images/leelee-1.jpg') }}">
            </div>
            <h1>Manifest</h1>
        </header>
        <table class="invoice-table">
            <thead>
                <tr>
                    <th>Order Id</th>
                    <th>Customer Name</th>
                    <th>Customer Address</th>
                    <th>Customer Phone number</th>
                    <th>Amount</th>
                    <th>Signature/Feedback</th>
                </tr>
            </thead>
            <tbody>
                @if(isset($data))
                    @foreach($data['orders'] as $order)
                        <tr>
                            <td> {{ $order['id'] }}</td>
                            <td> {{\DB::table('users')->where('id', $order['user_id'] )->pluck('first_name')->first() .' '.
                            \DB::table('users')->where('id', $order['user_id'] )->pluck('last_name')->first() }} </td>
                            <td> {{ $order['customer']->address }} {{ \DB::table('areas')->where('id', $order['customer']->area_id)->pluck('name')->first() }} {{ \DB::table('pincodes')->where('id', $order['customer']->pincode_id)->pluck('code')->first() }} </td>
                            <td> {{ $order['customer']->phone_number }} </td>
                            <td> {{ $order['sale_price'] }} </td>
                            <td> </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </body>
</html>