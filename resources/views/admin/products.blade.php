@extends('admin.layouts.app')

@section('content')
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/admin/home') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Products</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <h1 class="page-title"> Products
        <small>Manage products</small>
    </h1>
    <div class="row">
        <div class="col-md-12">
            @if(Session::has('success'))
                <div class="col-md-11 alert alert-info">
                    {{ Session::get('success') }}
                </div>
            @elseif(Session::has('warning'))
                <div class="col-md-11 alert alert-warning">
                    {{ Session::get('warning') }}
                </div>
            @endif
            <typer id="products" class="page_type"></typer>
            <div class="update_status"></div>
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <div class="portlet ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dropbox"></i>Product Listing
                    </div>
                    <div class="actions">
                        <a href="{{ url('admin/addProduct') }}" class="btn btn-circle btn-info">
                            <i class="fa fa-plus"></i>
                            <span class="hidden-xs"> Add New Product </span>
                        </a>
                        <div class="btn-group">
                            <a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;"
                               data-toggle="dropdown">
                                <i class="fa fa-share"></i>
                                <span class="hidden-xs"> Tools </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="{{ url('admin/exportData/excel') }}"> Export to Excel </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/exportData/csv') }}"> Export to CSV </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/exportData/xml') }}"> Export to XML </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a target="_blank" href="{{ url('admin/exportData/print') }}"> Print Product List </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                {{--<div class="portlet-header">
                    <div class="form-inline pull-right">
                        <label for="filter_category">
                            &nbsp;Filter with Category:
                            <select name="filter_category" id="filter_category" class="form-control input-sm" onchange="window.location = '{{url('admin/products')}}?category_id=' + this.value">
                                <option value="all" {{ request('category_id', 'all') == 'all'? 'selected' : '' }}>All</option>
                                @if(count($categories) > 0)
                                    @foreach($categories as $category)
                                        <option {{ request('category_id', 0) == $category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </label>
                    </div>
                </div>--}}
                <div class="portlet-body">
                    <div class="table-container">
                        <div class="table-actions-wrapper">
                            <span> </span>
                            <select class="table-group-action-input form-control input-inline input-small input-sm">
                                <option value="">Select...</option>
                                <option value="publish">Publish</option>
                                <option value="unpublished">Un-publish</option>
                                <option value="delete">Delete</option>
                            </select>
                            <button class="btn btn-sm btn-success table-group-action-submit">
                                <i class="fa fa-check"></i> Submit
                            </button>
                        </div>
                        <table class="table table-striped table-bordered table-hover datatable">
                            <thead>
                            <tr role="row" class="heading">
                                <th width="5%"> ID </th>
                                <th width="20%"> Product Name </th>
                                <th width="10%"> Price </th>
                                <th width="10%"> Image Url </th>
                                <th width="15%"> Category </th>
                                <th width="40%"> Actions </th>
                            </tr>
                            </thead>
                            <tbody>
                                @if(count($products) > 0)
                                    @foreach($products as $product)
                                        <tr role="row" class="odd">
                                            <td class="sorting_1">{{ $product->id }}</td>
                                            <td>{{ $product->name }}</td>
                                            <td>{{ $product->price }}</td>
                                            <td><img src="{{ $product->url }}" width="70"></td>
                                            <td>{{ ($category = \App\Category::find($product->category_id)) ? $category->name : ''}}</td>
                                            <td>
                                                <a class="btn btn-sm btn-default btn-circle btn-editable" href="{{ url('admin/viewProduct/'. $product->id) }}">
                                                    <i class="fa fa-search"></i>
                                                    View
                                                </a>
                                                <a href="{{ url('admin/addProduct/' . $product->id) }}" class="btn btn-sm btn-default btn-circle btn-editable">
                                                    <i class="fa fa-pencil"></i>
                                                    Edit
                                                </a>
                                                <a href="javascript:;" class="btn btn-circle btn-danger" onclick="confirm('Are you sure you want to delete this product?') ? deleteProducts('{{ $product->id }}', this) : ''">
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                    <span class="hidden-xs"> Delete </span>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        function deleteProducts(id, element) {
            $(element).parent().parent().hide();
            $.post('{{ url('admin/productDelete') }}', {product_id: id, "_token": $('#token').val()})
                .done(function (msg) {
                    $('.update_status').html('<div class="alert alert-warning">Product Deleted Successfully !!</div>').fadeIn('slow').delay(3000).hide(0);
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    alert(xhr.responseText);
                    $(element).parent().parent().show();
                    //$('.error_rough').html(xhr.responseText);
                });
        }
        $(function() {
            $(".datatable").dataTable();
        });
    </script>
@endsection