<!DOCTYPE html>
<html lang="en">
    <head>
        <link href="/mt/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        
        <style type="text/css">
        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #5D6975;
            text-decoration: underline;
        }

        body {
            position: relative;
            width: 21cm;  
            /*height: 29.7cm; */
            margin: 0 auto; 
            color: #001028;
            background: #FFFFFF; 
            font-family: Arial, sans-serif; 
            font-size: 14px; 
            font-family: Arial;
        }

        #logo {
            text-align: center;
            margin-bottom: 10px;
        }

        #logo img {
            width: 90px;
        }

        h1 {
            border-top: 1px solid  #5D6975;
            border-bottom: 1px solid  #5D6975;
            color: #5D6975;
            font-size: 2.4em;
            line-height: 1.4em;
            font-weight: normal;
            text-align: center;
            margin: 0 0 20px 0;
            /*background: url(dimension.png);*/
            width: 93%;
        }

        h3 {
            color: #5D6975;
            font-size: 1.4em;
            line-height: 1.4em;
            font-weight: normal;
            text-align: center;
            margin: 0 0 20px 0;
            background: url(dimension.png);
        }

        .invoice-table {
            width: 93%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 20px;
            padding-top: 10px;
        }

        .invoice-table tr:nth-child(2n-1) td {
            background: #F5F5F5;
        }

        .invoice-table th,
        .invoice-table td {
            text-align: center;
        }

        .invoice-table th {
            padding: 5px 20px;
            color: #5D6975;
            border-bottom: 1px solid #C1CED9;
            white-space: nowrap;        
            font-weight: normal;
        }

        .invoice-table .service,
        .invoice-table .desc {
            text-align: left;
        }

        .invoice-table td {
            padding: 20px;
            text-align: right;
        }

        .invoice-table td.service,
        .invoice-table td.desc {
            vertical-align: top;
        }

        .invoice-table td.unit,
        .invoice-table td.qty,
        .invoice-table td.total {
            font-size: 1.2em;
        }

        .invoice-table td.grand {
            border-top: 1px solid #5D6975;;
        }

        .common-float-width {
            float: left;
            
        }
          
        .info-table tr td.value {
            padding-left: 10px;
        }
        .info-table tr td.name {
            width: 4cm;
        }

        </style>
        <meta charset="utf-8">
        <title>Order Pickup Summery</title>
    </head>
    <body>
        <header class="clearfix">
            <div id="logo" style="width: 93%">
                <img src="{{ url('/public/images/leelee-1.jpg') }}">
            </div>
            <h1>Pick Up List</h1>
            
            <h3 style="width: 93%">Orders </h3>

            <div class="row" style="width: 93%">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit portlet-datatable bordered">
                        <div class="portlet-body">
                            <div class="row">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <table class="invoice-table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Product Name</th>
                    <th>Quantity</th>
                    <th>Units</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @if(isset($orderQuantity))
                    @foreach($orderQuantity as $order)
                        <?php $index = 1; ?>
                        <tr>
                            <td> {{ $index++ }}</td>
                            <td> {{ \DB::table('products')->where('id', $order->product_id)->pluck('name')->first() }}
                            </td>
                            <td> {{ $order->Total_Buy }} </td>
                            <td>@if($order->Total_Buy >= 1 && $order->Total_Buy <= 99)
                                    Kg
                                @elseif($order->Total_Buy > 100 && $order->Total_Buy < 1000)
                                    Gram 
                                @endif 
                            </td>
                            <td> {{ $order->total_list_price }}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </body>
</html>