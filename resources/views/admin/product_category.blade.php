@extends('admin.layouts.app')
@section('content')
<div class="container-fluid">
<div class="row">
   <!-- BEGIN PAGE BAR -->
   <div class="page-bar">
      <ul class="page-breadcrumb">
         <li>
            <a href="{{ url('/admin/home') }}">Home</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
            <span>Product Category</span>
         </li>
      </ul>
   </div>
   <!-- END PAGE BAR -->
   <div class="row">
      @if(Session::has('success'))
      <div class="col-md-12 alert alert-info">
         {{ Session::get('success') }}
      </div>
      @elseif(Session::has('warning'))
      <div class="col-md-12 alert alert-warning">
         {{ Session::get('warning') }}
      </div>
      @endif

      <p id="product" class="page_type"></p>

      <p class="update_status"></p>

<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

      <div class="col-md-12">
         <div class="portlet ">
            <div class="portlet-title">
               <div class="caption">
                  <i class="fa fa-shopping-cart"></i>Product Categories
               </div>
               <div class="actions add_new_category">
                  <a href="javascript:;" class="btn btn-circle btn-info ">
                  <i class="fa fa-plus"></i>
                  <span class="hidden-xs"> Add New Category </span>
                  </a>
               </div>
               <div class="action">
                  <form lass="form-inline" id="category_form" style="display:none;float:right;margin:10px;" method="POST" action="{{ url('/admin/productCategory') }}">
                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                     <div style="display: flex;">
                        <input style="width:auto;" class="form-control" type="text" name="category_name" placeholder="Enter Category Name" autocomplete="off" required>
                     </div>
                     <div style="display: flex;">
                        <input style="width:auto;" class="form-control" type="text" name="url" placeholder="Category Image Url" autocomplete="off" required>
                        <input  class="btn btn-circle btn-info" type="submit" value="Submit">
                     </div>
                  </form>
               </div>
            </div>
            <div class="portlet-body">
               <div class="table-container">
                  <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_products">
                     <thead>
                        <tr role="row" class="heading">
                           <th width="10%"> ID </th>
                           <th width="15%"> Product&nbsp;Category&nbsp;Name </th>
                           <th width="15%"> Category&nbsp;Image&nbsp;URL </th>
                           <th width="10%"> Date&nbsp;Created </th>
                           <th width="10%"> Edit </th>
                           <th width="10%"> Delete </th>
                        </tr>
                     </thead>
                     <tbody>
                        @if(isset($parent_categories) && count($parent_categories) > 0)
                        
                        @foreach($parent_categories as $category)
                        <tr>
                           <td width="10%"> {{ $category{'id'} }}</td>
                           <td  width="20%"> <p class="category_name">{{ ucfirst($category['name']) }} </p>
                            <input type="hidden" name="category_name" value="{{ $category['cat_id'] }}" required>
                            <input class="edit_category_name" style="display:none;" type="text" name="category_name" value="{{ $category['name'] }}" required>
                           </td>
                           <td  width="20%"> <p class="category_url">{{ $category['url'] }} </p>
                            <input type="hidden" name="category_url" value="{{ $category['cat_id'] }}" required>
                            <input class="edit_category_url" style="display:none;width: 500px;" type="text" name="category_url" value="{{ $category['url'] }}" required>
                           </td>
                           <td width="25%"> {{ $category['created_at']}} </td>
                           <td class="edit_category" width="10%">
                              <a href="javascript:;" class="btn btn-circle btn-info edit">
                                 <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                  <span class="hidden-xs">  Edit </span>
                              </a>
                              <a href="javascript:;" id="{{ $category['id'] }}" title="{{ $category['id'] }}" style="display:none;" class="btn btn-circle btn-success submit_catgory_name submit_catgory_url">
                                 <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                  <span class="hidden-xs">  Submit </span>
                              </a>
                           </td>
                           <td width="10%">
                           <!-- <a href="javascript:;" id="{{ $category['parent_id'] }}" class="btn btn-circle btn-danger delete">
                              <i class="fa fa-trash" aria-hidden="true"></i>
                              <span class="hidden-xs"> Delete </span>
                            </a> -->
                            <a href="javascript:;" id="{{ $category['parent_id'] }}" class="btn btn-circle btn-danger" onclick="confirm('Are you sure you want to delete this Product Category?') ? deleteProductCategory({{ $category['id'] }}, this) : '';">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                                <span class="hidden-xs"> Delete </span>
                            </a>
                           </td>
                        </tr>

                        @endforeach
                        @else
                        <tr>
                           <td colspan="100%">No Categories Defined</td>
                        </tr>
                        @endif
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript">
        function deleteProductCategory(id, element) {
            $(element).parent().parent().hide();
            $.post('/admin/productCategoryDelete', {cat_id: id, "_token": $('#token').val()})
                .done(function (msg) {
                    $('.update_status').html('<div class="alert alert-warning">Product Category Deleted Successfully !!</div>').fadeIn('slow').delay(3000).hide(0);
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    alert(xhr.responseText);
                    $(element).parent().parent().show();
                    //$('.error_rough').html(xhr.responseText);
                });
        }
    </script>
@endsection