<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <i class="fa fa-inr" style="font-size: 20px;"></i> <span id="total_sales" data-counter="counterup" data-value=""></span>
                </div>
                <div class="desc"> Total of transactions </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 red" href="#">
            <div class="visual">
                <i class="fa fa-bar-chart-o"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span id="total_nos" data-counter="counterup" data-value=""></span></div>
                <div class="desc"> Total number of transactions </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 green" href="#">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <i class="fa fa-inr" style="font-size: 20px;"></i> <span id="sales_avg" data-counter="counterup" data-value=""></span>
                </div>
                <div class="desc"> Average items per transaction </div>
            </div>
        </a>
    </div>
</div>
@section('scripts')
<script type="text/javascript">
    $(function () {
        $.ajax({
            url: '{{ url('/admin/orders/getorderstatistics') }}/',
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    $("#total_sales").attr("data-value", response.data.sale).counterUp();
                    $("#total_nos").attr("data-value", response.data.trxCount).counterUp();
                    $("#sales_avg").attr("data-value", response.data.AvgpriceTrx).counterUp();
                    $("#sales_avg_trx").attr("data-value", response.data.AvgitemTrx).counterUp();
                } else {
                    console.log(
                        'Something went wrong. Can\'t mark notification as read at this point.'
                    )
                }
            },
            error: function (err) {
                console.log(err);
            }
        });

    });
</script>
@endsection