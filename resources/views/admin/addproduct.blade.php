@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ url('/admin/home') }}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{ url('/admin/products') }}">Products</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Add/Edit Product</span>
                    </li>
                </ul>
            </div>
            <!-- END PAGE BAR -->
            <div class="row">
                @if($product->product_rejection_note != null)
                    <p class="alert alert-warning"><strong>Last Profile Update Rejection Notice:</strong><br/>{{ $product->product_rejection_note }}</p>
                @endif
                @if(!Session::has('email_error') && !Session::has('warning') && !Session::has('success') && $product->product_updates != null && $product->product_rejection_note == null)
                    <p class="alert alert-info">Last updates are yet to approve from admin.</p>
                @endif
                @if(Session::has('success'))
                    <div class="col-md-11 alert alert-info">
                        {{ Session::get('success') }}
                    </div>
                @elseif(Session::has('warning'))
                    <div class="col-md-11 alert alert-warning">
                        {{ Session::get('warning') }}
                    </div>
                @endif
                @if (count($errors) > 0)
                    @foreach ($errors->all() as $error)
                        <div class="col-md-11 alert alert-warning">{{ $error }}</div>
                    @endforeach
                @endif
                <p id="products" class="page_type"></p>
                <p class="update_status"></p>
            </div>
            <form class="form-horizontal form-row-seperated" action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-social-dropbox"></i>New Product </div>
                        <div class="actions btn-set">
                            <a class="btn btn-secondary-outline" href="{{ url('/admin/products') }}">
                                <i class="fa fa-angle-left"></i> Back</a>
                            <button class="btn btn-success" type="submit">
                                <i class="fa fa-check"></i> Save</button>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="col-md-8">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Name:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="name" required placeholder="" value="{{ old('name') != null ? old('name') : $product->name}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Description:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" name="description" required>{{old('description') != null ? old('description') : $product->description}}</textarea>
                                        <span class="help-block"> shown in product listing </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Nutrient:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" name="nutrient" required>{{old('nutrient') != null ? old('nutrient') : $product->nutrient}}</textarea>
                                        <span class="help-block"> shown in product listing </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Shelf Life:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" name="shelf_life" required>{{old('shelf_life') != null ? old('shelf_life') : $product->shelf_life}}</textarea>
                                        <span class="help-block"> shown in product listing </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Storage Tips:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" name="storage_tips" required>{{old('storage_tips') != null ? old('storage_tips') : $product->storage_tips}}</textarea>
                                        <span class="help-block"> shown in product listing </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Storage Temperature (degC):
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" name="storage_temperature" required>{{old('storage_temperature') != null ? old('storage_temperature') : $product->storage_temperature}}</textarea>
                                        <span class="help-block"> shown in product listing </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Disclaimer:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" name="disclaimer" required>{{old('disclaimer') != null ? old('disclaimer') : $product->disclaimer}}</textarea>
                                        <span class="help-block"> shown in product listing </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Categories:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9 tree-container">
                                        <div id="product_category_tree" class="tree-demo"></div>
                                        <input type="hidden" name="categories" id="categories" value="{{ old('categories') != null ? old('categories') : $product->category_id}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Price:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" name="price" required value="{{old('price') != null ? old('price') : $product->price}}">
                                    </div>
                                    <label class="col-md-2 control-label">Quantity:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" name="quantity" required value="{{old('quantity') != null ? old('quantity') : $product->quantity}}">
                                    </div>
                                    <label class="col-md-1 control-label">Unit:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-2">
                                        <select name="units" class="form-control" value="{{old('units') != null ? old('units') : $product->units}}" required>
                                                <option value="Gram" selected="">In Grams</option>
                                                <option value="Kg">In KG</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Primary Product Image URL:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="url" accept="image/*" value="{{old('url') != null ? old('url') : $product->url}}">
                                    </div>
                                    {{-- <div class="col-md-9 col-md-offset-3">
                                        <label class="text-center small">Image Preview</label>
                                        <div class="thumbnail-preview">
                                            @if(count($images) > 0)
                                                @foreach($images as $image)
                                                    @if(isset($product->primary_image_id) && $image->id == $product->primary_image_id)
                                                        <img src="{{ asset('/public/uploads/'.$image->file) }}" alt="Primary Image" />
                                                    @endif
                                                @endforeach
                                            @endif
                                        </div>
                                    </div> --}}
                                </div>
                                {{--<div class="form-group">
                                    <label class="col-md-3 control-label">Sub Product Images:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        @if(count($images) > 0)
                                            @foreach($images as $index => $image)
                                                @if(!isset($product->primary_image_id) || $image->id != $product->primary_image_id)
                                                    <div class="input-group" image-id="{{ $image->id }}">
                                                        <input class="form-control" type="text" value="{{ $image->caption }}" readonly />
                                                        <span class="input-group-btn">
                                                            <button class="btn red" type="button" onclick="removeImage(this)"><i class="fa fa-remove"></i></button>
                                                        </span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        @endif
                                        <div class="input-group file">
                                            <input type="file" class="form-control" name="sub_images[]" accept="image/*" onchange="imageAdded(this, 'multi')">
                                            <span class="input-group-btn hidden">
                                                <button class="btn red" type="button" onclick="removeImage(this)"><i class="fa fa-remove"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-md-offset-3">
                                        <label class="text-center small">Image Preview</label>
                                        <div class="thumbnail-preview">
                                            @if(count($images) > 0)
                                                @foreach($images as $index => $image)
                                                    @if(!isset($product->primary_image_id) || $image->id != $product->primary_image_id)
                                                        <img src="{{ asset('/public/uploads/'.$image->file) }}" alt="Secondary Image {{ $index+1 }}" />
                                                    @endif
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('styles')
    <link rel="stylesheet" href="{{ url('public/mt/assets/global/plugins/jstree/dist/themes/default/style.min.css') }}">
    <link rel="stylesheet" href="{{ url('public/mt/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ url('public/mt/assets/global/plugins/jstree/dist/jstree.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('public/mt/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript">
        $(function() {
            var categoryTree = $('#product_category_tree').jstree({
                'plugins': ["wholerow", "checkbox", "types", "changed"],
                'core': {
                    "themes" : {
                        "responsive": false
                    },
                    multiple : false,
                    'data': [
                        @if(count($categories) > 0)
                            @foreach($categories as $index => $category)

                                @if($category->parent_id == null)
                                {

                                    "id" : "{{$category->id}}",
                                    "text": "{{$category->name}}",
                                    "icon" : "fa fa-tags",
                                    "state": {
                                        "opened":true,
                                        "checkbox_disabled" : {{floatval(count($category->productSubCategories) > 0)}},
                                        "disabled" : {{floatval(count($category->productSubCategories) > 0)}},
                                        "selected": {{ floatval((old('categories') != null && old('categories') == $category->id) || $product->category_id == $category->id) }}
                                    }
                                },
                                @endif
                            @endforeach
                        @endif
                    ]
                }
            });
            setTimeout(function() {
                categoryTree.on("changed.jstree", function (e, data) {
                    console.log(data.changed.selected);
                    $('#categories').val(data.changed.selected[0]);
                });
            }, 1000);
        });
        function imageAdded(element, type) {
            if(element.files.length > 0) {

                var colImageContainer = $(element).parents('.col-md-9');
                var isUpdated = $(element).attr('updated') == 'true';
                if(type == 'multi' && !isUpdated) {

                    colImageContainer.append($(
                        '<div class="input-group file">' +
                            '<input type="file" class="form-control" name="sub_images[]" accept="image/*" onchange="imageAdded(this, \'multi\')">' +
                            '<span class="input-group-btn">' +
                                '<button class="btn red" type="button" onclick="removeImage(this)"><i class="fa fa-remove"></i></button>' +
                            '</span>' +
                        '</div>'));
                    $(element).attr('updated', true);
                }
                if(colImageContainer.find('.input-group.file').length > 1) {

                    colImageContainer.find('.input-group.file').first().find('.input-group-btn').removeClass('hidden');
                }
                if (element.files && element.files[0]) {

                    var reader = new FileReader();

                    reader.onload = function (e) {
                        if(type == 'multi') {

                            if(isUpdated) {

                                colImageContainer.next().find('.thumbnail-preview img:eq(' + $(element).parent().index() + ')').attr('src', e.target.result);
                            } else {

                                colImageContainer.next().find('.thumbnail-preview').append('<img src="' + e.target.result + '" />');
                            }
                        } else {
                            if(colImageContainer.next().find('.thumbnail-preview img').length > 0) {
                                colImageContainer.next().find('.thumbnail-preview img').first().attr('src', e.target.result);
                            } else {
                                colImageContainer.next().find('.thumbnail-preview').append('<img src="' + e.target.result + '" />');
                            }
                        }
                    };

                    reader.readAsDataURL(element.files[0]);
                }
            }
        }
        function removeImage(element) {

            var colImageContainer = $(element).parents('.col-md-9');

            if($(element).parents('.input-group').find('input[type="file"]').length > 0 && $(element).parents('.input-group').find('input[type="file"]')[0].files.length > 0) {

                colImageContainer.next().find('.thumbnail-preview img:eq('+ $(element).parents('.input-group').index() +')').remove();
                $(element).parents('.input-group').remove();

            } else if($(element).parents('.input-group').find('input[type="text"]').length > 0) {

                var imageId = $(element).parents('.input-group').attr('image-id');

                $(element).find('.fa').removeClass('fa-remove').addClass('fa-spinner fa-spin');

                var imageIndex = $(element).parents('.input-group').index();

                jQuery.ajax({
                    url : '{{ url('admin/deleteProductImage') }}/' + imageId,
                    type : 'POST',
                    data : {
                        _token : $('#token').val()
                    },
                    success : function(data) {

                        if(data.success) {

                            colImageContainer.next().find('.thumbnail-preview img:eq('+ imageIndex +')').remove();
                            $(element).parents('.input-group').remove();
                        }
                    },
                    error : function(err) {

                        $(element).find('fa').addClass('fa-remove').removeClass('fa-spinner fa-spin');
                        console.error(err);
                    }
                });
            }

            if(colImageContainer.find('.input-group.file input[type="file"]').length == 1) {

                colImageContainer.find('.input-group.file .input-group-btn').addClass('hidden');
            }
        }
    </script>
@endsection