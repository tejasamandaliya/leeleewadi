  <li class="{!! Request::is('admin/home*') ? 'active' : '' !!}">
      <a href="{!! url('/admin/home') !!}" class="nav-link nav-toggle">
          <i class="icon-home"></i>
          <span class="title">Home</span>
      </a>
  </li>
  <li class="nav-item {!! Request::is('admin/productCategory') ? 'active' : '' !!}">
      <a href="{{url('/admin/productCategory')}}" class="nav-link nav-toggle">
          <i class="fa fa-tags" aria-hidden="true"></i>
          <span class="title">Categories</span>
      </a>
  </li>
  <li class="{!! Request::is('admin/products') || Request::is('admin/*Product*') ? 'active' : '' !!}">
      <a href="{!! url('/admin/products') !!}" class="nav-link nav-toggle">
          <i class="icon-social-dropbox"></i>
          <span class="title">Products</span>
      </a>
  </li>
  <li class="{!! Request::is('admin/orders*') ? 'active' : '' !!}">
    <a href="{!! url('/admin/orders/') !!}" class="nav-link nav-toggle">
       <i class="icon-basket"></i>
       <span class="title">Order History</span>
    </a>
  </li>
  <li class="{!! Request::is('admin/pending/orders') ? 'active' : '' !!}">
    <a href="{!! url('/admin/pending/orders/') !!}" class="nav-link nav-toggle">
       <i class="icon-basket"></i>
       <span class="title">Pending Order</span>
    </a>
  </li>