@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid" data-ng-app="RetailerOrder">
        <div class="row create_order" ng-controller="ViewOrderController">
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{ url('/admin/home') }}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{ url('/admin/orders') }}">Orders</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>View Order</span>
                    </li>
                </ul>
                <div class="pull-right">
                    <a class="btn btn-secondary-outline pull-right" target="_blank"
                    class="btn btn-xs blue" style="margin-top:5px"
                    href="{{ url('/admin/orders/print/' . $order_id) }}">
                        <i class="fa fa-print"></i> 
                        Print Invoice 
                    </a>
                </div>
            </div>
            <!-- END PAGE BAR -->
            <h1 class="page-title"> View Order
                <small></small>
            </h1>
            <div class="row">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light portlet-fit portlet-datatable bordered">
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="actions btn-set">
                                        <a class="btn btn-secondary-outline" href="{{ url('/admin/orders') }}">
                                            <i class="fa fa-angle-left"></i> Back
                                        </a>
                                    </div>
                                    <div class="col col-md-6 col-sm-12 hidden">
                                        
                                    </div>
                                    <div class="col col-md-6 col-sm-12 hidden" ng-class="{'hidden' : !customerDetail}">
                                        <div class="portlet blue-hoki box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-user"></i>
                                                    Customer Information
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Customer Name: </div>
                                                    <div class="col-md-7 value"><% customerDetail.first_name %> <% customerDetail.last_name %> </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Email: </div>
                                                    <div class="col-md-7 value"> <% customerDetail.email %> </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Phone </div>
                                                    <div class="col-md-7 value"> <% customerDetail.profile.phone_number %> </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Alternate Phone </div>
                                                    <div class="col-md-7 value"> <% customerDetail.profile.alt_phone_number %> </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Address </div>
                                                    <div class="col-md-7 value"> <% customerDetail.profile.address %> </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 hidden"  ng-class="{'hidden' : !Cart[0]}">
                                        <div class="portlet grey-cascade box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-shopping-cart"></i>
                                                    Shopping Cart
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-bordered table-striped">
                                                        <thead>
                                                        <tr>
                                                            <th width="20%"> Product </th>
                                                            <th width="30%"> Description</th>
                                                            <th> Price </th>
                                                            <th> Quantity </th>
                                                            <th> Units </th>
                                                            <th> Total </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr ng-repeat="prod in Cart track by $index">
                                                            <td> <% prod.name %></td>
                                                            <td> <% prod.description %></td>
                                                            <td> <% prod.price | number : 2 %> <i class="fa fa-inr" style="font-size: 14px;"></td>
                                                            <td> <% (prod.quantity)| number:2 %> </td>
                                                            <td ng-if="(prod.per_quantity * prod.quantity) >= 1 && (prod.per_quantity * prod.quantity) <= 99"> 
                                                                <% (prod.per_quantity * prod.quantity) | number:2 %>                        Kg
                                                            </td>
                                                            <td ng-if="(prod.per_quantity * prod.quantity) > 100 && (prod.per_quantity * prod.quantity) < 1000"> 
                                                                <% (prod.per_quantity * prod.quantity) %>                        Gram                                                             
                                                            </td>
                                                            <td> <% (prod.price * prod.quantity) | number:2 %> <i class="fa fa-inr" style="font-size: 14px;"> </td>
                                                        </tr>
                                                        <tr ng-show="!Cart[0]">
                                                            <td colspan="7" class="text-center">Cart is empty</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 hidden" ng-class="{'hidden' : !customerFeedbacks}">
                                        <div class="well">
                                            <div class="row static-info">
                                                <div class="col-md-12"> <strong>Customer Feedback :</strong> </div>
                                            </div>
                                            <div class="row static-info">
                                                <div class="col-md-3 name"> Ratings: </div>
                                                <div class="col-md-7 value">
                                                    <i class="fa fa-star" ng-repeat="ratings in customerFeedbacks.stars track by $index"></i>
                                                </div>
                                            </div>
                                            <div class="row static-info">
                                                <div class="col-md-3 name"> Message: </div>
                                                <div class="col-md-7 value"> <% customerFeedbacks.message %> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 hidden" ng-class="{'col-md-offset-6' : !customerFeedbacks, 'hidden' : !subTotal}">
                                        <div class="well">
                                            <div class="row static-info align-reverse">
                                                <div class="col-md-8 name"> Sub Total: </div>
                                                <div class="col-md-3 value">
                                                    <% subTotal | number : 2 %> <i class="fa fa-inr" style="font-size: 15px;"></i>
                                                </div>
                                            </div>
                                            <div class="row static-info align-reverse">
                                                <div class="col-md-8 name"> Shipping Charges: </div>
                                                <div class="col-md-3 value">
                                                    <% shippingCharge | number : 2 %> <i class="fa fa-inr" style="font-size: 15px;"></i>
                                                </div>
                                            </div>
                                            <div class="row static-info align-reverse">
                                                <div class="col-md-8 name"> Total: </div>
                                                <div class="col-md-3 value">
                                                    <% totalSale | number : 2 %> <i class="fa fa-inr" style="font-size: 15px;"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{asset('/public/mt/assets/global/plugins/angularjs/angular.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/public/mt/assets/global/plugins/angularjs/angular-sanitize.min.js')}}"></script>
<script type="text/javascript">
    (function() {

        var RetailerOrder = angular.module('RetailerOrder', ['ngSanitize'], function ($interpolateProvider) {

            $interpolateProvider.startSymbol('<%');
            $interpolateProvider.endSymbol('%>');

        });

        RetailerOrder.controller('ViewOrderController', function($scope, $http) {

            $http.get("{{ url('/admin/orders/detail/' . $order_id) }}").then(function(success) {
                success = success.data;
                
                if(success.success) {

                    $http.get('{{ url('admin/customerProfile') }}/' + success.data.user_id).then(function(success) {

                        success = success.data;
                        $scope.customerDetail = success.data;

                    }, function(error) {});

                    $scope.customerDetail = success.data.customer;
                    $scope.Cart = success.data.products;
                    $scope.subTotal = success.data.list_price;
                    $scope.totalSale = success.data.sale_price;
                    $scope.shippingCharge = success.data.shipping_charge;

                } else {

                    alert('Order not found in system. Now you will be redirected to order listing');
                    window.location = '{{ url('/admin/orders') }}';
                }

            }, function(error) {

                console.log(error);
                alert('Something went wrong. Can\'t load order details');
                window.location = '{{ url('/admin/orders') }}';
            });
        });
    })(angular);
</script>
@endsection
