<!DOCTYPE html>
<html lang="en">
    <head>
        <link href="/mt/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        
        <style type="text/css">
        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #5D6975;
            text-decoration: underline;
        }

        body {
            position: relative;
            width: 21cm;  
            /*height: 29.7cm; */
            margin: 0 auto; 
            color: #001028;
            background: #FFFFFF; 
            font-family: Arial, sans-serif; 
            font-size: 14px; 
            font-family: Arial;
        }

        #logo {
            text-align: center;
            margin-bottom: 10px;
        }

        #logo img {
            width: 150px;
        }

        h2 {
            border-top: 1px solid  #5D6975;
            border-bottom: 1px solid  #5D6975;
            color: #5D6975;
            font-size: 2.4em;
            line-height: 1.4em;
            font-weight: normal;
            text-align: center;
            margin: 0 0 20px 0;
            /*background: url(dimension.png);*/
            width: 93%;
        }

        h3 {
            color: #5D6975;
            font-size: 1.4em;
            line-height: 1.4em;
            font-weight: normal;
            text-align: center;
            margin: 0 0 20px 0;
            background: url(dimension.png);
        }

        .invoice-table {
            width: 93%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 20px;
            padding-top: 10px;
        }

        .invoice-table tr:nth-child(2n-1) td {
            background: #F5F5F5;
        }

        .invoice-table th,
        .invoice-table td {
            text-align: center;
        }

        .invoice-table th {
            padding: 5px 20px;
            color: #5D6975;
            border-bottom: 1px solid #C1CED9;
            white-space: nowrap;        
            font-weight: normal;
        }

        .invoice-table .service,
        .invoice-table .desc {
            text-align: left;
        }

        .invoice-table td {
            padding: 10px;
            text-align: right;
        }

        .invoice-table td.service,
        .invoice-table td.desc {
            vertical-align: top;
        }

        .invoice-table td.unit,
        .invoice-table td.qty,
        .invoice-table td.total {
            font-size: 1.2em;
        }

        .invoice-table td.grand {
            border-top: 1px solid #5D6975;;
        }

        .common-float-width {
            float: left;
            
        }
          
        .info-table tr td.value {
            padding-left: 10px;
        }
        .info-table tr td.name {
            width: 4cm;
        }

        </style>
        <meta charset="utf-8">
        <title>Order Summary</title>
    </head>
    <body>
        <header class="clearfix">
            <div id="logo" style="width: 93%">
                <img src="{{ url('/public/images/leelee-1.jpg') }}">
            </div>
            <h2>Order Summary</h2>
            
            <h3 style="width: 93%">Order ID {{ $order_id }}</h3>

            <div class="row" style="width: 93%">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit portlet-datatable bordered">
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col col-md-6 col-sm-12 common-float-width">
                                    <div class="portlet blue-hoki box">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <b> Customer Information </b>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <table class="info-table">
                                                <tr>
                                                    <td class="name"> <b> Customer No: </b> </td>
                                                    <td class="value">{{ $customer->user->id }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="name"> <b> Customer Phone: </b> </td>
                                                    <td class="value">{{ $customer->phone_number }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="name"> <b> Customer Name: </b></td>
                                                    <td class="value">{{ $customer->user->first_name .' '. $customer->user->last_name }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="name"> <b> Email: </b></td>
                                                    <td class="value">{{$customer->user->email }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="name"> <b> Address:</b> </td>
                                                    <td class="value">{{ $customer->address}} {{ \DB::table('areas')->where('id', $customer->area_id)->pluck('name')->first() }} {{ \DB::table('pincodes')->where('id', $customer->pincode_id)->pluck('code')->first() }}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <table class="invoice-table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Product</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Units</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                
                @if(isset($orders['products']))
                    @foreach($orders['products'] as $order)
                        <?php $index = 1; ?>
                        <tr>
                            <td> {{ $index++ }}</td>
                            <td> {{ $order{'name'} }} </td>
                            <td> {{ number_format($order['price'],2)  }} </td>
                            <td> {{ $order['quantity']  }} </td>
                            @if( ($order['per_quantity'] * $order['quantity']) >= 1 && ($order['per_quantity'] * $order['quantity']) <= 99)
                                <td> {{ ($order['per_quantity'] * $order['quantity']) }} Kg</td>
                            @elseif( ($order['per_quantity'] * $order['quantity']) > 100 && ($order['per_quantity'] * $order['quantity']) < 1000 )
                                <td> {{ $order['per_quantity']  * $order['quantity']  }} Gram </td>
                            @endif
                            
                            <td> {{ $order['price'] * $order['quantity'] }} </td>
                        </tr>
                    @endforeach
                @endif
                <tr>
                    <td colspan="5" class="grand total">Sub Total: </td>
                    <td class="grand total">{{ number_format($orders['list_price'], 2) }}</td>
                </tr>
                <tr>
                    <td colspan="5" class="grand total">Shipping charge: </td>
                    <td class="grand total">{{ number_format($orders['shipping_charge'], 2) }}</td>
                </tr>
                <tr>
                    <td colspan="5" class="grand total">Payable Total: </td>
                    <td class="grand total">{{ number_format($orders['sale_price'], 2) }}</td>
                </tr>
            </tbody>
        </table>
    </body>
</html>