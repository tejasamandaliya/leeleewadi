<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function(Blueprint $table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('user_id')->unsigned()->index('fk_orders_users_1');
            $table->integer('shipping_charge')->unsigned()->nullable();
            $table->decimal('list_price', 10)->nullable();
            $table->decimal('discount', 10)->nullable();
            $table->decimal('sale_price', 10)->nullable();
            $table->enum('status', array('new','cancelled','process','completed'));
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }

}
