<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_profiles', function(Blueprint $table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('user_id')->unsigned()->index('fk_customer_profiles_users_1');
            $table->string('address')->nullable();
            $table->string('landmark')->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->integer('pincode_id')->unsigned()->index('fk_pincode');;
            $table->integer('area_id')->unsigned()->index('fk_area');;
            $table->string('city')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('alt_phone_number')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer_profiles');
    }
}
