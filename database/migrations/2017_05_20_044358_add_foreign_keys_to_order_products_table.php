<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOrderProductsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_products', function(Blueprint $table)
        {
            $table->foreign('order_id', 'fk_order_products_orders_1')->references('id')->on('orders')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('product_id', 'fk_order_products_products_1')->references('id')->on('products')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_products', function(Blueprint $table)
        {
            $table->dropForeign('fk_order_products_orders_1');
            $table->dropForeign('fk_order_products_products_1');
        });
    }

}
