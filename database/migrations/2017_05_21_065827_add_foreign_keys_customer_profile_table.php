<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysCustomerProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_profiles', function(Blueprint $table)
        {
            $table->foreign('pincode_id', 'fk_pincode')->references('id')->on('pincodes')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('area_id', 'fk_area')->references('id')->on('areas')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_profiles', function(Blueprint $table)
        {
            $table->dropForeign('fk_pincode');
            $table->dropForeign('fk_area');
        });
    }
}
