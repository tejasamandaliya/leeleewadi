<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('url');
            
            $table->text('description', 65535)->nullable();
            $table->text('nutrient', 65535)->nullable();
            $table->text('shelf_life', 65535)->nullable();
            $table->text('storage_tips', 65535)->nullable();
            $table->text('storage_temperature', 65535)->nullable();
            $table->text('disclaimer', 65535)->nullable();
            $table->integer('category_id')->unsigned()->nullable()->index('fk_products_product_categories_1');
            $table->timestamps();
            $table->softDeletes();
            $table->decimal('price', 10, 2);
            $table->integer('quantity');
            $table->enum('units', array('Gram','Kg'));
            $table->enum('is_active', array('1','0'));
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }

}
