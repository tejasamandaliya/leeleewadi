<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderProductsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_products', function(Blueprint $table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('order_id')->unsigned()->index('fk_order_products_orders_1');
            $table->integer('product_id')->unsigned()->index('fk_order_products_products_1');
            $table->integer('quantity')->unsigned()->nullable();
            $table->decimal('list_price', 10)->unsigned()->nullable();
            $table->integer('shipping_charges')->unsigned()->nullable();
            $table->decimal('sale_price', 10)->unsigned()->nullable();
            $table->decimal('discount_percent', 10)->unsigned()->nullable();
            $table->enum('units', array('Gram','Kg','Pices'));
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_products');
    }

}
