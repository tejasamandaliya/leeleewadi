<?php

use Illuminate\Database\Seeder;

class ProductCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('product_categories')->delete();
        
        \DB::table('product_categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Vagetables',
                'created_at' => '2017-01-31 07:23:02',
                'updated_at' => '2017-01-31 07:36:10',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
            	'name' => 'Fruits',
                'created_at' => '2017-01-31 09:53:05',
                'updated_at' => '2017-01-31 09:58:08',
                'deleted_at' => NULL,
            ),
        ));
    }
}
