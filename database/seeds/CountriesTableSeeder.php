<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('countries')->delete();
        
        \DB::table('countries')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'AD',
                'name' => 'Andorra',
            ),
            1 => 
            array (
                'id' => 2,
                'code' => 'AE',
                'name' => 'United Arab Emirates',
            ),
            2 => 
            array (
                'id' => 3,
                'code' => 'AF',
                'name' => 'Afghanistan',
            ),
            3 => 
            array (
                'id' => 4,
                'code' => 'AG',
                'name' => 'Antigua and Barbuda',
            ),
            4 => 
            array (
                'id' => 5,
                'code' => 'AI',
                'name' => 'Anguilla',
            ),
            5 => 
            array (
                'id' => 6,
                'code' => 'AL',
                'name' => 'Albania',
            ),
            6 => 
            array (
                'id' => 7,
                'code' => 'AM',
                'name' => 'Armenia',
            ),
            7 => 
            array (
                'id' => 8,
                'code' => 'AO',
                'name' => 'Angola',
            ),
            8 => 
            array (
                'id' => 9,
                'code' => 'AQ',
                'name' => 'Antarctica',
            ),
            9 => 
            array (
                'id' => 10,
                'code' => 'AR',
                'name' => 'Argentina',
            ),
            10 => 
            array (
                'id' => 11,
                'code' => 'AS',
                'name' => 'American Samoa',
            ),
            11 => 
            array (
                'id' => 12,
                'code' => 'AT',
                'name' => 'Austria',
            ),
            12 => 
            array (
                'id' => 13,
                'code' => 'AU',
                'name' => 'Australia',
            ),
            13 => 
            array (
                'id' => 14,
                'code' => 'AW',
                'name' => 'Aruba',
            ),
            14 => 
            array (
                'id' => 15,
                'code' => 'AX',
                'name' => 'Åland Islands',
            ),
            15 => 
            array (
                'id' => 16,
                'code' => 'AZ',
                'name' => 'Azerbaijan',
            ),
            16 => 
            array (
                'id' => 17,
                'code' => 'BA',
                'name' => 'Bosnia and Herzegovina',
            ),
            17 => 
            array (
                'id' => 18,
                'code' => 'BB',
                'name' => 'Barbados',
            ),
            18 => 
            array (
                'id' => 19,
                'code' => 'BD',
                'name' => 'Bangladesh',
            ),
            19 => 
            array (
                'id' => 20,
                'code' => 'BE',
                'name' => 'Belgium',
            ),
            20 => 
            array (
                'id' => 21,
                'code' => 'BF',
                'name' => 'Burkina Faso',
            ),
            21 => 
            array (
                'id' => 22,
                'code' => 'BG',
                'name' => 'Bulgaria',
            ),
            22 => 
            array (
                'id' => 23,
                'code' => 'BH',
                'name' => 'Bahrain',
            ),
            23 => 
            array (
                'id' => 24,
                'code' => 'BI',
                'name' => 'Burundi',
            ),
            24 => 
            array (
                'id' => 25,
                'code' => 'BJ',
                'name' => 'Benin',
            ),
            25 => 
            array (
                'id' => 26,
                'code' => 'BL',
                'name' => 'Saint Barthélemy',
            ),
            26 => 
            array (
                'id' => 27,
                'code' => 'BM',
                'name' => 'Bermuda',
            ),
            27 => 
            array (
                'id' => 28,
                'code' => 'BN',
                'name' => 'Brunei Darussalam',
            ),
            28 => 
            array (
                'id' => 29,
                'code' => 'BO',
                'name' => 'Bolivia',
            ),
            29 => 
            array (
                'id' => 30,
                'code' => 'BQ',
                'name' => 'Caribbean Netherlands ',
            ),
            30 => 
            array (
                'id' => 31,
                'code' => 'BR',
                'name' => 'Brazil',
            ),
            31 => 
            array (
                'id' => 32,
                'code' => 'BS',
                'name' => 'Bahamas',
            ),
            32 => 
            array (
                'id' => 33,
                'code' => 'BT',
                'name' => 'Bhutan',
            ),
            33 => 
            array (
                'id' => 34,
                'code' => 'BV',
                'name' => 'Bouvet Island',
            ),
            34 => 
            array (
                'id' => 35,
                'code' => 'BW',
                'name' => 'Botswana',
            ),
            35 => 
            array (
                'id' => 36,
                'code' => 'BY',
                'name' => 'Belarus',
            ),
            36 => 
            array (
                'id' => 37,
                'code' => 'BZ',
                'name' => 'Belize',
            ),
            37 => 
            array (
                'id' => 38,
                'code' => 'CA',
                'name' => 'Canada',
            ),
            38 => 
            array (
                'id' => 39,
                'code' => 'CC',
            'name' => 'Cocos (Keeling) Islands',
            ),
            39 => 
            array (
                'id' => 40,
                'code' => 'CD',
                'name' => 'Congo, Democratic Republic of',
            ),
            40 => 
            array (
                'id' => 41,
                'code' => 'CF',
                'name' => 'Central African Republic',
            ),
            41 => 
            array (
                'id' => 42,
                'code' => 'CG',
                'name' => 'Congo',
            ),
            42 => 
            array (
                'id' => 43,
                'code' => 'CH',
                'name' => 'Switzerland',
            ),
            43 => 
            array (
                'id' => 44,
                'code' => 'CI',
                'name' => 'Côte D’Ivoire',
            ),
            44 => 
            array (
                'id' => 45,
                'code' => 'CK',
                'name' => 'Cook Islands',
            ),
            45 => 
            array (
                'id' => 46,
                'code' => 'CL',
                'name' => 'Chile',
            ),
            46 => 
            array (
                'id' => 47,
                'code' => 'CM',
                'name' => 'Cameroon',
            ),
            47 => 
            array (
                'id' => 48,
                'code' => 'CN',
                'name' => 'China',
            ),
            48 => 
            array (
                'id' => 49,
                'code' => 'CO',
                'name' => 'Colombia',
            ),
            49 => 
            array (
                'id' => 50,
                'code' => 'CR',
                'name' => 'Costa Rica',
            ),
            50 => 
            array (
                'id' => 51,
                'code' => 'CU',
                'name' => 'Cuba',
            ),
            51 => 
            array (
                'id' => 52,
                'code' => 'CV',
                'name' => 'Cape Verde',
            ),
            52 => 
            array (
                'id' => 53,
                'code' => 'CW',
                'name' => 'Curaçao',
            ),
            53 => 
            array (
                'id' => 54,
                'code' => 'CX',
                'name' => 'Christmas Island',
            ),
            54 => 
            array (
                'id' => 55,
                'code' => 'CY',
                'name' => 'Cyprus',
            ),
            55 => 
            array (
                'id' => 56,
                'code' => 'CZ',
                'name' => 'Czech Republic',
            ),
            56 => 
            array (
                'id' => 57,
                'code' => 'DE',
                'name' => 'Germany',
            ),
            57 => 
            array (
                'id' => 58,
                'code' => 'DJ',
                'name' => 'Djibouti',
            ),
            58 => 
            array (
                'id' => 59,
                'code' => 'DK',
                'name' => 'Denmark',
            ),
            59 => 
            array (
                'id' => 60,
                'code' => 'DM',
                'name' => 'Dominica',
            ),
            60 => 
            array (
                'id' => 61,
                'code' => 'DO',
                'name' => 'Dominican Republic',
            ),
            61 => 
            array (
                'id' => 62,
                'code' => 'DZ',
                'name' => 'Algeria',
            ),
            62 => 
            array (
                'id' => 63,
                'code' => 'EC',
                'name' => 'Ecuador',
            ),
            63 => 
            array (
                'id' => 64,
                'code' => 'EE',
                'name' => 'Estonia',
            ),
            64 => 
            array (
                'id' => 65,
                'code' => 'EG',
                'name' => 'Egypt',
            ),
            65 => 
            array (
                'id' => 66,
                'code' => 'EH',
                'name' => 'Western Sahara',
            ),
            66 => 
            array (
                'id' => 67,
                'code' => 'ER',
                'name' => 'Eritrea',
            ),
            67 => 
            array (
                'id' => 68,
                'code' => 'ES',
                'name' => 'Spain',
            ),
            68 => 
            array (
                'id' => 69,
                'code' => 'ET',
                'name' => 'Ethiopia',
            ),
            69 => 
            array (
                'id' => 70,
                'code' => 'FI',
                'name' => 'Finland',
            ),
            70 => 
            array (
                'id' => 71,
                'code' => 'FJ',
                'name' => 'Fiji',
            ),
            71 => 
            array (
                'id' => 72,
                'code' => 'FK',
                'name' => 'Falkland Islands',
            ),
            72 => 
            array (
                'id' => 73,
                'code' => 'FM',
                'name' => 'Micronesia, Federated States of',
            ),
            73 => 
            array (
                'id' => 74,
                'code' => 'FO',
                'name' => 'Faroe Islands',
            ),
            74 => 
            array (
                'id' => 75,
                'code' => 'FR',
                'name' => 'France',
            ),
            75 => 
            array (
                'id' => 76,
                'code' => 'GA',
                'name' => 'Gabon',
            ),
            76 => 
            array (
                'id' => 77,
                'code' => 'GB',
                'name' => 'United Kingdom',
            ),
            77 => 
            array (
                'id' => 78,
                'code' => 'GD',
                'name' => 'Grenada',
            ),
            78 => 
            array (
                'id' => 79,
                'code' => 'GE',
                'name' => 'Georgia',
            ),
            79 => 
            array (
                'id' => 80,
                'code' => 'GF',
                'name' => 'French Guiana',
            ),
            80 => 
            array (
                'id' => 81,
                'code' => 'GG',
                'name' => 'Guernsey',
            ),
            81 => 
            array (
                'id' => 82,
                'code' => 'GH',
                'name' => 'Ghana',
            ),
            82 => 
            array (
                'id' => 83,
                'code' => 'GI',
                'name' => 'Gibraltar',
            ),
            83 => 
            array (
                'id' => 84,
                'code' => 'GL',
                'name' => 'Greenland',
            ),
            84 => 
            array (
                'id' => 85,
                'code' => 'GM',
                'name' => 'Gambia',
            ),
            85 => 
            array (
                'id' => 86,
                'code' => 'GN',
                'name' => 'Guinea',
            ),
            86 => 
            array (
                'id' => 87,
                'code' => 'GP',
                'name' => 'Guadeloupe',
            ),
            87 => 
            array (
                'id' => 88,
                'code' => 'GQ',
                'name' => 'Equatorial Guinea',
            ),
            88 => 
            array (
                'id' => 89,
                'code' => 'GR',
                'name' => 'Greece',
            ),
            89 => 
            array (
                'id' => 90,
                'code' => 'GS',
                'name' => 'South Georgia and the South Sandwich Islands',
            ),
            90 => 
            array (
                'id' => 91,
                'code' => 'GT',
                'name' => 'Guatemala',
            ),
            91 => 
            array (
                'id' => 92,
                'code' => 'GU',
                'name' => 'Guam',
            ),
            92 => 
            array (
                'id' => 93,
                'code' => 'GW',
                'name' => 'Guinea-Bissau',
            ),
            93 => 
            array (
                'id' => 94,
                'code' => 'GY',
                'name' => 'Guyana',
            ),
            94 => 
            array (
                'id' => 95,
                'code' => 'HK',
                'name' => 'Hong Kong',
            ),
            95 => 
            array (
                'id' => 96,
                'code' => 'HM',
                'name' => 'Heard and McDonald Islands',
            ),
            96 => 
            array (
                'id' => 97,
                'code' => 'HN',
                'name' => 'Honduras',
            ),
            97 => 
            array (
                'id' => 98,
                'code' => 'HR',
                'name' => 'Croatia',
            ),
            98 => 
            array (
                'id' => 99,
                'code' => 'HT',
                'name' => 'Haiti',
            ),
            99 => 
            array (
                'id' => 100,
                'code' => 'HU',
                'name' => 'Hungary',
            ),
            100 => 
            array (
                'id' => 101,
                'code' => 'ID',
                'name' => 'Indonesia',
            ),
            101 => 
            array (
                'id' => 102,
                'code' => 'IE',
                'name' => 'Ireland',
            ),
            102 => 
            array (
                'id' => 103,
                'code' => 'IL',
                'name' => 'Israel',
            ),
            103 => 
            array (
                'id' => 104,
                'code' => 'IM',
                'name' => 'Isle of Man',
            ),
            104 => 
            array (
                'id' => 105,
                'code' => 'IN',
                'name' => 'India',
            ),
            105 => 
            array (
                'id' => 106,
                'code' => 'IO',
                'name' => 'British Indian Ocean Territory',
            ),
            106 => 
            array (
                'id' => 107,
                'code' => 'IQ',
                'name' => 'Iraq',
            ),
            107 => 
            array (
                'id' => 108,
                'code' => 'IR',
                'name' => 'Iran',
            ),
            108 => 
            array (
                'id' => 109,
                'code' => 'IS',
                'name' => 'Iceland',
            ),
            109 => 
            array (
                'id' => 110,
                'code' => 'IT',
                'name' => 'Italy',
            ),
            110 => 
            array (
                'id' => 111,
                'code' => 'JE',
                'name' => 'Jersey',
            ),
            111 => 
            array (
                'id' => 112,
                'code' => 'JM',
                'name' => 'Jamaica',
            ),
            112 => 
            array (
                'id' => 113,
                'code' => 'JO',
                'name' => 'Jordan',
            ),
            113 => 
            array (
                'id' => 114,
                'code' => 'JP',
                'name' => 'Japan',
            ),
            114 => 
            array (
                'id' => 115,
                'code' => 'KE',
                'name' => 'Kenya',
            ),
            115 => 
            array (
                'id' => 116,
                'code' => 'KG',
                'name' => 'Kyrgyzstan',
            ),
            116 => 
            array (
                'id' => 117,
                'code' => 'KH',
                'name' => 'Cambodia',
            ),
            117 => 
            array (
                'id' => 118,
                'code' => 'KI',
                'name' => 'Kiribati',
            ),
            118 => 
            array (
                'id' => 119,
                'code' => 'KM',
                'name' => 'Comoros',
            ),
            119 => 
            array (
                'id' => 120,
                'code' => 'KN',
                'name' => 'Saint Kitts and Nevis',
            ),
            120 => 
            array (
                'id' => 121,
                'code' => 'KP',
                'name' => 'North Korea',
            ),
            121 => 
            array (
                'id' => 122,
                'code' => 'KR',
                'name' => 'South Korea',
            ),
            122 => 
            array (
                'id' => 123,
                'code' => 'KW',
                'name' => 'Kuwait',
            ),
            123 => 
            array (
                'id' => 124,
                'code' => 'KY',
                'name' => 'Cayman Islands',
            ),
            124 => 
            array (
                'id' => 125,
                'code' => 'KZ',
                'name' => 'Kazakhstan',
            ),
            125 => 
            array (
                'id' => 126,
                'code' => 'LA',
                'name' => 'Lao People’s Democratic Republic',
            ),
            126 => 
            array (
                'id' => 127,
                'code' => 'LB',
                'name' => 'Lebanon',
            ),
            127 => 
            array (
                'id' => 128,
                'code' => 'LC',
                'name' => 'Saint Lucia',
            ),
            128 => 
            array (
                'id' => 129,
                'code' => 'LI',
                'name' => 'Liechtenstein',
            ),
            129 => 
            array (
                'id' => 130,
                'code' => 'LK',
                'name' => 'Sri Lanka',
            ),
            130 => 
            array (
                'id' => 131,
                'code' => 'LR',
                'name' => 'Liberia',
            ),
            131 => 
            array (
                'id' => 132,
                'code' => 'LS',
                'name' => 'Lesotho',
            ),
            132 => 
            array (
                'id' => 133,
                'code' => 'LT',
                'name' => 'Lithuania',
            ),
            133 => 
            array (
                'id' => 134,
                'code' => 'LU',
                'name' => 'Luxembourg',
            ),
            134 => 
            array (
                'id' => 135,
                'code' => 'LV',
                'name' => 'Latvia',
            ),
            135 => 
            array (
                'id' => 136,
                'code' => 'LY',
                'name' => 'Libya',
            ),
            136 => 
            array (
                'id' => 137,
                'code' => 'MA',
                'name' => 'Morocco',
            ),
            137 => 
            array (
                'id' => 138,
                'code' => 'MC',
                'name' => 'Monaco',
            ),
            138 => 
            array (
                'id' => 139,
                'code' => 'MD',
                'name' => 'Moldova',
            ),
            139 => 
            array (
                'id' => 140,
                'code' => 'ME',
                'name' => 'Montenegro',
            ),
            140 => 
            array (
                'id' => 141,
                'code' => 'MF',
            'name' => 'Saint-Martin (France)',
            ),
            141 => 
            array (
                'id' => 142,
                'code' => 'MG',
                'name' => 'Madagascar',
            ),
            142 => 
            array (
                'id' => 143,
                'code' => 'MH',
                'name' => 'Marshall Islands',
            ),
            143 => 
            array (
                'id' => 144,
                'code' => 'MK',
                'name' => 'Macedonia',
            ),
            144 => 
            array (
                'id' => 145,
                'code' => 'ML',
                'name' => 'Mali',
            ),
            145 => 
            array (
                'id' => 146,
                'code' => 'MM',
                'name' => 'Myanmar',
            ),
            146 => 
            array (
                'id' => 147,
                'code' => 'MN',
                'name' => 'Mongolia',
            ),
            147 => 
            array (
                'id' => 148,
                'code' => 'MO',
                'name' => 'Macau',
            ),
            148 => 
            array (
                'id' => 149,
                'code' => 'MP',
                'name' => 'Northern Mariana Islands',
            ),
            149 => 
            array (
                'id' => 150,
                'code' => 'MQ',
                'name' => 'Martinique',
            ),
            150 => 
            array (
                'id' => 151,
                'code' => 'MR',
                'name' => 'Mauritania',
            ),
            151 => 
            array (
                'id' => 152,
                'code' => 'MS',
                'name' => 'Montserrat',
            ),
            152 => 
            array (
                'id' => 153,
                'code' => 'MT',
                'name' => 'Malta',
            ),
            153 => 
            array (
                'id' => 154,
                'code' => 'MU',
                'name' => 'Mauritius',
            ),
            154 => 
            array (
                'id' => 155,
                'code' => 'MV',
                'name' => 'Maldives',
            ),
            155 => 
            array (
                'id' => 156,
                'code' => 'MW',
                'name' => 'Malawi',
            ),
            156 => 
            array (
                'id' => 157,
                'code' => 'MX',
                'name' => 'Mexico',
            ),
            157 => 
            array (
                'id' => 158,
                'code' => 'MY',
                'name' => 'Malaysia',
            ),
            158 => 
            array (
                'id' => 159,
                'code' => 'MZ',
                'name' => 'Mozambique',
            ),
            159 => 
            array (
                'id' => 160,
                'code' => 'NA',
                'name' => 'Namibia',
            ),
            160 => 
            array (
                'id' => 161,
                'code' => 'NC',
                'name' => 'New Caledonia',
            ),
            161 => 
            array (
                'id' => 162,
                'code' => 'NE',
                'name' => 'Niger',
            ),
            162 => 
            array (
                'id' => 163,
                'code' => 'NF',
                'name' => 'Norfolk Island',
            ),
            163 => 
            array (
                'id' => 164,
                'code' => 'NG',
                'name' => 'Nigeria',
            ),
            164 => 
            array (
                'id' => 165,
                'code' => 'NI',
                'name' => 'Nicaragua',
            ),
            165 => 
            array (
                'id' => 166,
                'code' => 'NL',
                'name' => 'The Netherlands',
            ),
            166 => 
            array (
                'id' => 167,
                'code' => 'NO',
                'name' => 'Norway',
            ),
            167 => 
            array (
                'id' => 168,
                'code' => 'NP',
                'name' => 'Nepal',
            ),
            168 => 
            array (
                'id' => 169,
                'code' => 'NR',
                'name' => 'Nauru',
            ),
            169 => 
            array (
                'id' => 170,
                'code' => 'NU',
                'name' => 'Niue',
            ),
            170 => 
            array (
                'id' => 171,
                'code' => 'NZ',
                'name' => 'New Zealand',
            ),
            171 => 
            array (
                'id' => 172,
                'code' => 'OM',
                'name' => 'Oman',
            ),
            172 => 
            array (
                'id' => 173,
                'code' => 'PA',
                'name' => 'Panama',
            ),
            173 => 
            array (
                'id' => 174,
                'code' => 'PE',
                'name' => 'Peru',
            ),
            174 => 
            array (
                'id' => 175,
                'code' => 'PF',
                'name' => 'French Polynesia',
            ),
            175 => 
            array (
                'id' => 176,
                'code' => 'PG',
                'name' => 'Papua New Guinea',
            ),
            176 => 
            array (
                'id' => 177,
                'code' => 'PH',
                'name' => 'Philippines',
            ),
            177 => 
            array (
                'id' => 178,
                'code' => 'PK',
                'name' => 'Pakistan',
            ),
            178 => 
            array (
                'id' => 179,
                'code' => 'PL',
                'name' => 'Poland',
            ),
            179 => 
            array (
                'id' => 180,
                'code' => 'PM',
                'name' => 'St. Pierre and Miquelon',
            ),
            180 => 
            array (
                'id' => 181,
                'code' => 'PN',
                'name' => 'Pitcairn',
            ),
            181 => 
            array (
                'id' => 182,
                'code' => 'PR',
                'name' => 'Puerto Rico',
            ),
            182 => 
            array (
                'id' => 183,
                'code' => 'PS',
                'name' => 'Palestinian Territory, Occupied',
            ),
            183 => 
            array (
                'id' => 184,
                'code' => 'PT',
                'name' => 'Portugal',
            ),
            184 => 
            array (
                'id' => 185,
                'code' => 'PW',
                'name' => 'Palau',
            ),
            185 => 
            array (
                'id' => 186,
                'code' => 'PY',
                'name' => 'Paraguay',
            ),
            186 => 
            array (
                'id' => 187,
                'code' => 'QA',
                'name' => 'Qatar',
            ),
            187 => 
            array (
                'id' => 188,
                'code' => 'RE',
                'name' => 'Reunion',
            ),
            188 => 
            array (
                'id' => 189,
                'code' => 'RO',
                'name' => 'Romania',
            ),
            189 => 
            array (
                'id' => 190,
                'code' => 'RS',
                'name' => 'Serbia',
            ),
            190 => 
            array (
                'id' => 191,
                'code' => 'RU',
                'name' => 'Russian Federation',
            ),
            191 => 
            array (
                'id' => 192,
                'code' => 'RW',
                'name' => 'Rwanda',
            ),
            192 => 
            array (
                'id' => 193,
                'code' => 'SA',
                'name' => 'Saudi Arabia',
            ),
            193 => 
            array (
                'id' => 194,
                'code' => 'SB',
                'name' => 'Solomon Islands',
            ),
            194 => 
            array (
                'id' => 195,
                'code' => 'SC',
                'name' => 'Seychelles',
            ),
            195 => 
            array (
                'id' => 196,
                'code' => 'SD',
                'name' => 'Sudan',
            ),
            196 => 
            array (
                'id' => 197,
                'code' => 'SE',
                'name' => 'Sweden',
            ),
            197 => 
            array (
                'id' => 198,
                'code' => 'SG',
                'name' => 'Singapore',
            ),
            198 => 
            array (
                'id' => 199,
                'code' => 'SH',
                'name' => 'Saint Helena',
            ),
            199 => 
            array (
                'id' => 200,
                'code' => 'SI',
                'name' => 'Slovenia',
            ),
            200 => 
            array (
                'id' => 201,
                'code' => 'SJ',
                'name' => 'Svalbard and Jan Mayen Islands',
            ),
            201 => 
            array (
                'id' => 202,
                'code' => 'SK',
            'name' => 'Slovakia (Slovak Republic)',
            ),
            202 => 
            array (
                'id' => 203,
                'code' => 'SL',
                'name' => 'Sierra Leone',
            ),
            203 => 
            array (
                'id' => 204,
                'code' => 'SM',
                'name' => 'San Marino',
            ),
            204 => 
            array (
                'id' => 205,
                'code' => 'SN',
                'name' => 'Senegal',
            ),
            205 => 
            array (
                'id' => 206,
                'code' => 'SO',
                'name' => 'Somalia',
            ),
            206 => 
            array (
                'id' => 207,
                'code' => 'SR',
                'name' => 'Suriname',
            ),
            207 => 
            array (
                'id' => 208,
                'code' => 'SS',
                'name' => 'South Sudan',
            ),
            208 => 
            array (
                'id' => 209,
                'code' => 'ST',
                'name' => 'Sao Tome and Principe',
            ),
            209 => 
            array (
                'id' => 210,
                'code' => 'SV',
                'name' => 'El Salvador',
            ),
            210 => 
            array (
                'id' => 211,
                'code' => 'SX',
            'name' => 'Saint-Martin (Pays-Bas)',
            ),
            211 => 
            array (
                'id' => 212,
                'code' => 'SY',
                'name' => 'Syria',
            ),
            212 => 
            array (
                'id' => 213,
                'code' => 'SZ',
                'name' => 'Swaziland',
            ),
            213 => 
            array (
                'id' => 214,
                'code' => 'TC',
                'name' => 'Turks and Caicos Islands',
            ),
            214 => 
            array (
                'id' => 215,
                'code' => 'TD',
                'name' => 'Chad',
            ),
            215 => 
            array (
                'id' => 216,
                'code' => 'TF',
                'name' => 'French Southern Territories',
            ),
            216 => 
            array (
                'id' => 217,
                'code' => 'TG',
                'name' => 'Togo',
            ),
            217 => 
            array (
                'id' => 218,
                'code' => 'TH',
                'name' => 'Thailand',
            ),
            218 => 
            array (
                'id' => 219,
                'code' => 'TJ',
                'name' => 'Tajikistan',
            ),
            219 => 
            array (
                'id' => 220,
                'code' => 'TK',
                'name' => 'Tokelau',
            ),
            220 => 
            array (
                'id' => 221,
                'code' => 'TL',
                'name' => 'Timor-Leste',
            ),
            221 => 
            array (
                'id' => 222,
                'code' => 'TM',
                'name' => 'Turkmenistan',
            ),
            222 => 
            array (
                'id' => 223,
                'code' => 'TN',
                'name' => 'Tunisia',
            ),
            223 => 
            array (
                'id' => 224,
                'code' => 'TO',
                'name' => 'Tonga',
            ),
            224 => 
            array (
                'id' => 225,
                'code' => 'TR',
                'name' => 'Turkey',
            ),
            225 => 
            array (
                'id' => 226,
                'code' => 'TT',
                'name' => 'Trinidad and Tobago',
            ),
            226 => 
            array (
                'id' => 227,
                'code' => 'TV',
                'name' => 'Tuvalu',
            ),
            227 => 
            array (
                'id' => 228,
                'code' => 'TW',
                'name' => 'Taiwan',
            ),
            228 => 
            array (
                'id' => 229,
                'code' => 'TZ',
                'name' => 'Tanzania',
            ),
            229 => 
            array (
                'id' => 230,
                'code' => 'UA',
                'name' => 'Ukraine',
            ),
            230 => 
            array (
                'id' => 231,
                'code' => 'UG',
                'name' => 'Uganda',
            ),
            231 => 
            array (
                'id' => 232,
                'code' => 'UM',
                'name' => 'United States Minor Outlying Islands',
            ),
            232 => 
            array (
                'id' => 233,
                'code' => 'US',
                'name' => 'United States',
            ),
            233 => 
            array (
                'id' => 234,
                'code' => 'UY',
                'name' => 'Uruguay',
            ),
            234 => 
            array (
                'id' => 235,
                'code' => 'UZ',
                'name' => 'Uzbekistan',
            ),
            235 => 
            array (
                'id' => 236,
                'code' => 'VA',
                'name' => 'Vatican',
            ),
            236 => 
            array (
                'id' => 237,
                'code' => 'VC',
                'name' => 'Saint Vincent and the Grenadines',
            ),
            237 => 
            array (
                'id' => 238,
                'code' => 'VE',
                'name' => 'Venezuela',
            ),
            238 => 
            array (
                'id' => 239,
                'code' => 'VG',
            'name' => 'Virgin Islands (British)',
            ),
            239 => 
            array (
                'id' => 240,
                'code' => 'VI',
            'name' => 'Virgin Islands (U.S.)',
            ),
            240 => 
            array (
                'id' => 241,
                'code' => 'VN',
                'name' => 'Vietnam',
            ),
            241 => 
            array (
                'id' => 242,
                'code' => 'VU',
                'name' => 'Vanuatu',
            ),
            242 => 
            array (
                'id' => 243,
                'code' => 'WF',
                'name' => 'Wallis and Futuna Islands',
            ),
            243 => 
            array (
                'id' => 244,
                'code' => 'WS',
                'name' => 'Samoa',
            ),
            244 => 
            array (
                'id' => 245,
                'code' => 'YE',
                'name' => 'Yemen',
            ),
            245 => 
            array (
                'id' => 246,
                'code' => 'YT',
                'name' => 'Mayotte',
            ),
            246 => 
            array (
                'id' => 247,
                'code' => 'ZA',
                'name' => 'South Africa',
            ),
            247 => 
            array (
                'id' => 248,
                'code' => 'ZM',
                'name' => 'Zambia',
            ),
            248 => 
            array (
                'id' => 249,
                'code' => 'ZW',
                'name' => 'Zimbabwe',
            ),
        ));
        
        
    }
}