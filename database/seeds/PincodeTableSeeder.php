<?php

use Illuminate\Database\Seeder;

class PincodeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    	\DB::table('pincodes')->delete();

		\DB::table('pincodes')->insert(array (
			0 => array	('id'=>1, 'code'=>395007),
			1 => array	('id'=>2, 'code'=>395002),
			2 => array	('id'=>3, 'code'=>395001),
			3 => array	('id'=>4, 'code'=>395003),
			4 => array	('id'=>5, 'code'=>395005),
			5 => array	('id'=>6, 'code'=>395006),
			6 => array	('id'=>7, 'code'=>395009),
			7 => array	('id'=>8, 'code'=>395017),
			8 => array	('id'=>9, 'code'=>395004),
			9 => array	('id'=>10,'code'=>394370),
			10 => array	('id'=>11,'code'=>395008),
			11 => array	('id'=>12,'code'=>394150),
			12 => array	('id'=>13,'code'=>394107),
			13 => array	('id'=>14,'code'=>394110),
			14 => array	('id'=>15,'code'=>394325),
			15 => array	('id'=>16,'code'=>394130),
			16 => array	('id'=>17,'code'=>394210),
			17 => array	('id'=>18,'code'=>394246),
			18 => array	('id'=>19,'code'=>394510),
			19 => array	('id'=>20,'code'=>395023),
			20 => array	('id'=>21,'code'=>394120),
			21 => array	('id'=>22,'code'=>395010),
			22 => array	('id'=>23,'code'=>394105),
			23 => array	('id'=>24,'code'=>394160),
			24 => array	('id'=>25,'code'=>395010),
			25 => array	('id'=>26,'code'=>394270),
			26 => array	('id'=>27,'code'=>394515),
			27 => array	('id'=>28,'code'=>395010),
			28 => array	('id'=>29,'code'=>394101),
			29 => array	('id'=>30,'code'=>394540),
			30 => array	('id'=>31,'code'=>394221),
			31 => array	('id'=>32,'code'=>394248),
			32 => array	('id' =>33,'code'=>394163),
			33 => array	('id' =>34,'code'=>394230),
			34 => array	('id' =>35,'code'=>394520),
			35 => array	('id' =>36,'code'=>394518),
        ));
    }
}
