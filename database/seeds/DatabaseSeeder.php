<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        // $this->call(ProductCategoriesTableSeeder::class);
        // $this->call(ProductsTableSeeder::class);
        // $this->call(ProductImagesTableSeeder::class);
        // $this->call(OrderTableSeeder::class);
        // $this->call(OrderProductsTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(CityTableSeeder::class);
        $this->call(AreaTableSeeder::class);
        $this->call(PincodeTableSeeder::class);
        // $this->call(FakeUserSeeder::class);
    }
}
