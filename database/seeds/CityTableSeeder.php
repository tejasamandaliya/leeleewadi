<?php

use Illuminate\Database\Seeder;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       //
        \DB::table('cities')->delete();

        \DB::table('cities')->insert(array (
        	0 => 
            array (
            	'id'=>1,
			    'name'=>'Alabama',
			    'country_id'=>233
			),
			1 => 
			array (
				'id'=>2,
		        'name'=>'Alaska',
		        'country_id'=>233
			),
			2 => 
			array (
				'id'=>3,
		        'name'=>'Arizona',
		        'country_id'=>233
			),
			3 => 
			array (
				'id'=>4,
		        'name'=>'Arkansas',
		        'country_id'=>233
			),
			4 => 
			array (
				'id'=>5,
		        'name'=>'California',
		        'country_id'=>233
			),
			5 => 
			array (
				'id'=>6,
		        'name'=>'Colorado',
		        'country_id'=>233
			),
			6 => 
			array (
				'id'=>7,
		        'name'=>'Connecticut',
		        'country_id'=>233
			),
			7 => 
			array (
				'id'=>8,
		        'name'=>'Delaware',
		        'country_id'=>233
			),
			8 => 
			array (
				'id'=>9,
		        'name'=>'Florida',
		        'country_id'=>233
			),
			9 => 
			array (
				'id'=>10,
		        'name'=>'Georgia',
		        'country_id'=>233
			),
			10 => 
			array 	(
				'id'=>11,
		        'name'=>'Hawaii',
		        'country_id'=>233
			),
			11 => 
			array (
			    'id'=>12,
			    'name'=>'Idaho',
			    'country_id'=>233
			),
			12 => 
			array (
			    'id'=>13,
			    'name'=>'Illinois',
			    'country_id'=>233
			),
			13 => 
			array (
			    'id'=>14,
			    'name'=>'Indiana',
			    'country_id'=>233
			),
			14 => 
			array (
			    'id'=>15,
			    'name'=>'Iowa',
			    'country_id'=>233
			),
			15 => 
			array (
			    'id'=>16,
			    'name'=>'Kansas',
			    'country_id'=>233
			),
			16 => 
			array (
			    'id'=>17,
			    'name'=>'Kentucky',
			    'country_id'=>233
			),
			17 => 
			array (
			    'id'=>18,
			    'name'=>'Louisiana',
			    'country_id'=>233
			),
			18 => 
			array (
			    'id'=>19,
			    'name'=>'Maine',
			    'country_id'=>233
			),
			19 => 
			array (
			    'id'=>20,
			    'name'=>'Maryland',
			    'country_id'=>233
			),
			20 => 
			array (
			    'id'=>21,
			    'name'=>'Massachusetts',
			    'country_id'=>233
			),
			21 => array (
	            'id'=>22,
	            'name'=>'Michigan',
	            'country_id'=>233
			),
			22 => 
			array (
	            'id'=>23,
	            'name'=>'Minnesota',
	            'country_id'=>233
			),
			23 => 
			array (
	            'id'=>24,
	            'name'=>'Mississippi',
	            'country_id'=>233
			),
			24 => 
			array (
	            'id'=>25,
	            'name'=>'Missouri',
	            'country_id'=>233
			),
			25 => 
			array (
	            'id'=>26,
	            'name'=>'Montana',
	            'country_id'=>233
			),
			26 => 
			array (
	            'id'=>27,
	            'name'=>'Nebraska',
	            'country_id'=>233
			),
			27 => 
			array (
	            'id'=>28,
	            'name'=>'Nevada',
	            'country_id'=>233
			),
			28 => 
			array (
	            'id'=>29,
	            'name'=>'New Hampshire',
	            'country_id'=>233
			),
			29 => 
			array (
	            'id'=>30,
	            'name'=>'New Jersey',
	            'country_id'=>233
			),
			30 => 
			array (
	            'id'=>31,
	            'name'=>'New Mexico',
	            'country_id'=>233
			),
			31 => 
			array (
	            'id'=>32,
	            'name'=>'New York',
	            'country_id'=>233
			),
			32 => 
			array (
	            'id'=>33,
	            'name'=>'North Carolina',
	            'country_id'=>233
			),
			33 => 
			array (
	            'id'=>34,
	            'name'=>'North Dakota',
	            'country_id'=>233
			),
			34 => 
			array (
	            'id'=>35,
	            'name'=>'Ohio',
	            'country_id'=>233
			),
			35 => 
			array (
	            'id'=>36,
	            'name'=>'Oklahoma',
	            'country_id'=>233
			),
			36 => 
			array (
	            'id'=>37,
	            'name'=>'Oregon',
	            'country_id'=>233
			),
			37 => 
			array (
	            'id'=>38,
	            'name'=>'Pennsylvania',
	            'country_id'=>233
			),
			38 => 
			array (
	            'id'=>39,
	            'name'=>'Rhode Island',
	            'country_id'=>233
			),
			39 => 
			array (
	            'id'=>40,
	            'name'=>'South Carolina',
	            'country_id'=>233
			),
			40 => 
			array (
	            'id'=>41,
	            'name'=>'South Dakota',
	            'country_id'=>233
			),
			41 => 
			array (
	            'id'=>42,
	            'name'=>'Tennessee',
	            'country_id'=>233
			),
			42 => 
			array (
	            'id'=>43,
	            'name'=>'Texas',
	            'country_id'=>233
			),
			43 => 
			array (
	            'id'=>44,
	            'name'=>'Utah',
	            'country_id'=>233
			),
			44 => 
			array (
	            'id'=>45,
	            'name'=>'Vermont',
	            'country_id'=>233
			),
			45 => 
			array (
	            'id'=>46,
	            'name'=>'Virginia',
	            'country_id'=>233
			),
			46 => 
			array (
	            'id'=>47,
	            'name'=>'Washington',
	            'country_id'=>233
			),
			47 => 
			array (
	            'id'=>48,
	            'name'=>'West Virginia',
	            'country_id'=>233
			),
			48 => 
			array (
	            'id'=>49,
	            'name'=>'Wisconsin',
	            'country_id'=>233
			),
			49 => 
			array (
	            'id'=>50,
	            'name'=>'Wyoming',
	            'country_id'=>233
			),
			50 => 
			array ( 	
				'id'=>51,
				'name'=>'Dubai',
				'country_id'=>2
			),
			51 => 
			array ( 	
				'id'=>52,
				'name'=>'Abu Dhabi',
				'country_id'=>2
			),
			52 => 
			array ( 	
				'id'=>53,
				'name'=>'Sharjah',
				'country_id'=>2
			),
			53 => 
			array ( 	
				'id'=>54,
				'name'=>'Al Ain',
				'country_id'=>2
			),
			54 => 
			array ( 	
				'id'=>55,
				'name'=>'Ajman',
				'country_id'=>2
			),
			55 => 
			array ( 	
				'id'=>56,
				'name'=>'Ras Al Khaimah',
				'country_id'=>2
			),
			56 => 
			array ( 	
				'id'=>57,
				'name'=>'Fujairah',
				'country_id'=>2
			),
			57 => 
			array ( 	
				'id'=>58,
				'name'=>'Umm al-Quwain',
				'country_id'=>2	
			),
			58 =>
			array (
				'id'=> 59,
				'name'=> 'Abhā',
				'country_id'=> 193
			),
			59 =>
			array (
				'id'=> 60,
				'name'=> 'Abqaiq',
				'country_id'=> 193
			),
			60 =>
			array (
				'id'=> 61,
				'name'=> 'Al-Baḥah',
				'country_id'=> 193
			),
			61 =>
			array (
				'id'=> 62,
				'name'=> 'Al-Dammām',
				'country_id'=> 193
			),
			62 =>
			array (
				'id'=> 63,
				'name'=> 'Al-Hufūf',
				'country_id'=> 193
			),
			63 =>
			array (
				'id'=> 64,
				'name'=> 'Al-Jawf',
				'country_id'=> 193
			),
			64 =>
			array (
				'id'=> 65,
				'name'=> 'Al-Kharj (oasis)',
				'country_id'=> 193
			),
			65 =>
			array (
				'id'=> 66,
				'name'=> 'Al-Khubar',
				'country_id'=> 193
			),
			66 =>
			array (
				'id'=> 67,
				'name'=> 'Al-Qaṭīf',
				'country_id'=> 193
			),
			67 =>
			array (
				'id'=> 68,
				'name'=> 'Al-Ṭaʾif',
				'country_id'=> 193
			),
			68 =>
			array (
				'id'=> 69,
				'name'=> 'ʿArʿar',
				'country_id'=> 193
				),
			69 =>
			array (
				'id'=> 70,
				'name'=> 'Buraydah',
				'country_id'=> 193
			),
			70 =>
			array (
				'id'=> 71,
				'name'=> 'Dhahran',
				'country_id'=> 193
			),
			71 =>
			array (
				'id'=> 72,
				'name'=> 'Ḥāʾil',
				'country_id'=> 193
			),
			72 =>
			array (
				'id'=> 73,
				'name'=> 'Jiddah',
				'country_id'=> 193
			),
			73 =>
			array (
				'id'=> 74,
				'name'=> 'Jīzān',
				'country_id'=> 193
			),
			74 =>
			array (
				'id'=> 75,
				'name'=> 'Khamīs Mushayt',
				'country_id'=> 193
			),
			75 =>
			array (
				'id'=> 76,
				'name'=> 'King Khalīd Military City',
				'country_id'=> 193
			),
			76 =>
			array (
				'id'=> 77,
				'name'=> 'Mecca',
				'country_id'=> 193
			),
			77 =>
			array (
				'id'=> 78,
				'name'=> 'Medina',
				'country_id'=> 193
			),
			78 =>
			array (
				'id'=> 79,
				'name'=> 'Najrān',
				'country_id'=> 193
			),
			79 =>
			array (
				'id'=> 80,
				'name'=> 'Ras Tanura',
				'country_id'=> 193
			),
			80 =>
			array (
				'id'=> 81,
				'name'=> 'Riyadh',
				'country_id'=> 193
			),
			81 =>
			array (
				'id'=> 82,
				'name'=> 'Sakākā',
				'country_id'=> 193
			),
			82 =>
			array (
				'id'=> 83,
				'name'=> 'Tabūk',
				'country_id'=> 193
			),
			83 =>
			array (
				'id'=> 84,
				'name'=> 'Yanbuʿ',
				'country_id'=> 193
			)
        ));
    }
}
