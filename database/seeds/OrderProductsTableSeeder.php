<?php

use Illuminate\Database\Seeder;

class OrderProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('order_products')->delete();
        
        \DB::table('order_products')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2017-02-09 16:11:18',
                'updated_at' => '2017-02-11 17:05:54',
                'deleted_at' => NULL,
                'order_id' => 1,
                'product_id' => 1,
                'quantity' => 1,
                'list_price' => '5.00',
                'discount' => '0.25',
                'sale_price' => '4.75',
                'discount_percent' => '5.00',
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => '2017-02-09 16:11:18',
                'updated_at' => '2017-02-11 17:05:54',
                'deleted_at' => NULL,
                'order_id' => 1,
                'product_id' => 2,
                'quantity' => 1,
                'list_price' => '7.75',
                'discount' => '0.39',
                'sale_price' => '7.36',
                'discount_percent' => '5.00',
            ),
            2 => 
            array (
                'id' => 3,
                'created_at' => '2017-02-09 16:11:18',
                'updated_at' => '2017-02-11 17:05:54',
                'deleted_at' => NULL,
                'order_id' => 2,
                'product_id' => 3,
                'quantity' => 1,
                'list_price' => '15.50',
                'discount' => '1.55',
                'sale_price' => '13.95',
                'discount_percent' => '10.00',
            ),
            3 => 
            array (
                'id' => 4,
                'created_at' => '2017-02-10 13:17:12',
                'updated_at' => '2017-02-12 13:13:44',
                'deleted_at' => NULL,
                'order_id' => 2,
                'product_id' => 4,
                'quantity' => 1,
                'list_price' => '500.00',
                'discount' => '50.00',
                'sale_price' => '450.00',
                'discount_percent' => '10.00',
            ),
            4 => 
            array (
                'id' => 5,
                'created_at' => '2017-02-10 13:17:12',
                'updated_at' => '2017-02-12 13:13:44',
                'deleted_at' => NULL,
                'order_id' => 2,
                'product_id' => 5,
                'quantity' => 1,
                'list_price' => '80000.00',
                'discount' => '8000.00',
                'sale_price' => '72000.00',
                'discount_percent' => '10.00',
            ),
            5 => 
            array (
                'id' => 6,
                'created_at' => '2017-04-25 05:53:25',
                'updated_at' => '2017-04-25 05:53:25',
                'deleted_at' => NULL,
                'order_id' => 3,
                'product_id' => 5,
                'quantity' => 1,
                'list_price' => '80000.00',
                'discount' => '0.00',
                'sale_price' => '80000.00',
                'discount_percent' => '0.00',
            ),
            6 => 
            array (
                'id' => 7,
                'created_at' => '2017-04-25 05:53:25',
                'updated_at' => '2017-04-25 05:53:25',
                'deleted_at' => NULL,
                'order_id' => 3,
                'product_id' => 3,
                'quantity' => 1,
                'list_price' => '15.50',
                'discount' => '0.00',
                'sale_price' => '15.50',
                'discount_percent' => '0.00',
            ),
            7 => 
            array (
                'id' => 8,
                'created_at' => '2017-04-25 05:53:46',
                'updated_at' => '2017-04-25 05:53:46',
                'deleted_at' => NULL,
                'order_id' => 4,
                'product_id' => 1,
                'quantity' => 1,
                'list_price' => '5.00',
                'discount' => '0.25',
                'sale_price' => '4.75',
                'discount_percent' => '5.00',
            ),
            8 => 
            array (
                'id' => 9,
                'created_at' => '2017-04-25 05:53:46',
                'updated_at' => '2017-04-25 05:53:46',
                'deleted_at' => NULL,
                'order_id' => 4,
                'product_id' => 2,
                'quantity' => 1,
                'list_price' => '7.75',
                'discount' => '0.39',
                'sale_price' => '7.36',
                'discount_percent' => '5.00',
            ),
            9 => 
            array (
                'id' => 10,
                'created_at' => '2017-04-25 05:53:57',
                'updated_at' => '2017-04-25 05:53:57',
                'deleted_at' => NULL,
                'order_id' => 5,
                'product_id' => 1,
                'quantity' => 1,
                'list_price' => '5.00',
                'discount' => '0.25',
                'sale_price' => '4.75',
                'discount_percent' => '5.00',
            ),
            10 => 
            array (
                'id' => 11,
                'created_at' => '2017-04-25 05:54:17',
                'updated_at' => '2017-04-25 05:54:17',
                'deleted_at' => NULL,
                'order_id' => 6,
                'product_id' => 5,
                'quantity' => 1,
                'list_price' => '80000.00',
                'discount' => '0.00',
                'sale_price' => '80000.00',
                'discount_percent' => '0.00',
            ),
        ));
    }
}
