<?php

use Illuminate\Database\Seeder;

class ProductImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        


        \DB::table('product_images')->delete();

        \DB::table('product_images')->insert(array (
            0 =>
            array (
                'id' => 1,
                'product_id' => 1,
                'caption' => 'E',
                'file' => 'product_images/01.jpg',
                'created_at' => '2017-02-05 06:39:39',
                'updated_at' => '2017-02-13 07:56:56',
                'deleted_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                'product_id' => 2,
                'caption' => 'D',
                'file' => 'product_images/02.jpg',
                'created_at' => '2017-02-05 06:39:40',
                'updated_at' => '2017-02-05 07:50:39',
                'deleted_at' => NULL,
            ),
            2 =>
            array (
                'id' => 3,
                'product_id' => 1,
                'caption' => 'C',
                'file' => 'product_images/03.jpg',
                'created_at' => '2017-02-05 06:51:26',
                'updated_at' => '2017-02-05 07:52:28',
                'deleted_at' => NULL,
            ),
            3 =>
            array (
                'id' => 4,
                'product_id' => 1,
                'caption' => 'B',
                'file' => 'product_images/04.jpg',
                'created_at' => '2017-02-05 07:52:23',
                'updated_at' => '2017-02-05 07:52:43',
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 5,
                'product_id' => 2,
                'caption' => 'A',
                'file' => 'product_images/05.jpg',
                'created_at' => '2017-02-05 07:52:53',
                'updated_at' => '2017-02-05 07:53:13',
                'deleted_at' => NULL,
            ),
        ));

        \DB::table('products')->where('id', 1)->update(['primary_image_id' => 1]);
        \DB::table('products')->where('id', 2)->update(['primary_image_id' => 2]);
    }
}
