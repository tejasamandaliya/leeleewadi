<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('products')->delete();

        \DB::table('products')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'Berry Coffee Flavored Blends',
                'description' => 'Flavors Include: Caribbean hazelnut, Buttered Rum,
                  Swiss Chocolate Almond, Irish Cream, Tropical Vanilla.',
                'category_id' => 1,
                'created_at' => '2017-02-05 06:39:39',
                'updated_at' => '2017-02-05 09:22:54',
                'deleted_at' => NULL,
                'primary_image_id' => NULL,
                'url' => NULL,
                'price' => '5.00',
                'quantity' => '500',
                'units' =>'Gram',
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'Caribou Ground',
                'description' => 'Flavors Include: Caribou Blend, Daybreak, French Roast, decaf.
                  Size: 18 ct., Weight 2.5 oz.',
                'category_id' => 1,
                'created_at' => '2017-02-05 09:37:56',
                'updated_at' => '2017-02-05 09:37:56',
                'deleted_at' => NULL,
                'url' => NULL,
                'primary_image_id' => NULL,
                'price' => '7.75',
                'quantity' => '500',
                'units' =>'Kg',
            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'Dunn Brothers Ground',
                'description' => 'Flavors Include: Full City, French, French Decaf.Size: 18 ct.,
                  Weight: 2.5 oz.',
                'category_id' => 1,
                'created_at' => '2017-02-06 06:22:13',
                'updated_at' => '2017-02-06 06:22:13',
                'deleted_at' => NULL,
                'primary_image_id' => NULL,
                'price' => '15.50',
                'quantity' => '500',
                'units' =>'Kg',
            ),
            3 =>
            array (
                'id' => 4,
                'name' => 'Peace Coffee Whole Bean',
                'description' => 'Flavors Include: Espresso, French, Dark, Guatemalan,
                  Birchwood, Blue Ox, Morning Glory, Yellow Jacket., Size: 1 ct., Weight: 5 lb.,
                  Available in bulk whole bean: 5 lb. ',
                'category_id' => 1,
                'created_at' => '2017-02-13 07:51:56',
                'updated_at' => '2017-02-13 07:51:56',
                'deleted_at' => NULL,
                'primary_image_id' => NULL,
                'price' => '500.00',
                'quantity' => '500',
                'units' =>'Gram',
            ),
            4 =>
            array (
                'id' => 5,
                'name' => 'Starbucks Whole Bean',
                'description' => 'Growing on some of the highest coffee farms in
                  the majestic Andes Mountain range gives this coffee remarkable balance.
                  It’s medium-bodied and rich with juicy undertones and a crisp, nutty finish.',
                'category_id' => 1,
                'created_at' => '2017-02-13 07:54:04',
                'updated_at' => '2017-02-13 07:54:04',
                'deleted_at' => NULL,
                'primary_image_id' => NULL,
                'price' => '80000.00',
                'quantity' => '500',
                'units' =>'Gram',
            ),
        ));
    }
}
