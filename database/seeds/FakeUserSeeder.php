<?php

use Illuminate\Database\Seeder;

class FakeUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker\Factory::create();

        $customer_user_one =  App\User::find(3);

        $customer_profile = App\Profile::create([
          'created_at' 			=> $customer_user_one->created_at,
          'updated_at' 			=> $customer_user_one->updated_at,
          'user_id' 			=> $customer_user_one->id,
          'address' 			=> $faker->address,
          'landmark' 			=> $faker->randomElement($array = array ('katargam','adajan')),
          'country_id' 			=> $faker->randomElement($array = array (2, 105, 233)),
          'pincode_id' 			=> $faker->randomElement($array = array (9, 10, 11,13,14)),
          'area_id' 			=> $faker->randomElement($array = array (2,3,4,5,6,7,8)),
          'city' 				=> $faker->city,
          'phone_number' 		=> $faker->e164PhoneNumber,
          'alt_phone_number' 	=> $faker->e164PhoneNumber,
        ]);
    }
}
