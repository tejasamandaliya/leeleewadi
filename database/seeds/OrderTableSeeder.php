<?php

use Illuminate\Database\Seeder;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('orders')->delete();
        
        \DB::table('orders')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2017-02-09 16:11:18',
                'updated_at' => '2017-02-11 17:05:54',
                'deleted_at' => NULL,
                'user_id' => 1,
                'list_price' => '12.75',
                'discount' => '0.64',
                'sale_price' => '12.11',
                'status' => 'completed',
                'payment_time' => '2017-02-09 16:11:18',
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => '2017-02-10 13:17:12',
                'updated_at' => '2017-02-12 13:13:44',
                'deleted_at' => NULL,
                'user_id' => 2,
                'list_price' => '80515.50',
                'discount' => '8051.55',
                'sale_price' => '72463.95',
                'status' => 'completed',
                'payment_time' => '2017-02-10 13:17:12',
            ),
            2 => 
            array (
                'id' => 3,
                'created_at' => '2017-02-10 13:17:12',
                'updated_at' => '2017-04-25 05:53:25',
                'deleted_at' => NULL,
                'user_id' => 1,
                'list_price' => '80015.50',
                'discount' => '0.00',
                'sale_price' => '80015.50',
                'status' => 'completed',
                'payment_time' => '2017-02-10 13:17:12',
            ),
            3 => 
            array (
                'id' => 4,
                'created_at' => '2017-02-09 16:11:18',
                'updated_at' => '2017-04-25 05:53:46',
                'deleted_at' => NULL,
                'user_id' => 2,
                'list_price' => '12.75',
                'discount' => '0.64',
                'sale_price' => '12.11',
                'status' => 'completed',
                'payment_time' => '2017-02-09 16:11:18',
            ),
            4 => 
            array (
                'id' => 5,
                'created_at' => '2017-02-09 16:11:18',
                'updated_at' => '2017-04-25 05:53:57',
                'deleted_at' => NULL,
                'user_id' => 2,
                'list_price' => '5.00',
                'discount' => '0.25',
                'sale_price' => '4.75',
                'status' => 'completed',
                'payment_time' => '2017-02-09 16:11:18',
            ),
            5 => 
            array (
                'id' => 6,
                'created_at' => '2017-02-10 13:17:12',
                'updated_at' => '2017-04-25 05:54:17',
                'deleted_at' => NULL,
                'user_id' => 1,
                'list_price' => '80000.00',
                'discount' => '0.00',
                'sale_price' => '80000.00',
                'status' => 'completed',
                'payment_time' => '2017-02-10 13:17:12',
            ),
        ));
    }
}
