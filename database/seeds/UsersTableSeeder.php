<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->delete();
        
        $faker = Faker\Factory::create();

        \DB::table('users')->insert(array (
            0 =>
            array (
                'id' => 1,
                'role' => 'Admin',
                'first_name' => 'Wiglee',
                'last_name' => 'Admin',
                'email' => 'admin@wigglee.com',
                'password' => Hash::make('wigglee'),
                'remember_token' => '2DVDk6YUP4MMtvHQx6QfWz09AW0zthDiqud9jwhXD59BD4wiZgwvoY78bUM3',
                'created_at' => '2017-01-31 07:22:49',
                'updated_at' => '2017-02-01 17:29:57',
            ),
            1 =>
            array (
                'id' => 2,
                'role' => 'Admin',
                'first_name' => 'First',
                'last_name' => 'Admin',
                'email' => 'first@wigglee.com',
                'password' => Hash::make('first'),
                'remember_token' => '2DVDk6YUP4MMtvHQx6QfWz09AW0zthDiqud9jwhXD59BD4wiZgwvoY78bUM3',
                'created_at' => '2017-01-31 07:22:49',
                'updated_at' => '2017-02-01 17:29:57',
            ),
            2 =>
            array (
                'id' => 3,
                'role' => 'Customer',
                'first_name' => 'Customer',
                'last_name' => 'First',
                'email' => 'customer@wigglee.com',
                'password' => Hash::make('customer'),
                'remember_token' => '2DVDk6YUP4MMtvHQx6QfWz09AW0zthDiqud9jwhXD59BD4wiZgwvoY78bUM3',
                'created_at' => '2017-01-31 07:22:49',
                'updated_at' => '2017-02-01 17:29:57',
            ),
        ));
    }
}
