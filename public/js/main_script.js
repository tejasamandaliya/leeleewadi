$(document).ready(function(){

	/* Show/Hide Add Category Button*/

	$('.add_new_category').click(function(){

		$(this).hide();
		$('.add_new_subcategory').hide();
		$('#category_form').css('display','block');
		$('#subcategory_form').css('display','none');

	});


	/* Show/Hide Add SubCategory Button*/

	$('.add_new_subcategory').click(function(){

		$(this).hide();
		$('.add_new_category').hide();
		$('#category_form').css('display','none');
		$('#subcategory_form').css('display','block');

	});



		/** Edit Category Name Start**/

	$('.edit').click(function(){

		$(this).parent().parent().children('td').children('.category_name').hide();
		$(this).parent().parent().children('td').children('input.edit_category_name').show();

		$(this).parent().parent().children('td').children('.category_url').hide();
		$(this).parent().parent().children('td').children('input.edit_category_url').show();

		$(this).parent().children('.edit').hide();
		$(this).parent().children('.submit_catgory_name').show();
	});

	$('.submit_catgory_name').click(function(){


		var new_cat_name = $(this).parent().parent().children('td').children('input.edit_category_name').val();
		var cat_id = $(this).attr('id');

		var new_cat_url = $(this).parent().parent().children('td').children('input.edit_category_url').val();
		var cat_id = $(this).attr('id');

		var new_subcat_name = $(this).parent().parent().children('td').children('input.edit_subcategory_name').val();

		var subcat_id = $(this).attr('title');

		$(this).parent().parent().children('td').children('.category_name').html(new_cat_name);
		if(new_subcat_name == undefined)
		{

		//$(this).parent().parent().children('td').children('.subcategory_name').html('None');
		new_subcat_name = '';
		}
		else{
		$(this).parent().parent().children('td').children('.subcategory_name').html(new_subcat_name);
		}

		$(this).parent().parent().children('td').children('.category_name').show();
		$(this).parent().parent().children('td').children('input.edit_category_name').hide();

		$(this).parent().parent().children('td').children('.category_url').show();
		$(this).parent().parent().children('td').children('input.edit_category_url').hide();

		$(this).parent().children('.edit').show();
		$(this).parent().children('.submit_catgory_name').hide();

		var token= $('#token').val();

		var page_type = $('.page_type').attr('id');

		if(page_type == 'product'){
		$.post('/admin/productCategoryUpdate',{new_cat_name:new_cat_name,new_cat_url:new_cat_url,cat_id:cat_id,new_subcat_name:new_subcat_name,subcat_id:subcat_id,"_token":token})
		.done( function(msg) {


			$('.update_status').html('<div class="alert alert-success">Product Category Updated Successfully !!</div>').fadeIn('slow').delay(3000).hide(0);;


		} )
	    .fail( function(xhr, textStatus, errorThrown) {
	        //alert(xhr.responseText);
	        //alert(xhr.responseText);
	        //$('.error_rough').html(xhr.responseText);
	    });

		}
		else if(page_type == 'retailer'){

			$.post('/admin/retailerCategoryUpdate',{new_cat_name:new_cat_name,cat_id:cat_id,new_subcat_name:new_subcat_name,subcat_id:subcat_id,"_token":token})
			.done( function(msg) {


				$('.update_status').html('<div class="alert alert-success">Product Retailer Updated Successfully !!</div>').fadeIn('slow').delay(3000).hide(0);;


			} )
		    .fail( function(xhr, textStatus, errorThrown) {
		        //alert(xhr.responseText);
		        //$('.error_rough').html(xhr.responseText);
		    });

		}


	});

	/** Edit Category Name End**/


	/*Delete Category Name Start*/

		$('.delete').click(function(){

			var cat_id = $(this).attr('id');

			$(this).parent().parent().hide();

			var token= $('#token').val();

			var page_type = $('.page_type').attr('id');

		if(page_type == 'product'){

		$.post('/admin/productCategoryDelete',{cat_id:cat_id,"_token":token})
		.done( function(msg) {

			$('.update_status').html('<div class="alert alert-warning">Product Category Deleted Successfully !!</div>').fadeIn('slow').delay(3000).hide(0);;


		} )
	    .fail( function(xhr, textStatus, errorThrown) {
	        //alert(xhr.responseText);
	        //$('.error_rough').html(xhr.responseText);
	    });

		}else if(page_type == 'retailer'){

			$.post('/admin/retailerCategoryDelete',{cat_id:cat_id,"_token":token})
			.done( function(msg) {

				$('.update_status').html('<div class="alert alert-warning">Retailer Category Deleted Successfully !!</div>').fadeIn('slow').delay(3000).hide(0);;


			} )
		    .fail( function(xhr, textStatus, errorThrown) {
		        //alert(xhr.responseText);
		        //$('.error_rough').html(xhr.responseText);
		    });

		}

		});

	/*Delete Category Name End*/


});