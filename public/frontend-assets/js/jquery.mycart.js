/*
* jQuery myCart - v1.2 - 2017-05-09
* http://asraf-uddin-ahmed.github.io/
* Copyright (c) 2017 Asraf Uddin Ahmed; Licensed None
*/

(function ($) {

  // "use strict";

  var cartItemsArray = [];
  var addShipping = 0;
  var totalShipping = 0;
  // var perUnitGrams = 500;
  // get session value 
  cartItemsArray = sessionStorage.getItem("cartItems");

  var OptionManager = (function () {
    var objToReturn = {};

    var _options = null;
    var DEFAULT_OPTIONS = {
      currencySymbol: '$',
      classCartIcon: 'my-cart-icon',
      classCartBadge: 'my-cart-badge',
      classProductQuantity: 'my-product-quantity',
      classProductRemove: 'my-product-remove',
      classCheckoutCart: 'my-cart-checkout',
      affixCartIcon: true,
      showCheckoutModal: true,
      numberOfDecimals: 2,
      cartItems: cartItemsArray,
      clickOnAddToCart: function($addTocart) { },
      afterAddOnCart: function(products, totalPrice, totalQuantity) { },
      clickOnCartIcon: function($cartIcon, products, totalPrice, totalQuantity) { },
      checkoutCart: function(products, totalPrice, totalQuantity) { },
      getDiscountPrice: function(products, totalPrice, totalQuantity) { return null; }
    };


    var loadOptions = function (customOptions) {
      _options = $.extend({}, DEFAULT_OPTIONS);
      if (typeof customOptions === 'object') {
        $.extend(_options, customOptions);
      }
    }
    var getOptions = function () {
      return _options;
    }

    objToReturn.loadOptions = loadOptions;
    objToReturn.getOptions = getOptions;
    return objToReturn;
  }());

  var MathHelper = (function() {
    var objToReturn = {};
    var getRoundedNumber = function(number){
      var options = OptionManager.getOptions();
      if(typeof number == 'string'){
        return number;
      }else{
        return number.toFixed(options.numberOfDecimals);
      }
    }
    objToReturn.getRoundedNumber = getRoundedNumber;
    return objToReturn;
  }());

  var ProductManager = (function(){
    var objToReturn = {};

    /*
    PRIVATE
    */
    localStorage.products = localStorage.products ? localStorage.products : "";
    var getIndexOfProduct = function(id){
      var productIndex = -1;
      var products = getAllProducts();
      $.each(products, function(index, value){
        if(value.id == id){
          productIndex = index;
          return;
        }
      });
      return productIndex;
    }
    var setAllProducts = function(products){
      localStorage.products = JSON.stringify(products);
      // set session value
      if (window.sessionStorage) {
        sessionStorage.setItem("cartItems", localStorage.products);
      }


    }
    var addProduct = function(id, name, summary, price, quantity, per_quantity, image, units) {

      var products = getAllProducts();
      
      products.push({
        id: id,
        name: name,
        summary: summary,
        price: price,
        quantity: quantity,
        image: image,
        units: units,
        per_quantity: per_quantity,
      });
      setAllProducts(products);

      if(ProductManager.getTotalQuantity() !== 0){
        $('.cart-icon').html($('<span class="cart-total">'+ProductManager.getTotalQuantity()+'</span>'));
      }else{
        $('.cart-icon').html('');
      }
      if(quantity == 0){
        $('.cart-orders-'+id).html('');
      }else{
        var _per_unit_grams =( quantity * per_quantity ).toPrecision(2);
        var _unit='Kg';
        if( units == 'Gram' ){
          _unit = 'Gram';
          if(_per_unit_grams >= 1000){
            _per_unit_grams = (_per_unit_grams / 1000).toPrecision(2); 
            _unit = 'Kg';
          }
        }
        $('.cart-orders-'+id).html($('<div class="total-quantity">'+_per_unit_grams+ ' '+_unit+'</div>'));
      }
    }

    /*
    PUBLIC
    */
    var getAllProducts = function(){
      try {
        var products = JSON.parse(localStorage.products);
        $.each(products, function(index, value){
          if(value.quantity == 0){
            $('.cart-orders-'+value.id).html('');
          }else{
            var _per_unit_grams = value.quantity * value.per_quantity ;
            var _unit='Kg';
            if( value.units == 'Gram' ){
              _unit = 'Gram';
              if(_per_unit_grams >= 1000){
                _per_unit_grams = (_per_unit_grams / 1000).toPrecision(3); 
                _unit = 'Kg';
              }
            }
            $('.cart-orders-'+value.id).html($('<div class="total-quantity">'+_per_unit_grams+ ' ' + _unit +'</div>'));
          }
        });
        return products;
      } catch (e) {
        return [];
      }
    }
    var updatePoduct = function(id, quantity) {
      var productIndex = getIndexOfProduct(id);
      if(productIndex < 0){
        return false;
      }
      var products = getAllProducts();
      products[productIndex].quantity = typeof quantity === "undefined" ? products[productIndex].quantity * 1 + 1 : quantity;
      setAllProducts(products);
      if(quantity == 0){
        $('.cart-orders-'+id).html('');
      }else{
        var _per_unit_grams =( products[productIndex].quantity * products[productIndex].per_quantity ).toPrecision(2);
        var _unit='Kg';
        if( products[productIndex].units == 'Gram' ){
          _unit = 'Gram';
          if(_per_unit_grams >= 1000){
            _per_unit_grams = (_per_unit_grams / 1000).toPrecision(2); 
            _unit = 'Kg';
          }
        }
        $('.cart-orders-'+id).html($('<div class="total-quantity">'+_per_unit_grams+' '+_unit+'</div>'));
      }
      if(ProductManager.getTotalQuantity() !== 0){
        $('.cart-icon').html($('<span class="cart-total">'+ProductManager.getTotalQuantity()+'</span>'));
      }else{
        $('.cart-icon').html('');
      }
      return true;
    }
    var setProduct = function(id, name, summary, price, quantity, per_quantity, image, units) {
      if(typeof id === "undefined"){
        console.error("id required")
        return false;
      }
      if(typeof name === "undefined"){
        console.error("name required")
        return false;
      }
      if(typeof image === "undefined"){
        console.error("image required")
        return false;
      }
      if(!$.isNumeric(price)){
        console.error("price is not a number")
        return false;
      }
      if(!$.isNumeric(quantity)) {
        console.error("quantity is not a number");
        return false;
      }
      if(!$.isNumeric(per_quantity)) {
        console.error("per_quantity is not a number");
        return false;
      }
      if(typeof units === "undefined"){
        console.error("units required");
        return false;
      }
      summary = typeof summary === "undefined" ? "" : summary;

      if(!updatePoduct(id)){
        addProduct(id, name, summary, price, quantity, per_quantity, image, units);
      }
    }
    var clearProduct = function(){
      setAllProducts([]);
      // set session value
      if (window.sessionStorage) {
        sessionStorage.setItem("cartItems", []);
      }
    }
    var removeProduct = function(id){
      var products = getAllProducts();
      if(products.length == 0){
        $('.cart-orders-'+id).html('');
      }
      products = $.grep(products, function(value, index) {
        var _per_unit_grams = value.quantity * value.per_quantity ;
        var _unit='Kg';
        if( value.units == 'Gram' ){
          _unit = 'Gram';
          if(_per_unit_grams >= 1000){
            _per_unit_grams = (_per_unit_grams / 1000).toPrecision(2); 
            _unit = 'Kg';
          }
        }
        if(value.quantity == 0){
          $('.cart-orders-'+value.id).html('');
        }else{
          $('.cart-orders-'+value.id).html($('<div class="total-quantity">'+_per_unit_grams+ ' '+_unit+'</div>'));
        }

        if(value.id == id){
          $('.cart-orders-'+value.id).html('');
        }
        return value.id != id;
      });
      setAllProducts(products);
      
      if(ProductManager.getTotalQuantity() !== 0){
        $('.cart-icon').html($('<span class="cart-total">'+ProductManager.getTotalQuantity()+'</span>'));
      }else{
        $('.cart-icon').html('');
      }
    }
    var getTotalQuantity = function(){
      var total = 0;
      var products = getAllProducts();
      $.each(products, function(index, value){
        total += value.quantity * 1;
      });
      return total;
    }
    
    var getTotalPrice = function(){
      var products = getAllProducts();
      var total = 0;
      addShipping = 0 ;
      $.each(products, function(index, value){
        total += value.quantity * value.price;
        total = MathHelper.getRoundedNumber(total) * 1;
        addShipping += value.quantity * value.price;
      });
      if(addShipping < 199){
          total += 50;
          total = MathHelper.getRoundedNumber(total) * 1;
      }
      return total;
    }

    objToReturn.getAllProducts = getAllProducts;
    objToReturn.updatePoduct = updatePoduct;
    objToReturn.setProduct = setProduct;
    objToReturn.clearProduct = clearProduct;
    objToReturn.removeProduct = removeProduct;
    objToReturn.getTotalQuantity = getTotalQuantity;
    objToReturn.getTotalPrice = getTotalPrice;
    return objToReturn;
  }());


  var loadMyCartEvent = function(){

    var options = OptionManager.getOptions();
    var $cartIcon = $("." + options.classCartIcon);
    var $cartBadge = $("." + options.classCartBadge);
    var classProductQuantity = options.classProductQuantity;
    var classProductRemove = options.classProductRemove;
    var classCheckoutCart = options.classCheckoutCart;

    var idCartModal = 'my-cart-modal';
    var classCartTable = 'my-cart-table';
    var classCheckoutPageCarttable = 'my-checkout-page-cart-table';
    var classGrandTotal = 'my-cart-grand-total';
    var idEmptyCartMessage = 'my-cart-empty-message';
    var classDiscountPrice = 'my-cart-discount-price';
    var classProductTotal = 'my-product-total';
    var classAffixMyCartIcon = 'my-cart-icon-affix';
    var classPlaceOrder = 'my-place-order';


    if(options.cartItems && options.cartItems.constructor === Array) {
      ProductManager.clearProduct();
      $.each(options.cartItems, function() {
        ProductManager.setProduct(this.id, this.name, this.summary, this.price, this.quantity, this.per_quantity, this.image, this.units,addShipping);
      });
    }

    $cartBadge.text(ProductManager.getTotalQuantity());
  
    if(ProductManager.getTotalQuantity() !== 0){
      $('.cart-icon').html($('<span class="cart-total">'+ProductManager.getTotalQuantity()+'</span>'));
    }else{
      $('.cart-icon').html('');
    }


    if(!$("#" + idCartModal).length) {
      $('body').append(
        '<div class="modal fade" id="' + idCartModal + '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">' +
        '<div class="modal-dialog" role="document">' +
        '<div class="modal-content">' +
        '<div class="modal-header">' +
        '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
        '<h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-shopping-cart"></span> My Cart</h4>' +
        '</div>' +
        '<div class="modal-body">' +
        '<table class="table table-hover table-responsive '+classCartTable+'"></table>' +
        // '<div class="table-responsive ' + classCartTable + '"></div>'+
        '</div>' +
        '<div class="modal-footer">' +
        '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
        '<button type="button" class="btn btn-primary ' + classCheckoutCart + '">Checkout</button>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>'
      );
    }

    var drawTable = function(){
      var $cartTable = $("." + classCartTable);
      var $checkoutCartTable = $("." + classCheckoutPageCarttable);
      $cartTable.empty();
      $checkoutCartTable.empty();

      $tTable = $('<table class="table"></table>');
      $theader = $('<thead class="thead-default"></thead>');
      $tbody = $('<tbody></tbody>');
      
      var products = ProductManager.getAllProducts();

      if(products.length){
        $theader.append(
          '<tr>'+
            '<th>Image</th>'+
            '<th class="text-left">Product</th>'+
            '<th class="text-left">Unit Price</th>'+
            '<th class="text-left">Unit</th>'+
            '<th class="text-left">Quantity</th>'+
            '<th class="text-right">Total</th>'+
            '<th></th>'+
          '</tr>'
        );
      }


      $.each(products, function(){
        var total = this.quantity * this.price;
        var per_unit_grams =  this.quantity * this.per_quantity;//perUnitGrams;
        var unit='Kg';
        if( this.units == 'Gram' ){
          unit = 'Gram';
          if(per_unit_grams >= 1000){
            per_unit_grams = (per_unit_grams / 1000).toPrecision(2); 
            unit = 'Kg';
          }
        }
        

        $cartTable
        .append(
          $('<tr title="' + this.summary + '" data-id="' + this.id + '" data-units="' + this.units + '" data-per-quantity="' + this.per_quantity + '" data-price="' + this.price + '"></tr>')
            .append(
                '<td class="text-center" style="width: 30px;"><img width="30px" height="30px" src="' + this.image + '"/></td>' +
                '<td>' + this.name + '</td>' +
                '<td title="Unit Price">' + options.currencySymbol + MathHelper.getRoundedNumber(this.price) + '</td>' +
                '<td title="Unit"><span class="per-unit-grams-'+this.id+'">'+ per_unit_grams + ' ' + unit +'</span></td>'
            )
            .append(
              $('<td title="Quantity"></td>')
              .append(
                $('<input type="number" min="1" style="width: 70px;" class="' + classProductQuantity + '" value="' + this.quantity + '"/>')
              )
            )
            .append(
                '<td title="Total" class="' + classProductTotal + '">' + options.currencySymbol  + MathHelper.getRoundedNumber(total) + '</td>' +
                '<td title="Remove from Cart" class="text-center" style="width: 30px;"><a href="javascript:void(0);" class="btn btn-xs btn-danger ' + classProductRemove + '">X</a></td>'
            )
         );
        
        $tbody.append(
          $('<tr title="' + this.summary + '" data-id="' + this.id + '" data-units="' + this.units + '" data-per-quantity="' + this.per_quantity + '" data-price="' + this.price + '"></tr>')
          .append(
            '<th scope="row">'+
              '<a href="#">'+
                '<img class="img-thumbnail" src="' + this.image + '">'+
              '</a>'+
            '</th>'+
            '<td class="text-left">'+
              '<b>'+ this.name +'</b>'+
              '<div></div>'+
            '</td>'+
            '<td class="text-left">'+
              '<p class="price">'+
                '<b>' + options.currencySymbol + MathHelper.getRoundedNumber(this.price) + '</b>'+
              '</p>'+
            '</td>' +
            '<td class="text-left">'+
              '<b><span class="per-unit-grams-'+this.id+'">'+ per_unit_grams + ' ' + unit +'</span></b>'+
              '<div></div>'+
            '</td>'
          )
          .append(
            $('<td class="text-left qty-btn" title="Quantity"></td>')
            .append(
              '<input type="number" min="1" style="width: 70px;" class="' + classProductQuantity + '" value="' + this.quantity + '"/>'
            )
            .click(function(){
              $('.my-cart-icon').click();
              if(this.quantity !== 0){
                var _per_unit_grams =( this.quantity * this.per_quantity ).toPrecision(2);
                var _unit='Kg';
                if( this.units == 'Gram' ){
                  _unit = 'Gram';
                  if(_per_unit_grams >= 1000){
                    _per_unit_grams = (_per_unit_grams / 1000).toPrecision(2); 
                    _unit = 'Kg';
                  }
                }
                $('.cart-orders-'+this.id).html($('<div class="total-quantity">'+ _per_unit_grams + ' ' + _unit + '</div>'));
              }else{
                $('.cart-orders-'+this.id).html('');
              }
              if(ProductManager.getTotalQuantity() !== 0){
                $('.cart-icon').html($('<span class="cart-total">'+ProductManager.getTotalQuantity()+'</span>'));
              }else{
                $('.cart-icon').html($(''));
              }
            })
            // .keyup(function() {
            //     var value = parseFloat($(this).val());
            //     $output.val(value*250);
            // })
          )
          .append(
            '<td class="text-right price" class="' + classProductTotal + '">'+
              options.currencySymbol  + MathHelper.getRoundedNumber(total) +
            '</td>'+
            '<td title="Remove from Cart" class="text-center" style="width: 30px;"><a href="javascript:void(0);" class="btn btn-xs btn-danger ' + classProductRemove + '">X</a></td>' 
          )
        );

      });

      $tTable
      .append($theader)
      .append($tbody);

      $checkoutCartTable
      .append(
        $('<div class="table-responsive"></div>')
        .append($tTable)
      );

      $cartTable.append(products.length ?
        '<tr>' +
        '<td></td>' +
        '<td><strong>Total</strong></td>' +
        '<td></td>' +
        '<td></td>' +
        '<td><strong class="' + classGrandTotal + '"></strong></td>' +
        '<td></td>' +
        '</tr>'
        : '<div class="alert alert-danger" role="alert" id="' + idEmptyCartMessage + '">Your cart is empty</div>'
      );

      if(products.length){

        $('.my-empty-checkout-page-cart-table').hide();
        $('.my-cart-footer').show();

        $tTable = $('<table class="table"></table>');

        if(addShipping < 199){
          $tTable.append(
            '<thead class="thead-inverse">'+
              '<tr>'+
                '<td></td>'+
                '<td></td>'+
                '<td class="text-right">'+
                  '<strong style="color:red">Order Below Rs.199  </strong>'+
                  '<strong style="color:red">Shipping Charge :</strong>'+
                '</td>'+
                '<td class="text-right">'+
                  '<strong class="">50'+
                  '</strong>'+
                '</td>'+
              '</tr>'+
            '</thead>'
          );
        }
        $tTable.append(
          '<thead class="thead-inverse">'+
            '<tr>'+
              '<td></td>'+
              '<td></td>'+
              '<td class="text-right">'+
                '<strong>Total</strong>'+
              '</td>'+
              '<td class="text-right">'+
                '<strong class="' + classGrandTotal + '">'+
                '</strong>'+
              '</td>'+
            '</tr>'+
          '</thead>'
        );



        // var discountPrice = options.getDiscountPrice(products, ProductManager.getTotalPrice(), ProductManager.getTotalQuantity());
        // if(products.length && discountPrice !== null) {
        //   $cartTable.append(
        //     '<tr style="color: red">' +
        //     '<td></td>' +
        //     '<td><strong>Total (including discount)</strong></td>' +
        //     '<td></td>' +
        //     '<td></td>' +
        //     '<td><strong class="' + classDiscountPrice + '"></strong></td>' +
        //     '<td></td>' +
        //     '</tr>'
        //   );

        //   $tTable.append(
        //     '<thead class="thead-inverse">'+
        //       '<tr>'+
        //         '<td></td>'+
        //         '<td></td>'+
        //         '<td class="text-right">'+
        //           '<strong>Total (including discount)</strong>'+
        //         '</td>'+
        //         '<td class="text-right">'+
        //           '<strong class="' + classDiscountPrice + '">'+
        //           '</strong>'+
        //         '</td>'+
        //       '</tr>'+
        //     '</thead>'
        //   );

          
        // }

        $checkoutCartTable
        .append($tTable);        

      }else{
        $('.my-empty-checkout-page-cart-table').show();     
        $('.my-cart-footer').hide();
      }

      showGrandTotal();
      showDiscountPrice();
    }
    var showModal = function(){
      drawTable();
      if($('#checkout-page').data('page') !== 'checkout')
        $("#" + idCartModal).modal('show');
    }
    var updateCart = function(){
      $.each($("." + classProductQuantity), function(){
        var id = $(this).closest("tr").data("id");
        var per_quantity = $(this).closest("tr").data("per-quantity");

        var per_unit_grams = $(this).val() * per_quantity;//perUnitGrams;
        if(per_unit_grams >= 1000){
          per_unit_grams = (per_unit_grams / 1000).toPrecision(2); 
        }
        $('.per-unit-grams-'+id).html( per_unit_grams );
        ProductManager.updatePoduct(id, $(this).val());
      });
    }
    var showGrandTotal = function(){
      $("." + classGrandTotal).text(options.currencySymbol + MathHelper.getRoundedNumber(ProductManager.getTotalPrice()));
    }
    var showDiscountPrice = function(){
      $("." + classDiscountPrice).text(options.currencySymbol + MathHelper.getRoundedNumber(options.getDiscountPrice(ProductManager.getAllProducts(), ProductManager.getTotalPrice(), ProductManager.getTotalQuantity())));
    }

    /*
    EVENT
    */
    if(options.affixCartIcon) {
      var cartIconBottom = $cartIcon.offset().top * 1 + $cartIcon.css("height").match(/\d+/) * 1;
      var cartIconPosition = $cartIcon.css('position');
      $(window).scroll(function () {
        $(window).scrollTop() >= cartIconBottom ? $cartIcon.addClass(classAffixMyCartIcon) : $cartIcon.removeClass(classAffixMyCartIcon);
      });
    }

    $cartIcon.click(function(){
      options.showCheckoutModal ? showModal() : options.clickOnCartIcon($cartIcon, ProductManager.getAllProducts(), ProductManager.getTotalPrice(), ProductManager.getTotalQuantity());
    });

    $(document).on("input", "." + classProductQuantity, function () {
      var price = $(this).closest("tr").data("price");
      var id = $(this).closest("tr").data("id");
      var per_quantity = $(this).closest("tr").data("per-quantity");
      var units = $(this).closest("tr").data("units");
      var quantity = $(this).val();


      $(this).parent("td").next("." + classProductTotal).text(options.currencySymbol + MathHelper.getRoundedNumber(price * quantity));
      ProductManager.updatePoduct(id, quantity);

      $cartBadge.text(ProductManager.getTotalQuantity());

      var per_unit_grams = quantity * per_quantity;//perUnitGrams;
      var unit='Kg';
      if( units == 'Gram' ){
        unit = 'Gram';
        if(per_unit_grams >= 1000){
            per_unit_grams = (per_unit_grams / 1000).toPrecision(2); 
            unit = 'Kg';
        }
      }
      $('.per-unit-grams-'+id).html( per_unit_grams + ' ' + unit );

      showGrandTotal();
      showDiscountPrice();
    });

    $(document).on('keypress', "." + classProductQuantity, function(evt){
      if(evt.keyCode == 38 || evt.keyCode == 40){
        return ;
      }

      evt.preventDefault();
    });

    $(document).on('click', "." + classProductRemove, function(){
      var $tr = $(this).closest("tr");
      var id = $tr.data("id");
      $tr.hide(500, function(){
        ProductManager.removeProduct(id);
        drawTable();
        $cartBadge.text(ProductManager.getTotalQuantity());
      });
    });

    $("." + classCheckoutCart).click(function(){
      $("#" + idCartModal).modal("hide");
      window.location.replace("/checkout");
    });

    $("."+classPlaceOrder).click(function(){
      
      
      var products = ProductManager.getAllProducts();
      $.each(products, function(index, value){
        totalShipping += value.quantity * value.price;
        totalShipping = MathHelper.getRoundedNumber(totalShipping) * 1;
      });
      // place-order
      $.ajax({
         type:'POST',
         url:'/place-order',
         data: { 
          '_token': window.Laravel.csrfToken, 
          'products': products,
          'total_price': totalShipping,
          'total_quantity': ProductManager.getTotalQuantity(),
          'shippingCharge':(totalShipping<199)?50:0
        },
         success:function(data){
            if(data.success == true){
              if(!products.length) {
                $("#" + idEmptyCartMessage).fadeTo('fast', 0.5).fadeTo('fast', 1.0);
                return ;
              }
              updateCart();
              options.checkoutCart(ProductManager.getAllProducts(), ProductManager.getTotalPrice(), ProductManager.getTotalQuantity());
              window.location.replace("/thanks");
            }
         }
      });

      ProductManager.clearProduct();
      $cartBadge.text(ProductManager.getTotalQuantity());
    });

  }


  var MyCart = function (target) {
    /*
    PRIVATE
    */
    var $target = $(target);
    var options = OptionManager.getOptions();
    var $cartIcon = $("." + options.classCartIcon);
    var $cartBadge = $("." + options.classCartBadge);

    /*
    EVENT
    */
    $target.click(function(){
      options.clickOnAddToCart($target);

      var id = $target.data('id');
      var name = $target.data('name');
      var summary = $target.data('summary');
      var price = $target.data('price');
      var quantity = $target.data('quantity');
      var per_quantity = $target.data('per-quantity');
      var image = $target.data('image');
      var units = $target.data('units');
      ProductManager.setProduct(id, name, summary, price, quantity, per_quantity, image, units);
      $cartBadge.text(ProductManager.getTotalQuantity());

      options.afterAddOnCart(ProductManager.getAllProducts(), ProductManager.getTotalPrice(), ProductManager.getTotalQuantity());
    });

  }


  $.fn.myCart = function (userOptions) {
    OptionManager.loadOptions(userOptions);
    loadMyCartEvent();
    return $.each(this, function () {
      new MyCart(this);
    });
  }

})(jQuery);
