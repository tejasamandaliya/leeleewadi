<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
/* Admin Routes */
Route::group( [ 'namespace' => 'Admin', 'prefix' => 'admin' ], function() {
	Auth::routes();
    Route::get('/logout', 'Auth\LoginController@logout');
    Route::get('/', function() {
        if(auth()->check()) {
            if(Auth::user()->role == 'Customer'){
                return redirect('/home');
            }else{
             return redirect('/admin/home');   
            }
        } else {
            return redirect('/admin/login');
        }
    });
    
    Route::group( [ 'middleware' => 'auth.admin' ], function() {

        Route::get('/home', 'HomeController@index');
        Route::any('/productCategory', 'productsController@productCategoryList');
        Route::post('/productCategoryUpdate', 'productsController@productCategoryUpdate');
        Route::post('/productCategoryDelete', 'productsController@productCategoryDelete');

        Route::get('/products', 'productsController@productList');
        Route::match(['get', 'post'], '/addProduct/{productId?}/{type?}', 'productsController@createProduct');
        Route::get('/viewProduct/{productId}', 'productsController@viewProduct');
        Route::post('/deleteProductImage/{imageId}', 'productsController@removeProductImage');
        Route::post('/productDelete', 'productsController@productDelete');

        Route::get('/orders', 'ordersController@orderList');
        Route::get('/orders/getorderstatistics', 'ordersController@getorderstatistics');
        Route::get('/orders/getchart', 'ordersController@getchart');
        Route::get('/orders/addOrder/{order_id?}', 'ordersController@createOrder');
        Route::get('/orders/view/{order}', 'ordersController@viewOrder');
        Route::get('/orders/detail/{order}', 'ordersController@getOrderDetails');
        Route::get('/orders/delete/{order}', 'ordersController@deleteOrder');
        Route::get('/orders/print/{order_id}', 'ordersController@printOrderInvoice');
        Route::get('/orders/accept/{order}', 'ordersController@acceptStatusOrder');
        Route::get('/orders/complete/{order}', 'ordersController@completeStatusOrder');
        Route::get('/orders/cancel/{order}', 'ordersController@cancelStatusOrder');
        Route::get('/pending/orders', 'ordersController@pendingOrder');
        
        Route::get('/customerProfile/{customerId}', 'ordersController@getCustomerProfile');


        Route::get('/pdfview', 'ordersController@getMenifest');
        Route::get('get/menifest/{order_id}', 'ordersController@getMenifest');
        Route::get('/thanks','HomeController@thanks');
        

        Route::get('/pickUpList','ordersController@pickUpList'); 
    });
});

/* Frontend Routes */
Route::group( [ 'namespace' => 'Customer' ], function() {
    // Route::auth();
    Route::get('/logout', 'Auth\LoginController@logout');

    Route::get('/', function() {
        if(auth()->check()) {
            if(Auth::user()->role == 'Admin'){
                return redirect('/admin/home');    
            }else{
                return redirect('/home');    
            }
        } else {
            return redirect('/home');
        }
    });
    Auth::routes();
    Route::get('/home', 'HomeController@index');
    Route::get('/list', 'HomeController@productList');
    Route::get('/product/{id}', 'HomeController@product');
    Route::get('/checkout', 'HomeController@checkout');

    Route::get('/orders', 'ordersController@orderHistory');
    Route::get('/order/view/{order}', 'ordersController@orderView');

    Route::get('/categoris/{category}', 'CategoryController@categoriesList');
    Route::get('/products/{product}', 'CategoryController@productView');

    Route::get('/login', 'Auth\LoginController@showLoginForm');
    Route::post('/register', 'Auth\RegisterController@create');
    Route::get('/profile', 'HomeController@userProfile');
    Route::post('/profile', 'HomeController@userProfileSave');
    Route::post('/place-order','HomeController@place_order');
    Route::get('/orders', 'ordersController@orderHistory');
    Route::get('/addresses','ordersController@showAddresses');
    Route::get('/thanks','HomeController@thanks');
    
});

